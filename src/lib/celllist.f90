module celllist
  use datatypes
  use util
  implicit none

contains

  !==================================!
  !     initialize the cell list     !
  !==================================!
  subroutine initialize_cell_list(parameters,system)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    ! internal variables
    real(P) :: cell_volume
    integer :: max_occupancy, num_cells, cell_id

    ! calculate cell size and number
    system%cell_list%cell_dim = floor( system%box_length / parameters%cell_cutoff )
    
    where( system%cell_list%cell_dim .lt. 3 )
       system%cell_list%cell_dim = 3
    end where

    where( system%cell_list%cell_dim .gt. max_cells )
       system%cell_list%cell_dim = max_cells
    end where

    num_cells   = product( system%cell_list%cell_dim )
    ! calculate cell size
    system%cell_list%side_length = system%box_length / system%cell_list%cell_dim
    cell_volume = product( system%cell_list%side_length )

    ! assume the density can be no higher than 10
    ! (this is a VERY safe assumption)
    ! BUT only if the particles have a diameter of 1
    ! the new snapshot has a diameter of 0.01 and leads to a problem!
    max_occupancy = ceiling( cell_volume ) * 10
    system%cell_list%max_occupancy = max_occupancy

    ! allocate memory for cell list
    allocate( system%cell_list%cell_count( num_cells ) )
    allocate( system%cell_list%cell_map( max_occupancy, num_cells ) )
    ! allocate( system%cell_list%snapshot( 3, system%num_particles ) )
    ! allocate memory for neighbors list
    allocate( system%cell_list%neighbors( 27, num_cells ) )
    ! calculate neighbors list
    do cell_id = 1, num_cells
       system%cell_list%neighbors(:,cell_id) = cell_neighbors(system,cell_id)
    end do
    
  end subroutine initialize_cell_list

  !===============================!
  !     destroy the cell list     !
  !===============================!
  subroutine cleanup_cell_list(system)
    implicit none
    ! external variables
    type (system_struct),    intent(inout) :: system

    ! deallocate memory for cell list
    deallocate( system%cell_list%cell_count )
    deallocate( system%cell_list%cell_map )
    ! deallocate( system%cell_list%snapshot )
    deallocate( system%cell_list%neighbors )
    
  end subroutine cleanup_cell_list
  
  !==============================!
  !     update the cell list     !
  !==============================!
  subroutine build_cell_list(parameters,system)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    ! internal variables
    integer, dimension(3) :: nominal_cells
    integer :: particle_id, cell_id

    ! ensure that memory is allocated
    if( .not. allocated(system%cell_list%cell_map) ) then
       call initialize_cell_list(parameters,system)
    end if
    
    ! update cell size
    system%cell_list%side_length = system%box_length / system%cell_list%cell_dim

    ! check if the cells are the wrong size
    nominal_cells = floor( system%box_length / parameters%cell_cutoff )

    where( nominal_cells .lt. 3 )
       nominal_cells = 3
    end where

    where( nominal_cells .gt. max_cells )
       nominal_cells = max_cells
    end where
    
    if( any( nominal_cells .lt. system%cell_list%cell_dim ) .or. &
        any( nominal_cells .gt. system%cell_list%cell_dim * 2 ) ) then
       
       ! destroy the old data structures
       call cleanup_cell_list(system)
       ! create the new data structures
       call initialize_cell_list(parameters,system)

    end if
    
    ! zero the cell list
    system%cell_list%cell_map = 0
    system%cell_list%cell_count = 0

    ! place particles in the appropriate cells
    do particle_id = 1, system%num_particles
       ! get the cell id from the particle position
       cell_id = which_cell(system,system%position(:,particle_id))
       ! store the information in the cell list
       system%cell_list%cell_count(cell_id) = system%cell_list%cell_count(cell_id) + 1
       system%cell_list%cell_map(system%cell_list%cell_count(cell_id),cell_id) = particle_id
    end do

  end subroutine build_cell_list
  
  !========================================================!
  !     identify cell id based on particle coordinates     !
  !========================================================!
  function which_cell(system,particle_coord)
    implicit none
    integer :: which_cell
    ! external variables
    type (system_struct),    intent(inout) :: system
    real(P), dimension(3),   intent(inout) :: particle_coord
    ! internal variables
    integer, dimension(3) :: cell_coord
    
    ! find the coordinates
    ! NOTE: coordinates are shifted by L/2 so the indices start at zero
    cell_coord = floor( ( particle_coord + &
         system%box_length*0.5_P ) / system%cell_list%side_length )

    where( cell_coord .lt. 0 )
       cell_coord = 0
    end where

    where( cell_coord .ge. system%cell_list%cell_dim )
       cell_coord = system%cell_list%cell_dim - 1
    end where
    
    ! convert the coordinates to id
    which_cell = cell_coord_to_id(cell_coord,system%cell_list%cell_dim)

    if( which_cell .gt. product(system%cell_list%cell_dim) &
         .or. which_cell .lt. 1 ) then
       print*,'problem in which_cell:',particle_coord,':',cell_coord,':',&
            system%cell_list%side_length,'->',which_cell
       stop
    end if
    
  end function which_cell
  
  !=========================================================!
  !     identify neighboring cells based on cell number     !
  !=========================================================!
  function cell_neighbors(system,cell_id)
    implicit none
    integer, dimension(27) :: cell_neighbors
    ! external variables
    type (system_struct), intent(inout) :: system
    integer, intent(in) :: cell_id
    ! internal variables
    integer, dimension(3) :: cell_coord, neighbor_coord
    integer :: i, j, k, count
    integer :: cell_id_copy

    cell_id_copy = cell_id
    
    ! start by getting a tuple of the coordinates
    cell_coord = cell_id_to_coord(cell_id_copy,system%cell_list%cell_dim)

    if( any( cell_coord .ge. system%cell_list%cell_dim ) ) then
       print*,'problem in cell_neighbors'
    end if

    count = 0
    ! loop through cell neighbors in 3D
    do i = cell_coord(1)-1, cell_coord(1)+1
       do j = cell_coord(2)-1, cell_coord(2)+1
          do k = cell_coord(3)-1, cell_coord(3)+1
             count = count + 1
             neighbor_coord = wrap_cell_coord((/i,j,k/),system%cell_list%cell_dim)
             cell_neighbors(count) = cell_coord_to_id(neighbor_coord,system%cell_list%cell_dim)
          end do
       end do
    end do
    
  end function cell_neighbors
  
end module celllist
