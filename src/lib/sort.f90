! Recursive Fortran 95 quicksort routine
! sorts real numbers into ascending numerical order
! Author: Juli Rew, SCD Consulting (juliana@ucar.edu), 9/03
! Based on algorithm from Cormen et al., Introduction to Algorithms,
! 1997 printing

! Made F conformant by Walt Brainerd

module sort

  use datatypes
  implicit none
  public :: quicksort_real, quicksort_int
  private :: Partition_real, Partition_int

contains

  recursive subroutine quicksort_real(A)
    real(P), intent(in out), dimension(:) :: A
    integer :: iq

    if(size(A) > 1) then
       call Partition_real(A, iq)
       call quicksort_real(A(:iq-1))
       call quicksort_real(A(iq:))
    endif
  end subroutine quicksort_real

  subroutine Partition_real(A, marker)
    real(P), intent(in out), dimension(:) :: A
    integer, intent(out) :: marker
    integer :: i, j
    real(P) :: temp
    real(P) :: x      ! pivot point
    x = A(1)
    i= 0
    j= size(A) + 1

    do
       j = j-1
       do
          if (A(j) <= x) exit
          j = j-1
       end do
       i = i+1
       do
          if (A(i) >= x) exit
          i = i+1
       end do
       if (i < j) then
          ! exchange A(i) and A(j)
          temp = A(i)
          A(i) = A(j)
          A(j) = temp
       elseif (i == j) then
          marker = i+1
          return
       else
          marker = i
          return
       endif
    end do

  end subroutine Partition_Real
  
  recursive subroutine quicksort_int(A)
    integer, intent(in out), dimension(:) :: A
    integer :: iq
    
    if(size(A) > 1) then
       call Partition_int(A, iq)
       call quicksort_int(A(:iq-1))
       call quicksort_int(A(iq:))
    endif
  end subroutine quicksort_int
  
  subroutine Partition_int(A, marker)
    integer, intent(in out), dimension(:) :: A
    integer, intent(out) :: marker
    integer :: i, j
    integer :: temp
    integer :: x      ! pivot point
    x = A(1)
    i= 0
    j= size(A) + 1

    do
       j = j-1
       do
          if (A(j) <= x) exit
          j = j-1
       end do
       i = i+1
       do
          if (A(i) >= x) exit
          i = i+1
       end do
       if (i < j) then
          ! exchange A(i) and A(j)
          temp = A(i)
          A(i) = A(j)
          A(j) = temp
       elseif (i == j) then
          marker = i+1
          return
       else
          marker = i
          return
       endif
    end do

  end subroutine Partition_Int

end module sort
