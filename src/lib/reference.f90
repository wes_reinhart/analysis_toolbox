module reference
  use datatypes
  use io
  use celllist
  use cluster
  use compute
  use cna
  use util
  implicit none

contains

  subroutine read_ref_entries_cna(parameters,identities)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: identities
    ! internal variables
    integer, parameter :: f_ref = 110
    logical :: ref_exist
    integer :: ierr = 0
    character(len=1000) :: fline, name

    integer :: n_ref, max_particles
    integer :: i, j, k, s, e
    integer, dimension(max_pairs) :: raw_input, counts, sig

    max_particles = 0
    n_ref = 0

    ! check if there is a reference file specified
    inquire(file=(trim(reference_file)), exist=ref_exist)
    if( .not. ref_exist ) then

       write(*,'("could not open CNA reference file ",A)') trim(reference_file)

       num_structures = 0
       ref_size = num_structures + n_ref_buffer
       call setup_identities(parameters,identities)

    else

       write(*,'("reading reference CNA signatures from file ",A)') trim(reference_file)

       ! open the file
       open(unit=f_ref,file=trim(reference_file),action='read',status='old')

       ! first check the number of reference structures
       !     and the largest unit cell
       do while( ierr .eq. 0 )

          ! skip over blank lines
          fline = ""
          do while( index(fline,":") .eq. 0 .and. ierr .eq. 0 )
             read(f_ref,'(A)',iostat=ierr) fline
          end do

          if( ierr .eq. 0 ) then
             n_ref = n_ref + 1
          end if

       end do

       num_structures = n_ref
       ref_size = num_structures + n_ref_buffer

       call setup_identities(parameters,identities)

       write(*,'("there are ",I0," reference entries in the file")') num_structures

       rewind(f_ref)
       ierr = 0

       j = 0
       do while( ierr .eq. 0 )

          j = j + 1

          ! skip over blank lines
          fline = ""
          do while( index(fline,":") .eq. 0 .and. ierr .eq. 0 )
             read(f_ref,'(A)',iostat=ierr) fline
          end do

          if( ierr .eq. 0 ) then

             raw_input = 0
             counts = 0
             sig = 0

             ! parse name
             s = 1
             e = index( fline, ":")
             name = trim( fline(1:(e-1)) )
             write(*,'(A," : ")',advance='no') trim(name)
             s = e

             i = 0
             k = 1
             ! parse signature
             do while( k .gt. 0 )
                i = i + 1
                ! find count
                s = scan( fline(s:), "0123456789" ) + s - 1
                e = scan( fline(s:), " ") + s
                read(fline(s:e),*) counts(i)
                s = e
                ! find signature
                s = scan( fline(s:), "0123456789" ) + s - 1
                e = scan( fline(s:), " ") + s
                read(fline(s:e),*) sig(i)
                s = e
                ! report
                write(*,'(I0,"x",I0," ")',advance='no') counts(i), sig(i)
                ! next loop
                k = scan( fline(s:), "0123456789" )
             end do
             write(*,*)

             identities%name(j) = trim(name)
             identities%cna(j)%counts(1:i) = counts(1:i)
             identities%cna(j)%sig(1:i) = sig(1:i)

          end if

       end do

       ! close the file
       close(f_ref)

    end if

  end subroutine read_ref_entries_cna

  subroutine read_ref_entries_unit_cell(parameters,identities)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: identities
    ! internal variables
    type (system_struct) :: unitcell, system
    type (cluster_struct) :: nl
    type (cna_struct), dimension(:), allocatable :: cna_list
    integer, parameter :: f_ref = 110
    logical :: ref_exist
    integer :: ierr = 0
    character(len=1000) :: fline, name

    integer :: cutoff_flag

    integer :: n_ref, max_particles, n_particles
    integer :: i, j, k, m, n
    integer :: particle_id
    real(P), dimension(3) :: box_length

    real(P), dimension(max_pairs) :: angles

    max_particles = 0
    n_ref = 0

    ! check if there is a reference file specified
    inquire(file=(trim(reference_file)), exist=ref_exist)
    if( ref_exist ) then

       ! open the file
       open(unit=f_ref,file=trim(reference_file),action='read',status='old')

       ! first check the number of reference structures
       !     and the largest unit cell
       do while( ierr .eq. 0 )

          ! find next entry
          fline = ""
          do while( index(fline,"structure") .eq. 0 .and. ierr .eq. 0 )
             read(f_ref,'(A)',iostat=ierr) fline
          end do

          if( ierr .eq. 0 ) then

             ! increment counter
             read(f_ref,'(A)',iostat=ierr) fline
             n_ref = n_ref + 1

             ! parse data on second line
             read(f_ref,'(A)',iostat=ierr) fline
             read(fline,*) n_particles, box_length

             if( n_particles .gt. max_particles ) max_particles = n_particles

          end if

       end do

       num_structures = n_ref
       ref_size = num_structures + n_ref_buffer

       call setup_identities(parameters,identities)

       ierr = 0
       rewind(f_ref)

       ! allocate unitcell
       allocate( unitcell%position(3,max_particles) )

       k = 0

       ! loop until end of file
       do while( ierr .eq. 0 )

          ! find next entry
          fline = ""
          do while( index(fline,"structure") .eq. 0 .and. ierr .eq. 0 )
             read(f_ref,'(A)',iostat=ierr) fline
          end do

          if( ierr .eq. 0 ) then

             ! parse data on first line
             read(f_ref,'(A)',iostat=ierr) fline
             read(fline,'(A)') name

             ! parse data on second line
             read(f_ref,'(A)',iostat=ierr) fline
             read(fline,*) unitcell%num_particles, unitcell%box_length

             ! print*,''
             ! write(*,'(A," : ",I0," : ",3F10.5)') &
             !      trim(identities%name(k)), unitcell%num_particles, unitcell%box_length

             ! parse particle data
             do j = 1, unitcell%num_particles
                read(f_ref,'(A)',iostat=ierr) fline
                if( ierr .ne. 0 ) return
                read(fline,*) unitcell%position(:,j)
                ! write(*,'(3F10.5)') unitcell%position(:,j)
             end do

             ! build the reference system from the unit cell
             call build_box_from_unitcell(unitcell,system)
             call build_neighbor_list_cell_list(parameters,system,nl)
             call sort_neighbor_list(system,nl)
             call get_nearby_neighbors(system,parameters,nl,cutoff_flag)

             ! evaluate the reference system
             if( allocated( cna_list ) ) deallocate( cna_list )
             allocate(cna_list(system%num_particles))
             do i = 1, system%num_particles
             ! i = 1
                call common_neighbor_analysis(i,system,nl,cna_list(i))
             end do
             parameters%cna_tol = 0
             call find_clusters_cna(parameters,system,nl,cna_list)

             do m = 1, (nl%num_clusters - 1)

                k = k + 1

                if( nl%num_clusters .gt. 2 ) then
                   write(identities%name(k),'(A,"_",I0)') trim(name), (m-1)
                else
                   identities%name(k) = trim(name)
                end if

                ! take the first particle from each cluster as representative
                ! particle_id = nl%map(1,m)
                particle_id = minloc( nl%reverse_map, 1, nl%reverse_map .eq. m )

                ! assign the signature based on clustering
                identities%cna(k) = cna_list( particle_id )

                ! report
                write(*,'(A,": ")',advance='no') trim(identities%name(k))
                do j = 1, count(identities%cna(k)%counts .gt. 0)
                   write(*,'(I0,"x",I0," ")',advance='no') &
                        identities%cna(k)%counts(j), identities%cna(k)%sig(j)
                end do
                write(*,*)

                ! see if automatic bond angles are reliable
                angles = list_bond_angles(particle_id, nl%neighbor_map(1:nl%N0(particle_id),particle_id), system, nl)

                n = count( angles .ge. -1 )
                call quicksort_real(angles(1:n))

                identities%baa(k)%angles(1:n) = angles(1:n)

             end do

          end if

       end do

       ! close the file
       close(f_ref)

    end if

  end subroutine read_ref_entries_unit_cell

  subroutine build_box_from_unitcell(unitcell,system)
    implicit none
    ! external variables
    type (system_struct), intent(inout) :: unitcell, system
    ! internal variables
    integer, parameter :: min_particles = 100
    integer, parameter :: max_particles = 200
    real(P), parameter :: min_length = 6.0
    integer, dimension(3) :: n_repeat
    integer :: max_id
    integer :: i, j, k, l, n

    ! isotropically expand the box to minimum size
    n_repeat = ceiling( min_length / unitcell%box_length )
    do while( product(n_repeat) * unitcell%num_particles .lt. min_particles )
       n_repeat = n_repeat + 1
    end do

    ! anisotropically shrink the box to maximum size
    do while( product(n_repeat)*unitcell%num_particles .gt. max_particles )
       max_id = maxloc(n_repeat*unitcell%box_length, 1)
       n_repeat( max_id ) = n_repeat( max_id ) - 1
    end do

    ! write(*,'("unitcell will be replicated ",I0,"x",I0,"x",I0," for a total of ",I0," particles")') &
    !      n_repeat, product(n_repeat)*unitcell%num_particles

    ! construct the box
    system%num_particles = product(n_repeat)*unitcell%num_particles
    system%box_length    = unitcell%box_length * n_repeat
    if( allocated( system%position ) ) deallocate( system%position )
    allocate( system%position( 3, system%num_particles ) )

    n = 0
    do i = 1, n_repeat(1)
       do j = 1, n_repeat(2)
          do k = 1, n_repeat(3)
             do l = 1, unitcell%num_particles
                n = n + 1
                system%position(:,n) = unitcell%position(:,l) &
                     + (/i-1,j-1,k-1/)*unitcell%box_length
             end do
          end do
       end do
    end do

    ! wrap all particles back inside
    do i = 1, system%num_particles
       do k = 1, 3

          do while( system%position(k,i) .lt. -system%box_length(k) * 0.5_P )
             system%position(k,i) = system%position(k,i) + system%box_length(k)
          end do

          do while( system%position(k,i) .gt. system%box_length(k) * 0.5_P )
             system%position(k,i) = system%position(k,i) - system%box_length(k)
          end do

       end do
    end do

  end subroutine build_box_from_unitcell

end module reference
