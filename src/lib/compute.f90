module compute
  use datatypes
  use util
  use sort
  implicit none

contains

  !========================================!
  !     merge two copies of baa struct     !
  !========================================!  
  subroutine merge_reference_structures(parameters,local_identities,global_identities)
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: local_identities, global_identities
    ! internal varaibles
    type (identity_struct) :: temp_identities
    integer :: i, j, temp_count, match_id, match_count
    real(P) :: temp_p
    real(P), dimension(max_pairs) :: temp_angles

    ! keep a buffer of identity structs
    do while( num_local_structures + num_structures .ge. ref_size )
       write(*,'("expanding reference library from ",I0," to ",I0," entries")') &
            ref_size, ref_size + n_ref_buffer
       ref_size = ref_size + n_ref_buffer
       ! create a new identity struct with buffer space
       call copy_identities(parameters,global_identities,temp_identities)
       ! replace the global_identities with the new resized one
       global_identities = temp_identities
    end do
    
    do i = 1, num_local_structures ! size( local_identities%baa )

       ! find the correspondence between local and global reference structures
       match_id = 0
       ! loop over global reference structures
       !   (stopping when a perfect match is found)
       j =  0
       do while( j .lt. num_structures .and. match_id .eq. 0 &
            .and. num_structures .gt. 0)
          j = j + 1
          ! check for perfect match
          !   i for local, j for global
          match_count = count_cna_matches( local_identities%cna(i), global_identities%cna(j) )
          if( match_count .eq. sum(local_identities%cna(i)%counts) &
               .and. match_count .eq. sum(global_identities%cna(j)%counts) ) then
             match_id = j
          end if
       end do
       if( match_id .eq. 0 ) then
          ! no match found, create a new entry
          num_structures = num_structures + 1
          match_id = num_structures
          
          ! add the new cna signatures
          global_identities%cna(match_id) = local_identities%cna(i)
          global_identities%name(match_id) = local_identities%name(i)
       end if

       ! add this match to the local->global map
       global_identities%global_map(i,local_identities%frame_id) = match_id

       ! add the new angles with the appropriate weight
       temp_angles = &
            real(global_identities%baa(match_id)%num_obs,P) * global_identities%baa(match_id)%angles &
            + real(local_identities%baa(i)%num_obs,P) * local_identities%baa(i)%angles

       ! merge self-similarity with appropriate weight
       temp_p = real(global_identities%baa(match_id)%num_obs,P) * global_identities%p(match_id) &
            + real(local_identities%baa(i)%num_obs,P) * local_identities%p(i)
       
       ! tally the new obs count
       temp_count = global_identities%baa(match_id)%num_obs + local_identities%baa(i)%num_obs
       global_identities%baa(match_id)%num_obs = temp_count
       
       ! normalize the values by the new total
       if( temp_count .gt. 0 ) then
          global_identities%baa(match_id)%angles = temp_angles / real(temp_count,P)
          global_identities%p(match_id) = temp_p / real(temp_count,P)
       else
          global_identities%baa(match_id)%angles = 0.0_P
          global_identities%p(match_id) = 0.0_P
       end if

       ! tally the new total count
       temp_count = global_identities%baa(match_id)%num_total + local_identities%baa(i)%num_total
       global_identities%baa(match_id)%num_total = temp_count
       
    end do
       
  end subroutine merge_reference_structures

  !===============================!
  !     reset identity struct     !
  !===============================!
  subroutine reset_identities(identities)
    ! external variables
    type (identity_struct), intent(inout) :: identities
    ! internal varaibles
    integer :: i

    identities%num_structures = 0
    ! write(*,'("size of identities is ",I0)') size( identities%baa )
    do i = 1, size( identities%baa )
       identities%baa(i)%angles = -infinity
       identities%baa(i)%num_obs = 0
       identities%baa(i)%num_total = 0
    end do
    do i = 1, size( identities%cna )
       identities%cna(i)%sig    = 0
       identities%cna(i)%counts = 0
    end do
    do i = 1, size( identities%p )
       identities%p(i) = 0.0_P
    end do
  end subroutine reset_identities
  
  !================================================!
  !     wrapper for the optimal_match function     !
  !================================================!
  real(P) function match_residual(A,B)
    implicit none
    ! external variables
    real(P), dimension(:), intent(in) :: A, B
    ! internal variables
    real(P), dimension(:), allocatable :: long, short, residual
    integer, dimension(:), allocatable :: assigned_ids
    integer :: len_A, len_B
    integer :: i, r, l, s

    len_A = count( A .gt. -100.0 )
    len_B = count( B .gt. -100.0 )
    
    if( len_A .gt. len_B ) then
       ! A is larger than B
       r = ceiling( real(len_A,P) / real(len_B,P) )
       l = len_B
       s = len_A
    else
       ! B is larger than A (or same size)
       r = ceiling( real(len_B,P) / real(len_A,P) )
       l = len_A
       s = len_B
    end if

    if( l .le. 0 .or. s .le. 0 .or. r .le. 0 ) then
       ! print*,l,s,r
       ! print*,'cannot have a zero value in match_residual!'
       match_residual = infinity
       return
    end if
    
    allocate( short(s) )
    allocate( long(l*r) )
    allocate( assigned_ids(s) )
    allocate( residual(s) )

    if( len_A .gt. len_B ) then
       ! A is larger than B
       do i = 1, r
          long( ((i-1)*l+1):(i*l) ) = B(1:len_B)
       end do
       short = A(1:len_A)
    else
       ! B is larger than A (or same size)
       do i = 1, r
          long( ((i-1)*l+1):(i*l) ) = A(1:len_A)
       end do
       short = B(1:len_B)
    end if
    
    assigned_ids = optimal_match(short,long)

    do i = 1, s
       residual(i) = long(assigned_ids(i)) - short(i)
    end do

    match_residual = sqrt( sum( residual ** 2.0_P ) / real(size(short),P) )

  end function match_residual

  !======================================================!
  !     list all the bond angles of a given particle     !
  !======================================================!  
  function list_bond_angles(particle_id,N0_list,system,nl)
    implicit none
    real(P), dimension(max_pairs) :: list_bond_angles
    ! external variables
    integer, intent(in) :: particle_id
    integer, dimension(:), intent(in) :: N0_list
    type (system_struct),  intent(inout) :: system
    type (cluster_struct), intent(inout) :: nl
    ! internal variables
    integer :: j, k, N0, n
    real(P), dimension(3) :: r_i, r_j, r_k, r_ij, r_ik
    real(P) :: r0, dot, r_ij_length, r_ik_length

    list_bond_angles = -100
    
    N0 = size(N0_list)

    ! get adaptive cutoff radius
    r0 = sqrt( sum(nl%neighbor_dist(1:6,particle_id)**2.0_P) / 6.0_P )
    
    n = 0
    ! retrieve particle data
    r_i = system%position(:,particle_id)
    ! loop over first-shell neighbor pairs
    do j = 1, (N0-1)
       r_j  = system%position(:,N0_list(j))
       r_ij = displacement(system,r_i,r_j)
       r_ij_length = sqrt( sum( r_ij**2.0_P ) )
       do k = (j+1), N0
          r_k  = system%position(:,N0_list(k))
          r_ik = displacement(system,r_i,r_k)
          r_ik_length = sqrt( sum( r_ik**2.0_P ) )
          ! bond angle from dot product
          dot = dot_product(r_ij,r_ik) / r_ij_length / r_ik_length
          ! save all the bond angles
          n = n + 1
          list_bond_angles(n) = dot
       end do
    end do

  end function list_bond_angles
  
  !==========================================!
  !     compute normal cdf of some value     !
  !==========================================!
  real(P) function normal_cdf(x,mu,sigma)
    implicit none
    real(P), intent(in) :: x, mu, sigma
    normal_cdf = 0.5_P * ( 1.0_P + erf( (x-mu) / (sigma*sqrt(2.0_P)) ) )
  end function normal_cdf
  
  !========================================!
  !     apply the Ackland BAA analysis     !
  !========================================!
  subroutine bond_angles(particle_id,system,nl,chi)
    implicit none
    ! external variables
    integer, intent(in) :: particle_id
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: nl
    integer, dimension(10),   intent(out)   :: chi
    ! internal variables
    integer :: k
    integer :: N0, N1
    real(P) :: r0
    real(P) :: d_bcc, d_cp, d_fcc, d_hcp
    real(P), dimension(8) :: h_fcc, h_hcp, h_bcc

    ! ideal crystal histograms
    h_fcc = real( (/6,0,0,24,12,0,24,0/), P )
    h_hcp = real( (/3,0,6,21,12,0,24,0/), P )
    h_bcc = real( (/7,0,0,36,12,0,36,0/), P )
    
    nl%type(particle_id) = -1
    
    k = nl%neighbor_count(particle_id)

    ! need at least 6 neighbors to define a cutoff radius
    if( k .lt. 6 ) then
       write(*,'("particle ",I0," has fewer than six neighbors! (",I0,")")') particle_id, k
       return
    else
       ! get adaptive cutoff radius
       r0 = sqrt( sum(nl%neighbor_dist(1:6,particle_id)**2.0_P) / 6.0_P )
    end if
    nl%cutoff(particle_id) = r0

    ! find the neighbors that fall in the first shell
    N0 = count(nl%neighbor_dist(1:k,particle_id) .le. r0_cutoff * r0)

    ! find the neighbors that fall in the second shell
    N1 = count(nl%neighbor_dist(1:k,particle_id) .le. 1.2450_P * r0)

    ! chi = angle_histogram(particle_id,nl%neighbor_map(1:N0,particle_id),system,nl)
    call get_angle_histogram(particle_id,nl%neighbor_map(1:N0,particle_id),system,nl,chi)

    d_bcc = 0.35_P * chi(5) / ( chi(6) + chi(7) + chi(8) - chi(5) )
    d_cp  = abs( 1.0_P - chi(7) / 24.0_P )
    d_fcc = 0.61_P * ( abs( chi(1) + chi(2) - 6.0_P ) + chi(3) ) / 6.0_P
    d_hcp = ( abs(chi(1) - 3.0_P) + abs(chi(1) + chi(2) + chi(3) + chi(4) - 9.0_P) ) / 12.0_P
    
    if( chi(1) .eq. 7 ) then
       d_bcc = 0
    elseif( chi(1) .eq. 6 ) then
       d_fcc = 0
    elseif( chi(1) .le. 3 ) then
       d_hcp = 0
    end if

    if( chi(8) .gt. 0 ) then
       nl%type(particle_id) = 0
       return
    elseif( chi(5) .lt. 3 ) then
       if( N1 .gt. 13 .or. N1 .lt. 11 ) then
          nl%type(particle_id) = 0
          return
       else
          nl%type(particle_id) = 4
          return
       end if
    elseif( d_bcc .le. d_cp ) then
       if( N1 .lt. 11 ) then
          nl%type(particle_id) = 0
          return
       else
          nl%type(particle_id) = 3
          return
       end if
    elseif( N1 .gt. 12 .or. N1 .lt. 11 ) then
       nl%type(particle_id) = 0
       return
    elseif( d_fcc .lt. d_hcp ) then
       nl%type(particle_id) = 1
       return
    else
       nl%type(particle_id) = 2
       return
    end if

  end subroutine bond_angles

  !==================================================!
  !     compute the Ackland bond angle histogram     !
  !==================================================!
  subroutine get_angle_histogram(particle_id,N0_list,system,nl,chi)
    implicit none
    ! integer, dimension(10) :: angle_histogram
    ! external variables
    integer, intent(in) :: particle_id
    integer, dimension(:), intent(in) :: N0_list
    type (system_struct),  intent(inout) :: system
    type (cluster_struct), intent(inout) :: nl
    integer, dimension(8),  intent(inout) :: chi
    ! internal variables
    real(P), dimension(9) :: bins
    integer :: j, k, N0, bin_id
    real(P), dimension(3) :: r_i, r_j, r_k, r_ij, r_ik
    real(P) :: r0, dot, r_ij_length, r_ik_length
    integer, dimension(8) :: angle_histogram

    angle_histogram = 0
    bins = (/-1.01,-0.945,-0.915,-0.755,-0.195,0.195,0.245,0.795,1.01/)

    N0 = size(N0_list)

    ! get adaptive cutoff radius
    r0 = sqrt( sum(nl%neighbor_dist(1:6,particle_id)**2.0_P) / 6.0_P )
    
    ! retrieve particle data
    r_i = system%position(:,particle_id)
    ! loop over first-shell neighbor pairs
    do j = 1, (N0-1)
       r_j  = system%position(:,N0_list(j))
       r_ij = displacement(system,r_i,r_j)
       r_ij_length = sqrt( sum( r_ij**2.0_P ) )
       do k = (j+1), N0
          r_k  = system%position(:,N0_list(k))
          r_ik = displacement(system,r_i,r_k)
          r_ik_length = sqrt( sum( r_ik**2.0_P ) )
          ! bond angle from dot product
          dot = dot_product(r_ij,r_ik) / r_ij_length / r_ik_length

          ! round to three decimal places
          dot = real( nint( dot * 1000.0_P ), P ) / 1000.0_P

          ! find bin and increment histogram
          bin_id = minloc(bins, 1, bins .gt. dot) - 1
          if( bin_id .lt. 1 ) then
             print*,'bin_id',bin_id
          end if
          angle_histogram(bin_id) = angle_histogram(bin_id) + 1
       end do
    end do

    chi = angle_histogram
    
  end subroutine get_angle_histogram
  
end module compute
