!=================================================================!
!     contains the derived data types used by all the modules     !
!=================================================================!
module datatypes
  implicit none

  !==============================================!
  !     define the level of precision to use     !
  !==============================================!
  integer, parameter :: P = kind(1.d0)
  integer, parameter :: max_cells = 100
  integer, parameter :: recursion_limit = 10000
  integer :: recursion_depth

  !=======================================================!
  !     make the value of pi available to all modules     !
  !=======================================================!
  real(P), parameter :: pi = 4.0_P*atan(1.0_P)
  real(P), parameter :: infinity = real(1e25,P)
  real(P), parameter :: sigma = 0.10
  real(P), parameter :: min_prob_diff = 0.01
  integer, parameter :: max_pairs = 250
  integer, parameter :: max_levels = 1
  integer, parameter :: min_cluster_size = 0
  integer, parameter :: clustering_shells = 0
  integer, parameter :: type_len = 10
  character(len=100), parameter :: reference_file = "reference_cna.dat"
  real(P) :: r0_cutoff
  integer :: num_nn_avg
  integer, parameter :: n_ref_buffer = 100
  integer :: num_structures = 0
  integer :: num_local_structures = 0
  integer :: ref_size = 0

  !=====================================================================!
  !     holds parameters which are frequently referenced by modules     !
  !=====================================================================!
  type :: parameter_struct
     character(len=100) :: run_name
     character(len=100) :: run_dir
     ! name of text file listing all snapshots
     character(len=1000) :: snapshot_file
     ! name of config file, if one exists
     character(len=1000) :: config_file
     ! number of frames in snapshot_file
     integer :: num_frames = 0
     ! initial number of particles
     integer :: num_particles = 0
     ! number of patches on particles
     integer :: num_patches = 0
     ! cell size
     real(P) :: cell_cutoff = 1.0_P
     real(P) :: neighbor_cutoff = 1.0_P
     ! tolerance for clustering
     integer :: cna_tol = 0
     ! rmsd for assigned particles
     real(P) :: mean_structure_rmsd = 0.0_P
     real(P) :: median_structure_rmsd = 0.0_P
     ! default to less than zero
     real(P) :: tree_cutoff = -1.0_P
     ! should we do map analysis?
     logical :: map_construction = .false.
  end type parameter_struct

  !=================================!
  !     holds one cna signature     !
  !=================================!
  type :: cna_struct
     integer, dimension(max_pairs) :: sig = 0
     integer, dimension(max_pairs) :: counts = 0
     character(len=type_len), dimension(max_pairs) :: type
  end type cna_struct

  !======================================!
  !     holds bond angle definitions     !
  !======================================!
  type :: bond_angle_struct
     real(P), dimension(max_pairs) :: angles = -infinity
     integer :: num_obs = 0
     integer :: num_total = 0
  end type bond_angle_struct

  !========================================================!
  !     holds all the information about classification     !
  !========================================================!
  type :: identity_struct
     character(100), dimension(:), allocatable :: name
     type (bond_angle_struct), dimension(:), allocatable :: baa
     type (cna_struct), dimension(:), allocatable :: cna
     real(P), dimension(:), allocatable :: p
     integer, dimension(:,:), allocatable :: global_map
     integer :: frame_id
     integer :: num_structures
  end type identity_struct

  !========================================================!
  !     holds all the information about classification     !
  !========================================================!
  type :: reference_struct
     character(10) :: name
     type (bond_angle_struct) :: baa
     type (cna_struct) :: cna
  end type reference_struct

  !=============================================================!
  !     holds all the information about multi-atom patterns     !
  !=============================================================!
  type :: multiatom_struct

     integer, dimension(:), allocatable :: all_assoc_id

     integer :: center_id = 0
     character(len=type_len) :: center_type
     type (cna_struct) :: center_cna

     integer, dimension(max_pairs) :: edge_id = 0
     character(len=type_len), dimension(max_pairs) :: edge_type
     type (cna_struct), dimension(max_pairs) :: edge_cna

     integer, dimension(max_pairs) :: tess_id = 0
     character(len=type_len), dimension(max_pairs) :: tess_type
     type (cna_struct), dimension(max_pairs) :: tess_cna

     integer, dimension(:), allocatable :: all_edge_id, all_center_id, all_tess_id

  end type multiatom_struct

  !====================================================!
  !     holds data for the hierarchical clustering     !
  !====================================================!
  type :: hierarchy_struct
     ! number of clusters
     integer :: n = 0
     ! current end of Y and Yprime vectors
     integer :: m = 0
     ! size of the linkage vector
     integer :: l = 0
     ! 1d vector of distances
     real(P), dimension(:), allocatable :: Y
     ! is this cluster still alive?
     integer, dimension(:), allocatable :: Yprime
     ! record linkages
     integer, dimension(:,:), allocatable :: Zid
     ! record linkage heights
     real(P), dimension(:), allocatable :: Zheight
     ! record leaves recursively
     integer, dimension(:,:), allocatable :: leaves, reduced_leaves
     ! record disallowed values
     integer, dimension(:), allocatable :: disallowed
     ! reverse mapping from leaf to coarse id
     integer, dimension(:), allocatable :: reverse_map
     ! keep track of self-similarity
     real(P), dimension(:), allocatable :: selfsim
     ! keep track of cluster sizes
     integer, dimension(:), allocatable :: counts
  end type hierarchy_struct

  !========================================!
  !     holds cell list for the system     !
  !========================================!
  type :: cell_list_struct
     ! number of cells in each dimension
     integer, dimension(3) :: cell_dim
     ! the dimensions of the cell
     real(P), dimension(3) :: side_length
     ! how much memory we allocated to the cell
     integer :: max_occupancy
     ! cell_count stores the number of particles in each cell
     integer, dimension(:),   allocatable :: cell_count
     ! particle ids for cell j are stored in column j of cell_map
     ! column j of cell_map contains cell_count(j) entries
     integer, dimension(:,:), allocatable :: cell_map
     ! what the system looked like when the list was built
     real(P), dimension(:,:), allocatable :: snapshot
     ! list of all the neighbors for each cell
     integer, dimension(:,:), allocatable :: neighbors
  end type cell_list_struct

  !==============================================!
  !     holds the data for cluster recursion     !
  !==============================================!
  type :: cluster_struct
     ! stores the current location in the search tree
     integer :: particle_id
     ! number of clusters
     integer :: num_clusters
     ! maximum size of neighbor list
     integer :: max_occupancy
     ! cluster index and size
     integer, dimension(2)   :: stats
     ! crystal type
     integer, dimension(:), allocatable :: type
     ! likelihood of each structure
     real(P), dimension(:), allocatable :: likelihood
     ! common neighbor analysis signature
     integer, dimension(:,:), allocatable :: cna
     ! adaptive neighbor cutoff
     real(P), dimension(:), allocatable :: cutoff
     ! number of particles inside the adaptive neighbor cutoff
     integer, dimension(:), allocatable :: N0
     ! binary map of whether particle_id has been searched
     integer, dimension(:),   allocatable :: particle_list
     ! maps clusters in form [particle_id, cluster_id]
     ! integer, dimension(:,:), allocatable :: map
     ! gives the cluster id for each particle
     integer, dimension(:), allocatable :: reverse_map
     ! maps neighbors in form [neighbor_id, particle_id]
     integer, dimension(:,:), allocatable :: neighbor_map
     ! distance to neighbors in form [neighbor_id, particle_id]
     real(P), dimension(:,:), allocatable :: neighbor_dist
     ! number of neighbors for particle
     integer, dimension(:),   allocatable :: neighbor_count
     ! center of mass for each cluster
     real(P), dimension(:,:), allocatable :: com
  end type cluster_struct

  !==================================================!
  !     holds all the mutable system information     !
  !==================================================!
  type :: system_struct
     logical :: unary_system
     integer :: num_particles
     real(P), dimension(3)                  :: box_length
     real(P), dimension(:,:),   allocatable :: position
     real(P), dimension(:,:,:), allocatable :: orientation
     character(len=type_len), dimension(:), allocatable :: type
     type (cell_list_struct) :: cell_list
  end type system_struct

  !==========================================================================!
  !     set up a data structure for the initial configuration generation     !
  !==========================================================================!
  type :: lattice_struct
     ! total number of lattice sites
     integer :: num_sites
     ! number of currently available sites
     integer :: num_available_sites
     ! size of the stencil
     integer :: num_stencil
     ! lattice spacing in (x,y,z)
     real(P), dimension(3) :: delta
     ! number of lattice sites in (x,y,z)
     integer, dimension(3) :: dimensions
     ! number of stencil sites in (x,y,z)
     integer, dimension(3) :: stencil_dim
     ! list of sites with availability
     integer, dimension(:),   allocatable :: map
     ! which surrounding sites are occupied by a particle, in 3D
     integer, dimension(:,:), allocatable :: stencil
     ! list of the site id's for the stencil, in 1D
     integer, dimension(:),   allocatable :: neighbors
  end type lattice_struct

end module datatypes
