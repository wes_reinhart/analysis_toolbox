module io
  use datatypes
  use util
  use celllist
  use cna
  implicit none

contains

  !=========================================================================!
  !     parse command-line arguments and save them to parameters struct     !
  !=========================================================================!
  subroutine parse_cmdline(parameters)
    implicit none
    ! external variables
    type (parameter_struct), intent(out) :: parameters
    ! internal variables
    character(len=100) :: temp_char
    integer :: len, status, dims
    call get_command_argument(1,parameters%snapshot_file,len,status)
    if( status .ne. 0 ) then
       write(*,'("could not read snapshot file from command line")')
       stop
    end if
    call get_command_argument(2,temp_char,len,status)
    ! catch problems with the (optional) second command line argument
    if( status .ne. 0 .or. &
         len .eq. 0 .or. &
         verify( trim(temp_char), "0123456789." ) .ne. 0 ) then
       parameters%neighbor_cutoff = 1.0
    else
       read(temp_char,*) parameters%neighbor_cutoff
    end if
    call get_command_argument(3,temp_char,len,status)
    ! catch problems with the (optional) third command line argument
    if( status .ne. 0 .or. &
         len .eq. 0 .or. &
         verify( trim(temp_char), "0123456789." ) .ne. 0 ) then
       ! assume 3D unless otherwise specified
       num_nn_avg = 6
       r0_cutoff  = 1.2071_P
    else
       read(temp_char,*) dims
       if( dims .eq. 2 ) then
          num_nn_avg = 3
          r0_cutoff  = 1.5366_P
          ! r0_cutoff = 1.3660_P
          ! r0_cutoff = 1.2866_P
       elseif( dims .eq. 3 ) then
          num_nn_avg = 6
          r0_cutoff  = 1.2071_P
       else
          write(*,'("dims argument must be 2 or 3, got ",I0)') dims
          stop
       end if
    end if
    write(*,'("using ",I0," nn x ",F10.5)') num_nn_avg, r0_cutoff
    ! catch problems with optional -map argument
    call get_command_argument(4,temp_char,len,status)
    if( status .eq. 0 .and. len .gt. 0 ) then
       if( index(temp_char,"-map") .gt. 0 ) then
          parameters%map_construction = .true.
       end if
    end if
  end subroutine parse_cmdline

  !=========================================================================!
  !     parse command-line arguments and save them to parameters struct     !
  !=========================================================================!
  subroutine parse_cmdline_hierarchy(parameters)
    implicit none
    ! external variables
    type (parameter_struct), intent(out) :: parameters
    ! internal variables
    character(len=100) :: temp_char
    integer :: len, status
    call get_command_argument(1,parameters%snapshot_file,len,status)
    if( status .ne. 0 ) then
       write(*,'("could not read snapshot file from command line")')
       stop
    end if
    call get_command_argument(2,temp_char,len,status)
    ! catch problems with the (optional) second command line argument
    if( status .ne. 0 .or. &
         len .eq. 0 .or. &
         verify( trim(temp_char), "0123456789." ) .ne. 0 ) then
       parameters%tree_cutoff = -1.0
    else
       read(temp_char,*) parameters%tree_cutoff
    end if
  end subroutine parse_cmdline_hierarchy

  !=================================================!
  !     acquire initial configuration from file     !
  !=================================================!
  subroutine acquire_config(parameters,system)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    ! internal variables
    logical :: prev_config

    ! check if there is a configuration file specified
    inquire(file=(trim(parameters%config_file)), exist=prev_config)
    if( prev_config ) then
       if( index(parameters%config_file,".xml") .gt. 0 ) then
          call read_XML(parameters,system)
       elseif( index(parameters%config_file,".xyz") .gt. 0 ) then
          call read_XYZ(parameters,system)
       else
          write(*,'("Error: file format not recognized: ",A)') trim(parameters%config_file)
          stop
       end if
    else
       write(*,'(A,A)') "Specified configuration does not exist: ", trim(parameters%config_file)
       stop
    end if

  end subroutine acquire_config

  !=============================================================!
  !     get minimum information to do cluster agglomeration     !
  !=============================================================!
  subroutine acquire_hierarchy_info(parameters,nl)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (cluster_struct),   intent(inout) :: nl
    ! internal variables
    type (system_struct) :: system
    logical :: prev_config

    ! check if there is a configuration file specified
    inquire(file=(trim(parameters%config_file)), exist=prev_config)
    if( prev_config ) then

       if( index(parameters%config_file,".xml") .gt. 0 ) then
          call read_XML(parameters,system)
       elseif( index(parameters%config_file,".xyz") .gt. 0 ) then
          call read_XYZ(parameters,system)
       else
          write(*,'("Error: file format not recognized: ",A)') trim(parameters%config_file)
          stop
       end if

       nl%num_clusters = system%num_particles

       ! print information to command line
       write(*,'("Read the specified file: ",A," -> N = ",I0)') &
            trim(parameters%config_file), nl%num_clusters

       ! setup neighborlist types
       if( allocated( nl%type ) ) deallocate( nl%type )
       allocate( nl%type( nl%num_clusters ) )
       nl%type = 0

       ! setup reference structures
       num_structures = 0
       ref_size = num_structures + n_ref_buffer

    else

       write(*,'("Specified configuration does not exist: ",A)') trim(parameters%config_file)
       stop

    end if

  end subroutine acquire_hierarchy_info

  !============================!
  !     read XYZ from file     !
  !============================!
  subroutine read_XYZ(parameters,system)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    ! internal variables
    integer :: f_xml = 100
    character(len=200) :: fline
    integer :: i

    print*,trim(parameters%config_file)

    ! open the file
    open(unit=f_xml,file=trim(parameters%config_file),action='read',status='old')

    ! first line has the number of particles
    read(f_xml,'(A)') fline
    read(fline,*) system%num_particles

    ! second line has the box size
    read(f_xml,'(A)') fline
    read(fline,*) system%box_length

    ! print information to command line
    write(*,'(A,A)') "Read the specified file: ", trim(parameters%config_file)
    write(*,'("N = ",I0,"; L = (",F10.5,",",F10.5,",",F10.5,")")') system%num_particles, system%box_length

    ! allocate memory for the particle position within the system struct
    if( allocated( system%position ) ) deallocate( system%position )
    allocate( system%position(3,system%num_particles) )

    ! allocate memory for the particle type within the system struct
    if( allocated( system%type ) ) deallocate( system%type )
    allocate( system%type(system%num_particles) )

    ! read each particle's position
    do i = 1, system%num_particles
       read(f_xml,'(A)') fline
       read(fline,*) system%type(i), system%position(:,i)
       system%type(i) = trim(system%type(i))
    end do

  end subroutine read_XYZ

  !============================!
  !     read XML from file     !
  !============================!
  subroutine read_XML(parameters,system)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    ! internal variables
    integer :: f_xml = 100
    character(len=200) :: fline
    integer :: i, s, e, ierr

    ! open the file
    open(unit=f_xml,file=trim(parameters%config_file),action='read',status='old')

    ! find the box tag
    fline = ""
    do while( index(fline,"<box") .eq. 0 )
       read(f_xml,'(A)') fline
    end do

    ! identify box length
    system%box_length = 0.0_P
    s = 0
    e = -1
    do i = 1, 3
       s = e + 2
       s = scan(fline(s:len(fline)),'"') + s
       e = scan(fline(s:len(fline)),'"') - 2 + s
       read(fline(s:e),*) system%box_length(i)
    end do

    ! go to start of position tag
    fline = ""
    do while( index(fline,"<position") .eq. 0 )
       read(f_xml,'(A)') fline
    end do

    ! identify number of particles
    s = scan(fline,'"') + 1
    e = scan(fline,'"',.true.) - 1
    read(fline(s:e),*) system%num_particles

    ! print information to command line
    write(*,'(A,A)') "Read the specified file: ", trim(parameters%config_file)
    write(*,'("N = ",I0,"; L = (",F10.5,",",F10.5,",",F10.5,")")') system%num_particles, system%box_length

    ! allocate memory for the particle position within the system struct
    if( allocated( system%position ) ) deallocate( system%position )
    allocate( system%position(3,system%num_particles) )

    ! read each particle's position
    do i = 1, system%num_particles
       read(f_xml,'(A)') fline
       read(fline,*) system%position(:,i)
    end do

    rewind(f_xml)

    ! allocate memory for the particle type within the system struct
    if( allocated( system%type ) ) deallocate( system%type )
    allocate( system%type(system%num_particles) )

    ! go to start of type tag
    ierr = 0
    fline = ""
    do while( index(fline,"<type") .eq. 0 .and. ierr .eq. 0 )
       read(f_xml,'(A)',iostat=ierr) fline
    end do

    if( ierr .ne. 0 ) then

       write(*,'("could not find <type> tag, assuming unary system")')
       system%unary_system = .true.
       ! set each particle's type to "A"
       do i = 1, system%num_particles
          system%type(i) = "A"
       end do

    else

       ! read each particle's type
       do i = 1, system%num_particles
          read(f_xml,'(A)') fline
          read(fline,*) system%type(i)
          system%type(i) = trim(system%type(i))
       end do

       ! check for all same type
       if( all(system%type .eq. system%type(1)) ) then
          write(*,'("all <type> tags identical, treating unary system")')
          system%unary_system = .true.
       end if

    end if

  end subroutine read_XML

  !===========================!
  !     write XML to file     !
  !===========================!
  subroutine write_XML(parameters,system,nl,mode)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: nl
    character(len=*), intent(in) :: mode
    ! internal variables
    integer :: f_xml = 200
    character(len=200) :: filename
    integer :: i, num_atoms

    num_atoms = system%num_particles + 2

    write(filename,'(A,".",A)') trim(parameters%config_file), trim(mode)

    open(unit=f_xml,file=trim(filename),action="write",status="replace")

    ! write HOOMD-style headers
    write(f_xml,'(A)') &
         "<?xml version=""1.0"" encoding=""UTF-8""?>"
    write(f_xml,'(A)') &
         "<hoomd_xml version=""1.5"">"
    write(f_xml,'(A,I0,A,I0,A)') &
         "<configuration time_step=""", 0,&
         """ dimensions=""3"" natoms=""", num_atoms, """ >"
    write(f_xml,'(7A)') &
         "<box lx=""", var_float(system%box_length(1),10), &
         """ ly=""",   var_float(system%box_length(2),10), &
         """ lz=""",   var_float(system%box_length(3),10), &
         """ xy=""0"" xz=""0"" yz=""0""/>"

    ! write positions of particles
    write(f_xml,'(A,I0,A)') &
         "<position num=""", num_atoms, """>"
    do i = 1, system%num_particles
       write(f_xml,'(A," ",A," ",A)') &
            var_float(system%position(1,i),10), &
            var_float(system%position(2,i),10), &
            var_float(system%position(3,i),10)
    end do
    ! ghost particles
    write(f_xml,'("0 0 0")')
    write(f_xml,'("0 0 0")')
    write(f_xml,'(A,I0,A)') "</position>"

    ! write types and clusters into velocity field
    write(f_xml,'(A,I0,A)') &
         "<velocity num=""", num_atoms, """>"
    do i = 1, system%num_particles
       ! write(f_xml,'(I0," ",A," ",I0)') nl%type(i), var_float(nl%likelihood(i),5), nl%reverse_map(i)
       write(f_xml,'(I0," ",E15.5," ",I0)') nl%type(i), nl%likelihood(i), nl%reverse_map(i)
       ! write(f_xml,'(F0.5," ",F0.5," ",F0.5)') system%delta(1,i), system%delta(2,i), system%delta(3,i)
    end do
    ! ghost particles
    write(f_xml,'("-1 0 -1")')
    write(f_xml,'("-1 1 -1")')
    write(f_xml,'(A,I0,A)') "</velocity>"

    ! end the tags
    write(f_xml,'(A,I0,A)') "</configuration>"
    write(f_xml,'(A,I0,A)') "</hoomd_xml>"

    ! close the file
    close(f_xml)

  end subroutine write_XML

  !====================================!
  !     write neighborlist to file     !
  !====================================!
  subroutine write_neighbor_XML(parameters,system,nl,cna_list)
    implicit none
    ! external variables
    type (parameter_struct),         intent(inout) :: parameters
    type (system_struct),            intent(inout) :: system
    type (cluster_struct),           intent(inout) :: nl
    type (cna_struct), dimension(:), intent(inout) :: cna_list
    ! internal variables
    integer :: f_xml = 210
    character(len=200) :: filename
    integer :: i, j, k, m, num_atoms
    integer, parameter :: m_max = 16
    integer, dimension(m_max) :: p

    num_atoms = system%num_particles

    write(filename,'(A,".",A)') trim(parameters%config_file), "neighbors"

    open(unit=f_xml,file=trim(filename),action="write",status="replace")

    ! write HOOMD-style headers
    write(f_xml,'(A)') &
         "<?xml version=""1.0"" encoding=""UTF-8""?>"
    write(f_xml,'(A)') &
         "<hoomd_xml version=""1.5"">"
    write(f_xml,'(A,I0,A,I0,A)') &
         "<configuration time_step=""", 0,&
         """ dimensions=""3"" natoms=""", num_atoms, """ >"
    ! write(f_xml,'(7A)') &
    !      "<box lx=""", var_float(system%box_length(1),10), &
    !      """ ly=""",   var_float(system%box_length(2),10), &
    !      """ lz=""",   var_float(system%box_length(3),10), &
    !      """ xy=""0"" xz=""0"" yz=""0""/>"

    ! write positions of particles
    ! write(f_xml,'(A,I0,A)') &
    !      "<position num=""", num_atoms, """>"
    ! do i = 1, system%num_particles
    !    write(f_xml,'(A," ",A," ",A)') &
    !         var_float(system%position(1,i),10), &
    !         var_float(system%position(2,i),10), &
    !         var_float(system%position(3,i),10)
    ! end do
    ! write(f_xml,'(A,I0,A)') "</position>"

    ! write neighbor IDs
    write(f_xml,'(A,I0,A)') &
         "<neighbors num=""", num_atoms, """>"
    do i = 1, system%num_particles
       ! write(f_xml,'(I0)',advance='no') nl%N0(i)
       do j = 1, nl%N0(i)
          write(f_xml,'(" ",I0)',advance='no') nl%neighbor_map(j,i)
       end do
       write(f_xml,*)
    end do
    write(f_xml,'(A,I0,A)') "</neighbors>"

    ! write neighbor distances
    ! write(f_xml,'(A,I0,A)') &
    !      "<distances num=""", num_atoms, """>"
    ! do i = 1, system%num_particles
    !    write(f_xml,'(I0)',advance='no') nl%N0(i)
    !    do j = 1, nl%N0(i)
    !       write(f_xml,'(" ",A)',advance='no') var_float(nl%neighbor_dist(j,i),10)
    !    end do
    !    write(f_xml,*)
    ! end do
    ! write(f_xml,'(A,I0,A)') "</distances>"

    ! write CNA signatures
    write(f_xml,'(A,I0,A)') &
         "<cna num=""", num_atoms, """>"
    ! loop over particles
    do i = 1, system%num_particles
       p = 0
       ! find number of signatures
       m = 0
       do j = 1, count( cna_list(i)%counts .gt. 0 )
          m = m + cna_list(i)%counts(j)
          p(j) = cna_list(i)%sig(j)
       end do
       ! write correct number of zeros
       do j = 1, (m_max - m)
          write(f_xml,'("000000")',advance='no')
       end do
       ! loop over signatures in ascending order
       do while( count(p .gt. 0) .gt. 0 )
          j = minloc(p, 1, p .gt. 0)
          p(j) = 0
          do k = 1, cna_list(i)%counts(j)
             if( cna_list(i)%sig(j) .ge. 10 ) then
                write(f_xml,'("0",I0)',advance='no') cna_list(i)%sig(j)
             else
                write(f_xml,'(I0)',advance='no') cna_list(i)%sig(j)
             end if
          end do
       end do
       ! newline for next signature
       write(f_xml,*)
    end do
    write(f_xml,'(A,I0,A)') "</cna>"

    ! write cluster information
    write(f_xml,'(A,I0,A)') &
         "<cluster num=""", num_atoms, """>"
    ! loop over particles
    do i = 1, system%num_particles
       j = nl%reverse_map(i)
       write(f_xml,'(I0," ",I0)') j, count( nl%reverse_map .eq. j )
    end do
    write(f_xml,'(A,I0,A)') "</cluster>"

    ! end the tags
    write(f_xml,'(A,I0,A)') "</configuration>"
    write(f_xml,'(A,I0,A)') "</hoomd_xml>"

    ! close the file
    close(f_xml)

  end subroutine write_neighbor_XML

  !================================================================!
  !     read colored XML from file and apply coarse clustering     !
  !================================================================!
  subroutine recolor_XML(frame_id,parameters,identities,hierarchy)
    implicit none
    ! external variables
    integer, intent(inout) :: frame_id
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: identities
    type (hierarchy_struct), intent(inout) :: hierarchy
    ! internal variables
    integer :: f_xml = 100
    integer :: f_recolor = 110
    character(len=1000) :: fline
    integer :: ierr = 0
    integer :: i
    integer :: local_type, global_type, coarse_type
    real(P), dimension(3) :: vel

    ! open the files
    open(unit=f_xml,file=trim(parameters%config_file)//'.full_color',action='read',status='old')
    open(unit=f_recolor,file=trim(parameters%config_file)//'.coarse_color',action='write',status='unknown')

    ! write the file exactly as-is until the velocity field
    fline = ""
    do while( index(fline,"<velocity") .eq. 0 )
       read(f_xml,'(A)',iostat=ierr) fline
       write(f_recolor,'(A)') trim(fline)
    end do

    ! loop over all velocity entries
    !     first fline is the first data entry of velocity tag
    i = 0
    read(f_xml,'(A)',iostat=ierr) fline
    do while( index(fline,"</velocity") .eq. 0 )
       i = i + 1
       read(fline,*) vel

       if( vel(1) .gt. 0 ) then
          ! regular particles
          local_type  = int( vel(1) )
          ! keep unassigned particles unassigned
          if( local_type .eq. 0 ) then
             coarse_type = 0
          else
             ! first map from local structure id to global structure id
             global_type = identities%global_map(local_type,frame_id)
             ! then map from full color to coarse color in global definition
             coarse_type = hierarchy%reverse_map(global_type)
          end if
          write(f_recolor,'(I0," ",E15.5," ",I0)') coarse_type, vel(2), int(vel(3))
       else
          ! unassigned and ghost particles
          write(f_recolor,'(I0," ",E15.5," ",I0)') int(vel(1)), vel(2), int(vel(3))
       end if
       read(f_xml,'(A)') fline
    end do

    ! continue until eof
    !     first fline is </velocity> tag
    do while( ierr .eq. 0 )
       write(f_recolor,'(A)') trim(fline)
       read(f_xml,'(A)',iostat=ierr) fline
    end do

    ! close files
    close(f_xml)
    close(f_recolor)

  end subroutine recolor_XML

  !==============================================================!
  !     read colored XML from file and apply full clustering     !
  !==============================================================!
  subroutine recolor_XML_full(frame_id,parameters,identities,hierarchy)
    implicit none
    ! external variables
    integer, intent(inout) :: frame_id
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: identities
    type (hierarchy_struct), intent(inout) :: hierarchy
    ! internal variables
    integer :: f_xml = 100
    integer :: f_recolor = 110
    character(len=1000) :: fline
    integer :: ierr
    integer :: i
    integer :: local_type, global_type
    real(P), dimension(3) :: vel

    ! open the files
    open(unit=f_xml,file=trim(parameters%config_file)//'.full_color',action='read',status='old',iostat=ierr)
    open(unit=f_recolor,file=trim(parameters%config_file)//'.traj_color',action='write',status='unknown',iostat=ierr)

    if( ierr .ne. 0 ) then
       write(*,'("Error: could not open one of _color files")')
       stop
    end if

    ! write the file exactly as-is until the velocity field
    fline = ""
    do while( index(fline,"<velocity") .eq. 0 .and. ierr .eq. 0 )
       read(f_xml,'(A)',iostat=ierr) fline
       write(f_recolor,'(A)') trim(fline)
    end do

    if( ierr .ne. 0 ) then
       write(*,'("Error: Could not find <velocity> tag in file: ",A)') trim(parameters%config_file)//'.full_color'
       stop
    end if

    ! loop over all velocity entries
    !     first fline is the first data entry of velocity tag
    i = 0
    read(f_xml,'(A)',iostat=ierr) fline
    do while( index(fline,"</velocity") .eq. 0 )
       i = i + 1
       read(fline,*) vel

       if( vel(1) .gt. 0 ) then
          ! regular particles
          local_type  = int( vel(1) )
          ! keep unassigned particles unassigned
          if( local_type .eq. 0 ) then
             global_type = 0
          else
             ! first map from local structure id to global structure id
             global_type = identities%global_map(local_type,frame_id)
          end if
          write(f_recolor,'(I0," ",E15.5," ",I0)') global_type, vel(2), int(vel(3))
       else
          ! unassigned and ghost particles
          write(f_recolor,'(I0," ",E15.5," ",I0)') int(vel(1)), vel(2), int(vel(3))
       end if
       read(f_xml,'(A)') fline
    end do

    ! continue until eof
    !     first fline is </velocity> tag
    do while( ierr .eq. 0 )
       write(f_recolor,'(A)') trim(fline)
       read(f_xml,'(A)',iostat=ierr) fline
    end do

    ! close files
    close(f_xml)
    close(f_recolor)

  end subroutine recolor_XML_full

  !======================================!
  !     write cna signatures to file     !
  !======================================!
  subroutine write_cna_sigs(parameters,system,nl)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: nl
    ! internal variables
    integer :: i, j, k, m
    integer :: f_cna = 530
    character(len=1000) :: filename
    integer, parameter :: m_max = 16
    type (cna_struct) :: cna
    integer, dimension(m_max) :: p

    ! open file
    write(filename,'(A,".cna")') trim(parameters%config_file)
    open(unit=f_cna,file=trim(filename),action="write",status="replace")

    ! loop over particles
    do i = 1, system%num_particles

       ! reset cna
       cna%sig = 0
       cna%counts = 0
       cna%type = ''
       p = 0

       ! compute cna
       call common_neighbor_analysis(i,system,nl,cna)

       ! find number of signatures
       m = 0
       do j = 1, count( cna%counts .gt. 0 )
          m = m + cna%counts(j)
          p(j) = cna%sig(j)
       end do
       ! write correct number of zeros
       do j = 1, (m_max - m)
          write(f_cna,'("000000")',advance='no')
       end do
       ! loop over signatures in ascending order
       do while( count(p .gt. 0) .gt. 0 )
          j = minloc(p, 1, p .gt. 0)
          p(j) = 0
          do k = 1, cna%counts(j)
             if( cna%sig(j) .ge. 10 ) then
                write(f_cna,'("0",I0)',advance='no') cna%sig(j)
             else
                write(f_cna,'(I0)',advance='no') cna%sig(j)
             end if
          end do
       end do
       ! newline for next signature
       write(f_cna,*)

    end do

    close(f_cna)

  end subroutine write_cna_sigs

  !============================================!
  !     write reference structures to file     !
  !============================================!
  subroutine write_reference_structure(parameters,identities,nl)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: identities
    type (cluster_struct),   intent(inout) :: nl
    ! internal variables
    integer :: i, j
    integer :: f_ref = 520
    character(len=1000) :: filename

    ! open file
    write(filename,'(A,".ref")') trim(parameters%config_file)
    open(unit=f_ref,file=trim(filename),action="write",status="replace")

    ! for each reference structure, write:
    write(f_ref,'("% number : name : cna_signature : cluster_count total_count : self_similarity : bond angles")')

    do i = 1, num_structures

       ! name of reference structure
       write(f_ref,'(I0," : ",A," : ")',advance='no') i, trim(identities%name(i))

       ! cna signature
       do j = 1, count( identities%cna(i)%counts .gt. 0 )
          write(f_ref,'(I0," ",A," ",I0," ")',advance='no')  &
               identities%cna(i)%counts(j), trim(identities%cna(i)%type(j)), identities%cna(i)%sig(j)
       end do

       ! particle count for clusters and clusters + singletons
       write(f_ref,'(": ",I0," ",I0," : ")',advance='no') &
            identities%baa(i)%num_obs, count( nl%type .eq. i )

       ! self similarity metric
       write(f_ref,'(A," : ")',advance='no') var_float(identities%p(i),5)

       ! bond angles
       do j = 1, count( identities%baa(i)%angles .gt. -100 )
          write(f_ref,'(A," ")',advance='no') var_float(identities%baa(i)%angles(j),5)
       end do

       write(f_ref,*)

    end do

    close(f_ref)

  end subroutine write_reference_structure

  !============================================!
  !     write reference structures to file     !
  !============================================!
  subroutine write_global_reference_structure(parameters,identities)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: identities
    ! internal variables
    integer :: i, j
    integer :: f_ref = 500
    character(len=1000) :: filename

    ! open file
    write(filename,'(A,".ref")') trim(parameters%snapshot_file)
    open(unit=f_ref,file=trim(filename),action="write",status="replace")

    ! for each reference structure, write:
    write(f_ref,'("% number : name : cna_signature : cluster_count total_count : self_similarity : bond angles")')

    do i = 1, num_structures

       ! name of reference structure
       write(f_ref,'(I0," : ",A," : ")',advance='no') i, trim(identities%name(i))

       ! cna signature
       do j = 1, count( identities%cna(i)%counts .gt. 0 )
          write(f_ref,'(I0," ",I0," ")',advance='no')  &
               identities%cna(i)%counts(j), identities%cna(i)%sig(j)
       end do

       ! particle count for clusters and clusters + singletons
       write(f_ref,'(": ",I0," ",I0," : ")',advance='no') &
            identities%baa(i)%num_obs, identities%baa(i)%num_total

       ! self similarity metric
       write(f_ref,'(A," : ")',advance='no') var_float(identities%p(i),5)

       ! bond angles
       do j = 1, count( identities%baa(i)%angles .gt. -100 )
          write(f_ref,'(A," ")',advance='no') var_float(identities%baa(i)%angles(j),5)
       end do

       write(f_ref,*)

    end do

    close(f_ref)

  end subroutine write_global_reference_structure

  !============================================!
  !     write reference structures to file     !
  !============================================!
  subroutine read_reference_structure(parameters,identities)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: identities
    ! internal variables
    type (identity_struct) :: temp_identities ! for buffering
    integer :: i, j, k, l
    integer :: s, e
    integer :: ierr = 0
    integer :: f_ref = 510
    character(len=1000) :: filename, tempchar
    character(len=5000) :: fline ! the lines may be very long
    real(P) :: tempreal
    integer, parameter :: verbose = 0

    ! open file
    write(filename,'(A,".ref")') trim(parameters%config_file)
    open(unit=f_ref,file=trim(filename),action="read",status="unknown")

    print*,trim(filename)
    write(*,'("reading reference file ",A," back from disk...")') trim(filename)

    ! for each reference structure, read the following
    !      number : name : cna_signature : cluster_count total_count : self_similarity : bond angles
    ! first read through header(s)
    fline = '%'
    do while( index(fline,"%") .eq. 1 )
       read(f_ref,'(A)',iostat=ierr) fline
    end do

    ! loop over lines until eof
    i = 0
    do while( ierr .eq. 0 )
       i = i + 1

       ! keep a buffer of identity structs
       if( i .ge. size(identities%baa) ) then
          ref_size = size(identities%baa)
          write(*,'("expanding reference library from ",I0," to ",I0," entries")') &
               ref_size, ref_size + n_ref_buffer
          ref_size = ref_size + n_ref_buffer
          ! create a new identity struct with buffer space
          call copy_identities(parameters,identities,temp_identities)
          ! replace the identities with the new resized one
          identities = temp_identities
       end if

       ! parse number
       s = 1
       e = index( fline, ":")
       tempchar = trim( fline(s:(e-1)) )
       read(tempchar,*) tempreal
       if( verbose .eq. 1 ) then
          write(*,'(I0," : ")',advance='no') int( tempreal )
       end if
       s = e + 1

       if( int( tempreal ) .ne. i ) then
          write(*,*)
          write(*,'("expected ",I0," != ",I0)') i, int( tempreal )
          stop
       end if

       ! parse name
       e = index( fline(s:), ":") + s - 1
       identities%name(i) = trim( fline(s:(e-1)) )
       if( verbose .eq. 1 ) then
          write(*,'(A," : ")',advance='no') trim(identities%name(i))
       end if
       s = e

       ! parse cna signature
       j = 0
       k = 0
       l = 1
       identities%cna(i)%counts = 0
       identities%cna(i)%sig = 0
       do while( k .lt. l )
          j = j + 1
          ! find count
          s = scan( fline(s:), "0123456789" ) + s - 1
          e = scan( fline(s:), " ") + s
          read(fline(s:e),*) tempreal
          identities%cna(i)%counts(j) = int( tempreal )
          s = e
          ! find type
          e = scan( fline(s:), " " ) + s - 2
          identities%cna(i)%type(j) = trim(fline(s:e))
          s = e
          ! find signature
          s = scan( fline(s:), "0123456789" ) + s - 1
          e = scan( fline(s:), " ") + s
          read(fline(s:e),*) tempreal
          identities%cna(i)%sig(j) = int( tempreal )
          s = e
          ! report
          if( verbose .eq. 1 ) then
             write(*,'(I0," ",A," ",I0," ")',advance='no')  &
                  identities%cna(i)%counts(j), &
                  trim(identities%cna(i)%type(j)), &
                  identities%cna(i)%sig(j)
          end if
          ! next loop
          k = scan( fline(s:), "0123456789" )
          l = scan( fline(s:), ":" )
       end do

       ! parse particle count for reference structures
       s = scan( fline(s:), "0123456789" ) + s - 1
       e = scan( fline(s:), " ") + s
       read(fline(s:e),*) tempreal
       identities%baa(i)%num_obs = int( tempreal )
       s = e
       if( verbose .eq. 1 ) then
          write(*,'(": ",I0," ")',advance='no') identities%baa(i)%num_obs
       end if

       ! parse total (including singleton) count
       s = scan( fline(s:), "0123456789" ) + s - 1
       e = scan( fline(s:), " ") + s
       read(fline(s:e),*) tempreal
       identities%baa(i)%num_total = int( tempreal )
       s = e
       if( verbose .eq. 1 ) then
          write(*,'(I0," : ")',advance='no') identities%baa(i)%num_total
       end if

       ! skip to next delimiter
       e = index( fline(s:), ":") + s - 1
       s = e + 1

       ! parse self-similarity metric
       s = scan( fline(s:), "0123456789*" ) + s - 1
       e = scan( fline(s:), " ") + s
       read(fline(s:e),*) tempchar
       if( index(tempchar,'*') .gt. 0 ) then
          identities%p(i) = 0.0
       else
          read(tempchar,*) identities%p(i)
       end if
       s = e
       if( verbose .eq. 1 ) write(*,'(A," : ")',advance='no') var_float(identities%p(i),5)
       e = index( fline(s:), ":") + s - 1
       s = e + 1

       ! parse bond angles
       j = 0
       k = scan( fline(s:), "0123456789-" )
       identities%baa(i)%angles = -infinity
       do while( k .gt. 0 )
          j = j + 1

          ! find angle
          s = scan( fline(s:), "0123456789-" ) + s - 1
          e = scan( fline(s:), " ") + s
          read(fline(s:e),*) identities%baa(i)%angles(j)
          s = e
          ! report
          if( verbose .eq. 1 ) write(*,'(A," ")',advance='no')  var_float(identities%baa(i)%angles(j),5)
          ! next loop
          k = scan( fline(s:), "0123456789-" )
       end do

       if( verbose .eq. 1 ) write(*,*)

       read(f_ref,'(A)',iostat=ierr) fline

    end do

    close(f_ref)

    num_local_structures = i

  end subroutine read_reference_structure

  !=====================================!
  !     write cluster graph to file     !
  !=====================================!
  subroutine write_cluster(parameters,nl)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (cluster_struct),   intent(inout) :: nl
    ! internal variables
    integer :: i, j
    integer :: f_nl = 300
    character(len=1000) :: filename
    integer, dimension(parameters%num_particles) :: map

    ! open file
    write(filename,'(A,"_",I0,".clust")') trim(parameters%config_file), parameters%cna_tol
    open(unit=f_nl,file=trim(filename),action="write",status="replace")

    write(f_nl,'("% there are ",I0," clusters")') nl%num_clusters

    ! write cluster map
    do i = 1, nl%num_clusters
       ! do j = 1, count( nl%map(:,i) .gt. 0 )
       ! write(f_nl,'(I0," ")',advance='no') nl%map(j,i)
       map = 0
       map = pack([(j,j=1,size(nl%reverse_map))],nl%reverse_map.eq.i,map)
       do j = 1, count( map .gt. 0 )
          write(f_nl,'(I0," ")',advance='no') map(j)
       end do
       write(f_nl,'(A)')
    end do

    close(f_nl)

  end subroutine write_cluster

  !======================================!
  !     write cna signatures to file     !
  !======================================!
  subroutine write_cna(parameters,cna_list)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (cna_struct), dimension(:), intent(in) :: cna_list
    ! internal variables
    integer :: i, j
    integer :: f_cna = 400
    character(len=1000) :: filename

    ! open file
    write(filename,'(A,".cna")') trim(parameters%config_file)
    open(unit=f_cna,file=trim(filename),action="write",status="replace")

    ! write cna signatures in pairs
    do i = 1, size(cna_list)
       do j = 1, size(cna_list(i)%counts)
          write(f_cna,'(I0," ",I0," ")',advance='no') &
               cna_list(i)%counts(j), cna_list(i)%sig(j)
       end do
       write(f_cna,'(A)')
    end do

    close(f_cna)

  end subroutine write_cna

  !===================================!
  !     write MAP results to file     !
  !===================================!
  subroutine write_maps(parameters,ma_list)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (multiatom_struct), dimension(:), intent(in) :: ma_list
    ! internal variables
    integer :: i, j
    integer :: f_map = 600
    character(len=1000) :: filename

    ! open file
    write(filename,'(A,".map")') trim(parameters%config_file)
    open(unit=f_map,file=trim(filename),action="write",status="replace")

    i = 1
    do while( count( ma_list(i)%all_center_id .gt. 0 ) .gt. 0 )

       write(f_map,'("A:")',advance='no')
       do j = 1, count( ma_list(i)%all_assoc_id .gt. 0 )
          write(f_map,'(" ",I0)',advance='no') ma_list(i)%all_assoc_id(j) - 1
       end do
       write(f_map,*)

       write(f_map,'("E:")',advance='no')
       do j = 1, count( ma_list(i)%all_edge_id .gt. 0 )
          write(f_map,'(" ",I0)',advance='no') ma_list(i)%all_edge_id(j) - 1
       end do
       write(f_map,*)

       write(f_map,'("T:")',advance='no')
       do j = 1, count( ma_list(i)%all_tess_id .gt. 0 )
          write(f_map,'(" ",I0)',advance='no') ma_list(i)%all_tess_id(j) - 1
       end do
       write(f_map,*)

       write(f_map,'("C:")',advance='no')
       do j = 1, count( ma_list(i)%all_center_id .gt. 0 )
          write(f_map,'(" ",I0)',advance='no') ma_list(i)%all_center_id(j) - 1
       end do
       write(f_map,*)

       write(f_map,*)

       i = i + 1
    end do

    close(f_map)

  end subroutine write_maps

end module io
