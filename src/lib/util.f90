module util
  use datatypes
  implicit none
  
contains

  !=============================================!
  !     write variable width floating point     !
  !=============================================!
  function var_float(val,prec)
    implicit none
    character(len=:), allocatable :: var_float
    ! external variables
    real(P), intent(in) :: val
    integer, intent(in) :: prec
    ! internal variables
    integer, parameter :: max_places = 20
    integer :: pre_decimal
    integer :: pre_places, tot_places
    character(len=100) :: fmt
    ! if( abs(val) .le. almost_zero ) then
    !    tot_places = prec + 2 ! accounts for zero and decimal
    !    write(fmt,'("(F",I0,".",I0,")")') tot_places, prec
    !    allocate( character(len=tot_places) :: var_float )
    !    write(var_float,trim(fmt)) val
    !    return
    ! else
    if( val .ge. ( 10.0_P ** real(max_places,P) ) .or. val .ne. val ) then
       allocate( character(len=10) :: var_float )
       write(var_float,'("**********")')
       return
    end if
    pre_decimal = int( sign( real(floor(abs(val)),P), val ) )
    if( abs(pre_decimal) .lt. 1 ) then
       pre_places = 1
    else
       pre_decimal = int( sign( real(floor(abs(val)),P), val ) )
    end if
    if( abs(pre_decimal) .lt. 1 ) then
       pre_places = 1
    else
       pre_places  = floor( log( real(abs(pre_decimal),P) ) / log(10.0_P) ) + 1
    end if
    tot_places = pre_places + prec + 2 ! accounts for sign and decimal
    write(fmt,'("(F",I0,".",I0,")")') tot_places, prec
    allocate( character(len=tot_places) :: var_float )
    write(var_float,trim(fmt)) val
  end function var_float
    
  !==========================================!
  !     seed the random number generator     !
  !==========================================!
  subroutine initialize_rng()
    use iso_fortran_env, only: int64
    implicit none
    integer, allocatable :: seed(:)
    integer :: i, n, un, istat, dt(8), pid
    integer(int64) :: t

    call random_seed(size = n)
    allocate(seed(n))
    ! First try if the OS provides a random number generator
    open(newunit=un, file="/dev/urandom", access="stream", &
         form="unformatted", action="read", status="old", iostat=istat)
    if (istat == 0) then
       read(un) seed
       close(un)
    else
       ! Fallback to XOR:ing the current time and pid. The PID is
       ! useful in case one launches multiple instances of the same
       ! program in parallel.
       call system_clock(t)
       if (t == 0) then
          call date_and_time(values=dt)
          t = (dt(1) - 1970) * 365_int64 * 24 * 60 * 60 * 1000 &
               + dt(2) * 31_int64 * 24 * 60 * 60 * 1000 &
               + dt(3) * 24_int64 * 60 * 60 * 1000 &
               + dt(5) * 60 * 60 * 1000 &
               + dt(6) * 60 * 1000 + dt(7) * 1000 &
               + dt(8)
       end if
       pid = getpid()
       t = ieor(t, int(pid, kind(t)))
       do i = 1, n
          seed(i) = lcg(t)
       end do
    end if
    call random_seed(put=seed)
  contains
    ! This simple PRNG might not be good enough for real work, but is
    ! sufficient for seeding a better PRNG.
    function lcg(s)
      integer :: lcg
      integer(int64) :: s
      if (s == 0) then
         s = 104729
      else
         s = mod(s, 4294967296_int64)
      end if
      s = mod(s * 279470273_int64, 4294967291_int64)
      lcg = int(mod(s, int(huge(0), int64)), kind(0))
    end function lcg
  end subroutine initialize_rng
  
  !===============================================================!
  !     choose a random point on the surface of a unit sphere     !
  !===============================================================!
  subroutine random_sphere(direction)
    implicit none
    ! external variables
    real(P), dimension(3), intent(out) :: direction
    ! internal variables
    real(P) :: u, v
    call random_number(u)
    u = u * 2.0_P - 1.0_P
    call random_number(v)
    v = v * 2.0_P * pi
    direction(1) = sqrt(1-u**2.0)*cos(v)
    direction(2) = sqrt(1-u**2.0)*sin(v)
    direction(3) = u
  end subroutine random_sphere
  
  !=============================================!
  !     compute cross product of 3x3 matrix     !
  !=============================================!
  function cross(a,b)
    implicit none
    real(P), dimension(3) :: cross
    real(P), dimension(3), intent(in) :: a, b
    cross(1) = a(2) * b(3) - a(3) * b(2)
    cross(2) = a(3) * b(1) - a(1) * b(3)
    cross(3) = a(1) * b(2) - a(2) * b(1)
  end function cross

  !===================================!
  !     construct identity matrix     !
  !===================================!
  function eye(rank)
    implicit none
    ! external variables
    integer, intent(in) :: rank
    real(P), dimension(rank,rank) :: eye
    ! internal variables
    integer :: i    
    eye = 0.0_P
    do i = 1, rank
       eye(i,i) = 1.0_P
    end do    
  end function eye

  !===================================!
  !     construct rotation matrix     !
  !===================================!
  function rotation_matrix(direction,angle)
    implicit none
    real(P), dimension(3,3) :: rotation_matrix
    ! external variables
    real(P), dimension(3) :: direction
    real(P) :: angle
    ! internal variables
    real(P), dimension(3,3) :: rxr, cross
    integer :: i, j

    ! build cross product matrix
    cross(1,:) = (/0.0_P,          -direction(3), direction(2)/)
    cross(2,:) = (/direction(3),  0.0_P,         -direction(1)/)
    cross(3,:) = (/-direction(2), direction(1), 0.0_P/)

    ! build tensor product
    rxr = 0.0_P
    do i = 1,3
       do j = 1,3
          rxr(i,j) = direction(i)*direction(j)
       end do
    end do

    ! build rotation matrix
    rotation_matrix = cos(angle)*eye(3) + sin(angle)*cross + (1.0_P-cos(angle))*rxr
    
  end function rotation_matrix

  !======================================!
  !     normalize a 3 element vector     !
  !======================================!
  function normalize(a)
    implicit none
    real(P), dimension(3) :: normalize
    real(P), dimension(3), intent(in) :: a
    normalize = a / sqrt( sum( a**2.0_P ) )
  end function normalize

  !==========================================================!
  !     return wrapped displacement vector pos_i - pos_j     !
  !==========================================================!
  function displacement(system,r_i,r_j)
    implicit none
    real(P), dimension(3) :: displacement
    ! external variables
    type (system_struct),   intent(inout) :: system
    real(P), dimension(3),  intent(in)    :: r_i, r_j
    
    ! compute wrapped displacement by referencing box size
    displacement = r_j - r_i

    where( abs( displacement ) .gt. system%box_length*0.5_P )
       displacement = displacement - sign( system%box_length, displacement )
    end where

  end function displacement

  !====================================================!
  !     identify the cell id from cell coordinates     !
  !====================================================!
  function cell_coord_to_id(cell_coord,cell_dim)
    implicit none
    integer :: cell_coord_to_id
    ! external variables
    integer, dimension(3), intent(inout) :: cell_coord
    integer, dimension(3), intent(inout) :: cell_dim
    ! internal variables
    integer :: x_stack, y_stack, z_stack

    ! make the math more transparent
    x_stack = cell_coord(1) * ( cell_dim(2)*cell_dim(3) )
    y_stack = cell_coord(2) * cell_dim(3)
    z_stack = cell_coord(3)
    
    ! convert coordinates to id
    cell_coord_to_id = x_stack + y_stack + z_stack + 1

    if( cell_coord_to_id .gt. product(cell_dim) .or. &
         cell_coord_to_id .lt. 1 ) then
       print*,'problem in cell_coord_to_id:',cell_coord,'->',cell_coord_to_id,&
            '//',cell_dim,'=',product(cell_dim)
    end if
    
  end function cell_coord_to_id

  !====================================================!
  !     identify the cell coordinates from cell id     !
  !====================================================!
  function cell_id_to_coord(cell_id,cell_dim)
    implicit none
    integer, dimension(3) :: cell_id_to_coord
    ! external variables
    integer, intent(inout) :: cell_id
    integer, dimension(3), intent(inout) :: cell_dim
    ! internal variables
    integer :: leftover

    ! useful counter for dealing with three dimensions
    leftover = cell_id - 1
    
    ! find how many 2D slices are behind this cell in the list
    ! NOTE: integer math is truncated, so this emulates FLOOR()
    cell_id_to_coord(1) = leftover / ( cell_dim(2)*cell_dim(3) )
    leftover = leftover - cell_id_to_coord(1) * ( cell_dim(2)*cell_dim(3) )
    ! find how many 1D slices are behind this cell in the list
    ! NOTE: integer math is truncated, so this emulates FLOOR()
    cell_id_to_coord(2) = leftover / cell_dim(3)
    leftover = leftover - cell_id_to_coord(2) * cell_dim(3)
    ! nothing left to do in the third dimension
    cell_id_to_coord(3) = leftover

    if( any( cell_id_to_coord .ge. cell_dim ) ) then
       print*,'problem in cell_id_to_coord:',cell_id,'->',cell_id_to_coord
    end if
    
  end function cell_id_to_coord

  !================================================================!
  !     apply periodic boundary conditions to cell coordinates     !
  !================================================================!
  function wrap_cell_coord(cell_coord,cell_dim)
    implicit none
    integer, dimension(3) :: wrap_cell_coord
    ! external variables
    integer, dimension(3), intent(in) :: cell_coord
    integer, dimension(3), intent(inout) :: cell_dim
    ! internal variables

    wrap_cell_coord = cell_coord
    
    where( cell_coord .ge. cell_dim )
       wrap_cell_coord = cell_coord - cell_dim
    end where

    where( cell_coord .lt. 0 )
       wrap_cell_coord = cell_dim + cell_coord
    end where

  end function wrap_cell_coord

  !=========================================!
  !     count matches in cna signatures     !
  !=========================================!
  integer function count_cna_matches(cna_a,cna_b)
    implicit none
    ! external variables
    type (cna_struct), intent(in) :: cna_a, cna_b
    ! internal variables
    integer :: i
    integer :: b_id, s

    s = 0
    
    ! loop over a and compare to b
    i = 1
    do while( cna_a%counts(i) .gt. 0 )
       ! find a match among cna_b
       b_id = minloc( cna_b%sig, 1, cna_b%sig .eq. cna_a%sig(i) .and. cna_b%type .eq. cna_a%type(i) )
       ! add the lesser of the count in a or b
       if( b_id .gt. 0 ) then
          s = s + min(cna_a%counts(i), cna_b%counts(b_id))
       end if
       i = i + 1
    end do

    count_cna_matches = s
    
  end function count_cna_matches
  
  !=====================================================!
  !     return the optimal matching between A and B     !
  !=====================================================!
  function optimal_match(temp_ang,this_angles)
    implicit none
    ! external variables
    real(P), dimension(:), intent(in) :: temp_ang, this_angles
    integer, dimension(size(temp_ang)) :: optimal_match
    ! internal variables
    integer, dimension( size(this_angles), size(temp_ang) ) :: prohibited
    integer, dimension( size(this_angles) ) :: this_count
    integer, dimension( size(temp_ang) ) :: assigned
    real(P), dimension( size(temp_ang) ) :: residual
    integer :: k, n
    integer :: match_id, sub_match_id, remove_id
    integer :: pair_counter, angle_counter
    
    n = size(temp_ang)
    this_count = 1
    
    ! keep track of locations that cannot be occupied
    !      by pair i in prohibited(:,i)
    prohibited = 0
    assigned = 0

    ! loop until all the matches have been assigned
    do while( count( assigned .eq. 0 ) .gt. &
         max(0,size(temp_ang) - size(this_angles)) )

       ! write(*,'("trying to assign ",I0," values...")') count(assigned .eq. 0)
       
       ! loop over the unassigned pairs
       do pair_counter = 1, count( assigned .eq. 0 )
          ! get pair index
          k = minloc( assigned, 1, assigned .eq. 0 )
          ! find best match from this structure that is allowed
          match_id = minloc( abs( temp_ang(k) - this_angles ), 1, &
               prohibited(:,k) .lt. 1 )
          if( match_id .eq. 0 ) then
             residual(k) = infinity
             print*,'no place to assign this entry'
          else
             ! check for multiple assignment
             if( count( this_angles .eq. this_angles(match_id) ) .gt. 1 ) then
                ! write(*,'("multiple assignment encountered...")',advance='no')
                ! find the first entry that is not assigned already
                do while( count( assigned .eq. match_id ) .gt. 1 &
                     .and. match_id .lt. size(this_angles) )
                   sub_match_id = minloc( abs( temp_ang(k) - this_angles(match_id:) ), 1, &
                        prohibited(match_id:,k) .lt. 1 )
                   if( sub_match_id .gt. 0 ) then
                      match_id = sub_match_id + match_id
                   else
                      match_id = size(this_angles) + 1
                   end if
                end do
             end if
             
             if( match_id .gt. n ) then
                ! there were no matches, so just try the first one
                match_id = minloc( abs( temp_ang(k) - this_angles ), 1, &
                        prohibited(:,k) .lt. 1 )
             end if

             ! write(*,'("settled on ",I0)') match_id
             ! assign the pair to an angle
             assigned(k) = match_id
             ! record the residual
             residual(k) = abs( temp_ang(k) - this_angles(match_id) )

          end if
       end do

       ! remove the worst over-assigned angles
       do angle_counter = 1, size( this_angles )
          ! loop over pairs that were assigned to this angle
          do while( count( assigned .eq. angle_counter ) .gt. 1 )
             ! find the worst match for this angle
             remove_id = maxloc( residual, 1, assigned .eq. angle_counter )
             ! remove it and prohibit it from being assigned again
             residual(remove_id) = infinity
             assigned(remove_id) = 0
             prohibited(angle_counter,remove_id) = 1
          end do
       end do

    end do

    optimal_match = assigned
    
  end function optimal_match
  
  subroutine setup_identities(parameters,identities)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: identities

    if( allocated(identities%name) ) deallocate( identities%name )
    allocate( identities%name(ref_size) )
    if( allocated(identities%baa) ) deallocate( identities%baa )
    allocate( identities%baa(ref_size) )
    if( allocated(identities%cna) ) deallocate( identities%cna )
    allocate( identities%cna(ref_size) )
    if( allocated(identities%p) ) deallocate( identities%p )
    allocate( identities%p(ref_size) )
    if( allocated(identities%global_map) ) deallocate( identities%global_map )
    allocate( identities%global_map(ref_size,parameters%num_frames) )
    
  end subroutine setup_identities

  subroutine copy_identities(parameters,copyfrom_identities,copyto_identities)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: copyfrom_identities, copyto_identities
    ! internal variables
    integer :: i, old_ref_size
    
    ! allocate the destination struct
    call setup_identities( parameters, copyto_identities )

    copyto_identities%frame_id = copyfrom_identities%frame_id
    copyto_identities%num_structures = copyfrom_identities%num_structures
    
    ! copy the values
    !     DERIVED TYPES WITH ALLOCATABLE ARRAYS COPY POINTERS ONLY!!!!!
    do i = 1, size(copyfrom_identities%name)
       copyto_identities%name(i) = copyfrom_identities%name(i)
    end do
    do i = 1, size(copyfrom_identities%baa)
       copyto_identities%baa(i) = copyfrom_identities%baa(i)
    end do
    do i = 1, size(copyfrom_identities%cna)
       copyto_identities%cna(i) = copyfrom_identities%cna(i)
    end do
    do i = 1, size(copyfrom_identities%p)
       copyto_identities%p(i) = copyfrom_identities%p(i)
    end do
    old_ref_size = ref_size - n_ref_buffer
    do i = 1, size(copyfrom_identities%global_map,2)
       copyto_identities%global_map(1:old_ref_size,i) = copyfrom_identities%global_map(1:old_ref_size,i)
    end do    
    
  end subroutine copy_identities

  !============================================================!
  !     add element to a list if it is not already present     !
  !============================================================!
  subroutine add_unique_element(a,b)
    implicit none
    ! external variables
    integer, intent(in) :: a
    integer, intent(inout), dimension(:) :: b
    ! internal variables
    integer :: j
    j = count( b .gt. -1 )
    if( minloc( b, 1, b .eq. a ) .eq. 0 ) then
       j = j + 1
       b(j) = a
    end if
  end subroutine add_unique_element

  !============================================================!
  !     add element to a list if it is not already present     !
  !============================================================!
  subroutine add_unique_element_pos(a,b)
    implicit none
    ! external variables
    integer, intent(in) :: a
    integer, intent(inout), dimension(:) :: b
    ! internal variables
    integer :: j
    j = count( b .gt. 0 )
    if( minloc( b, 1, b .eq. a ) .eq. 0 ) then
       j = j + 1
       b(j) = a
    end if
  end subroutine add_unique_element_pos

  ! subroutine expand_histogram(histogram)
  !   implicit none
  !   ! external variables
  !   integer, dimension(:), intent(inout) :: histogram
  !   ! internal variables
  !   integer, dimension(:), allocatable :: temp_histogram
  !   ! copy histogram to temporary buffer
  !   allocate( temp_histogram(ref_size) )
  !   temp_histogram = 0
  !   temp_histogram(1:size(histogram)) = histogram
  !   ! resize histogram
  !   deallocate( histogram )
  !   allocate( histogram(ref_size) )
  !   histogram = temp_histogram
  ! end subroutine expand_histogram
  
end module util
