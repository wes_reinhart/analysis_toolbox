module cna
  use datatypes
  use util
  use sort
  use compute
  implicit none
contains

  !==========================================================!
  !     calculate the CNA signature for a given particle     !
  !==========================================================!
  subroutine common_neighbor_analysis(particle_id,system,nl,cna)
    implicit none
    ! external variables
    integer, intent(in) :: particle_id
    type (system_struct),  intent(inout) :: system
    type (cluster_struct), intent(inout) :: nl
    type (cna_struct),     intent(inout) :: cna
    ! internal variables
    integer :: i, j, k
    integer :: idx, kdx, irow, krow
    integer :: i_neighbors, j_neighbors, neighbor_count, bond_count, cna_counter
    integer :: i_neighbor_id, j_neighbor_id, cna_id
    integer, dimension(max_pairs,max_pairs) :: common_neighbors, nn
    integer, dimension(max_pairs) :: neighbor_map, neighbor_common_count
    integer, dimension(max_pairs) :: neighbor_path_length, neighbor_bond_count
    integer, dimension(max_pairs) :: cna_signature
    character(len=type_len), dimension(max_pairs) :: neighbor_type
    logical :: unique_flag

    common_neighbors = 0
    nn = 0
    neighbor_map = 0

    ! count the number of common neighbors between central particle and its nearest neighbor
    i_neighbors = nl%N0(particle_id)
    do i = 1, i_neighbors
       neighbor_count = 0

       i_neighbor_id = nl%neighbor_map(i,particle_id)
       neighbor_map(i) = i_neighbor_id

       j_neighbors = nl%N0(i_neighbor_id)
       ! investigate neighbor's neighbors
       do j = 1, j_neighbors
          j_neighbor_id = nl%neighbor_map(j,i_neighbor_id)
          ! check if this neighbor is a neighbor of the central particle
          if( any( nl%neighbor_map(1:i_neighbors,particle_id) .eq. j_neighbor_id ) ) then
             ! only include it once
             if( .not. any( common_neighbors(1:max_pairs,i) .eq. j_neighbor_id ) ) then
                neighbor_count = neighbor_count + 1
                common_neighbors(neighbor_count,i) = j_neighbor_id
                nn(neighbor_count,i) = minloc(nl%neighbor_map(1:i_neighbors,particle_id),1,&
                     nl%neighbor_map(1:i_neighbors,particle_id).eq.j_neighbor_id)
             end if
          end if
       end do

       neighbor_common_count(i) = neighbor_count
       neighbor_type(i) = system%type(i_neighbor_id)

    end do

    ! count the number of bonds between these common neighbors
    do j = 1, i_neighbors
       bond_count = 0
       do i = 1, count(common_neighbors(:,j) .gt. 0)
          idx  = common_neighbors(i,j)
          irow = minloc(nl%neighbor_map(:,particle_id),1,nl%neighbor_map(:,particle_id).eq.idx)
          do k = 1, count(common_neighbors(:,j) .gt. 0)
             if( i == k ) then
                continue
             end if
             kdx = common_neighbors(k,j)
             krow = minloc(nl%neighbor_map(:,particle_id),1,nl%neighbor_map(:,particle_id).eq.kdx)
             if( any( common_neighbors(:,irow) .eq. kdx ) ) then
                bond_count = bond_count + 1
             end if
          end do
       end do
       neighbor_bond_count(j) = bond_count
    end do

    ! find longest chain of bonds connecting the common neighbors
    ! call find_longest_path(neighbor_path_length,common_neighbors,neighbor_map,neighbor_bond_count)
    do i = 1, i_neighbors
       neighbor_path_length(i) = find_longest_path_graph(i,nn)
    end do

    cna_signature = 0
    ! convert to CNA signatures
    do i = 1, i_neighbors
       cna_signature(i) = neighbor_common_count(i) * 10000 &
            + neighbor_bond_count(i) * 100 &
            + neighbor_path_length(i)
       if( cna_signature(i) .lt. 0 ) then
          write(*,'("cna_signature < 0 : ",I0," ",I0," ",I0)') &
               neighbor_common_count(i), neighbor_bond_count(i), neighbor_path_length(i)
       end if
       nl%cna(i,particle_id) = cna_signature(i)
    end do

    cna%sig     = 0
    cna%counts  = 0
    cna_counter = 0
    ! count unique signatures
    do i = 1, i_neighbors
       unique_flag = .true.
       ! check whether the signature is unique
       do j = 1, count( cna%counts .gt. 0 )
          if( cna_signature(i) .eq. cna%sig(j) &
               .and. neighbor_type(i) .eq. cna%type(j) ) &
               unique_flag = .false.
       end do
       ! add unique signatures to the list
       if( unique_flag ) then
          cna_counter = cna_counter + 1
          cna%sig(cna_counter)  = cna_signature(i)
          cna%type(cna_counter) = neighbor_type(i)
       end if
       ! find the mapping for this signature
       cna_id = minloc(cna%sig, 1, cna%sig .eq. cna_signature(i) .and. cna%type .eq. neighbor_type(i) )
       ! increment the counter
       cna%counts(cna_id) = cna%counts(cna_id) + 1
    end do

    ! if( count( cna%counts .gt. 0 ) .gt. 0 ) then

    !    i = particle_id
    !    write(*,'("cna_signature (",I0,"):")',advance='no') particle_id
    !    do j = 1, count( cna%counts .gt. 0 )
    !       write(*,'(" ",I0,"x",A,I0,",")',advance='no') cna%counts(j), trim(cna%type(j)), cna%sig(j)
    !    end do
    !    write(*,*)

    ! else

    !    write(*,'("particle ",I0," has no cna signature. N0 = ",I0,"; xyz = ",3F10.3)') &
    !         particle_id, nl%N0(particle_id), system%position(:,particle_id)

    ! end if

  end subroutine common_neighbor_analysis

  !============================================================!
  !     compare cluster signatures to reference structures     !
  !============================================================!
  subroutine match_clusters_cna(system,nl,parameters,cna_list,identities)
    implicit none
    ! external variables
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: nl
    type (parameter_struct), intent(inout) :: parameters
    type (cna_struct), dimension(:), intent(inout) :: cna_list
    type (identity_struct), intent(inout) :: identities
    ! internal variables
    type (identity_struct) :: temp_identities ! for buffering
    character(len=100) :: temp
    integer :: i, j, k, m, n
    integer :: particle_id, j_id, match_type
    integer :: cluster_size
    integer, dimension(system%num_particles) :: map

    real(P), dimension(max_pairs) :: angles, cumulative_angles

    integer :: match_count

    ! create a copy of the global identities
    ! identities = global_identities

    ! reset the bond angles on a new snapshot
    call reset_identities(identities)
    num_structures = 0

    do i = 1, (nl%num_clusters-1)
       match_type = 0
       ! use the first particle in the cluster arbitrarily
       !     (all particles have identical cna signatures)
       ! particle_id = nl%map(1,i)
       particle_id = minloc(nl%reverse_map,1,nl%reverse_map.eq.i)
       ! loop over possible reference structures
       !     (stopping when a perfect match is found)
       j =  0
       do while( j .lt. num_structures .and. match_type .eq. 0 )
          j = j + 1
          ! check for perfect match
          match_count = count_cna_matches( cna_list(particle_id), identities%cna(j) )
          if( match_count .eq. sum(identities%cna(j)%counts) &
               .and. match_count .eq. sum(cna_list(particle_id)%counts) ) then
             match_type = j
          end if
       end do

       if( match_type .gt. 0 ) then
          ! write(*,*)
          ! write(*,'("cluster ",I0," is a perfect match for ",A)') i, trim(identities%name(match_type))

          ! assign the match to all particles in the cluster
          ! do j = 1, count(nl%map(:,i) .gt. 0)
          !   j_id = nl%map(j,i)
          ! construct a map for this cluster
          map = 0
          map = pack([(j,j=1,size(nl%reverse_map))],nl%reverse_map.eq.i,map)
          do j = 1, count(map .gt. 0)
             j_id = map(j)
             nl%type(j_id) = match_type
          end do

          cumulative_angles = 0.0_P
          n = 1
          ! add these bond angles to the reference
          ! cumulative_angles = 0.0_P
          ! m = count(nl%map(:,i) .gt. 0)
          ! do j = 1, m
          !    j_id = nl%map(j,i)
          !    nl%type(j_id) = match_type

          !    ! try to evaluate bond angles
          !    angles = list_bond_angles(j_id, nl%neighbor_map(1:nl%N0(j_id),j_id), system, nl)

          !    n = nl%N0(j_id) * ( nl%N0(j_id) - 1 ) / 2

          !    call quicksort_real(angles(1:n))

          !    cumulative_angles(1:n) = cumulative_angles(1:n) + angles(1:n)
          ! end do

          cumulative_angles(1:n) = cumulative_angles(1:n)
          cumulative_angles((n+1):) = -infinity

          ! add the new angles with the appropriate weight
          identities%baa(match_type)%angles = real(identities%baa(match_type)%num_obs,P) &
               * identities%baa(match_type)%angles + cumulative_angles
          ! tally the new total count
          identities%baa(match_type)%num_obs = identities%baa(match_type)%num_obs + m
          ! normalize the angles by the new total
          identities%baa(match_type)%angles = identities%baa(match_type)%angles &
               / real(identities%baa(match_type)%num_obs,P)

          ! write(*,'("added ",I0," observations to the bond angles for a total of ",I0)') &
          !      m, identities%baa(match_type)%num_obs

       else

          ! write(*,*)
          ! write(*,'("cluster ",I0," did not match a reference structure: " )',advance='no') i
          num_structures = num_structures + 1

          ! keep a buffer of identity structs
          if( num_structures .ge. ref_size ) then
             write(*,'("expanding reference library from ",I0," to ",I0," entries")') &
                  ref_size, ref_size + n_ref_buffer
             ref_size = ref_size + n_ref_buffer
             ! create a new identity struct with buffer space
             call copy_identities(parameters,identities,temp_identities)
             ! replace the identities with the new resized one
             identities = temp_identities
          end if

          identities%name(num_structures) = ""
          do k = 1, count(cna_list(particle_id)%counts .gt. 0)
             write(temp,'(I0,"x",A,I0,"/")') &
                  cna_list(particle_id)%counts(k), &
                  trim(cna_list(particle_id)%type(k)), &
                  cna_list(particle_id)%sig(k)
             j = len(trim(identities%name(num_structures)))
             identities%name(num_structures)((j+1):(j+len(trim(temp)))) = trim(temp)
          end do
          ! write(*,'(A)',advance='no') trim(identities%name(num_structures))
          ! write(*,'("(it will be assigned as a new reference structure, no.",I0,")")') num_structures
          identities%cna(num_structures) = cna_list(particle_id)

          map = 0
          map = pack([(j,j=1,size(nl%reverse_map))],nl%reverse_map.eq.i,map)
          do j = 1, count(map .gt. 0)
             j_id = map(j)
             nl%type(j_id) = match_type
          end do

          match_type = num_structures
          cumulative_angles = 0.0_P
          n = 1
          ! assign the match to all particles in the cluster
          ! m = count(nl%map(:,i) .gt. 0)
          ! n = 0
          ! do j = 1, m
          !    j_id = nl%map(j,i)
          !    nl%type(j_id) = match_type

          !    ! try to evaluate bond angles
          !    angles = list_bond_angles(j_id, nl%neighbor_map(1:nl%N0(j_id),j_id), system, nl)

          !    n = nl%N0(j_id) * ( nl%N0(j_id) - 1 ) / 2

          !    call quicksort_real(angles(1:n))

          !    cumulative_angles(1:n) = cumulative_angles(1:n) + angles(1:n)
          ! end do

          cumulative_angles(1:n) = cumulative_angles(1:n) / real(m,P)
          cumulative_angles((n+1):) = -infinity

          identities%baa(num_structures)%angles = cumulative_angles
          identities%baa(num_structures)%num_obs = m

       end if
    end do

    ! compute the rmsd for each particle
    !     it will be used later for self-similarity metric
    nl%likelihood = -infinity
    do i = 1, system%num_particles
       if( nl%type(i) .gt. 0 ) then
          angles = list_bond_angles(i, nl%neighbor_map(1:nl%N0(i),i), system, nl)
          nl%likelihood(i) = match_residual(angles, identities%baa(nl%type(i))%angles)
       end if
    end do

    ! compute self-similarity for all reference
    !     structures that appear in this snapshot
    identities%p = -1.0
    do i = 1, num_structures
       if( count( nl%type .eq. i ) .gt. 0 ) then
          ! check self-similarity
          identities%p(i) = sum( nl%likelihood, 1, nl%type .eq. i ) / real( count( nl%type .eq. i ), P )
       end if
    end do

  end subroutine match_clusters_cna

  !============================================================!
  !     assign singleton clusters to the nearest reference     !
  !============================================================!
  subroutine assign_singleton_cna(parameters,system,nl,cna_list,identities)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),  intent(inout) :: system
    type (cluster_struct), intent(inout) :: nl
    type (cna_struct), dimension(:), intent(inout) :: cna_list
    type (identity_struct), intent(inout) :: identities
    ! internal variables
    integer :: j, k, l
    integer :: particle_id, best_match, j_id
    real(P) :: rmsd, lowest_rmsd, mean_rmsd
    real(P), dimension(max_pairs) :: angles
    integer, dimension(num_structures) :: visited_structures
    integer, dimension(system%num_particles) :: temp_type

    l = 0
    mean_rmsd = 0.0

    temp_type = nl%type

    do particle_id = 1, system%num_particles

       if( nl%type(particle_id) .eq. 0 ) then
          ! write(*,'("assigning particle ",I0,"...")',advance='no') particle_id

          lowest_rmsd = infinity
          best_match  = 0
          visited_structures = 0

          angles = list_bond_angles(particle_id, &
               nl%neighbor_map(1:nl%N0(particle_id),particle_id), system, nl)

          do j = 1, nl%N0(particle_id)

             j_id = nl%neighbor_map(j,particle_id)
             k = nl%type(j_id)

             if( k .gt. 0 ) then
                if( visited_structures(k) .eq. 0 ) then
                   visited_structures(k) = 1

                   rmsd = match_residual(angles, identities%baa(k)%angles)
                   if( rmsd .lt. lowest_rmsd ) then
                      lowest_rmsd = rmsd
                      best_match = k
                   end if

                end if
             end if

          end do

          if( best_match .gt. 0 .and. best_match .lt. infinity ) then
             ! write(*,'(I0,F10.5)') best_match, lowest_rmsd
             temp_type(particle_id) = best_match
             nl%likelihood(particle_id) = lowest_rmsd

             mean_rmsd = mean_rmsd + lowest_rmsd
             l = l + 1
          elseif( best_match .ge. infinity ) then
             write(*,'("no good match")')
             temp_type(particle_id) = 0
             nl%likelihood(particle_id) = 0.0
          else
             ! write(*,'("none")')
             temp_type(particle_id) = best_match
             nl%likelihood(particle_id) = 0.0
          end if

       end if

    end do

    nl%type = temp_type
    mean_rmsd = mean_rmsd / real(l,P)

    ! write(*,'("mean_rmsd for singleton particles = ",F10.5)') mean_rmsd

    ! ! compute mean rmsd for clusters
    ! j = 0
    ! do i = 1, num_structures
    !    ! only count the reference structures that got matched
    !    if( count( nl%type .eq. i ) .gt. 0 ) then
    !       structure_rmsd(i) = identities%p(i)
    !       j = j + 1
    !    else
    !       structure_rmsd(i) = -infinity
    !    end if
    ! end do
    ! mean_rmsd = sum(structure_rmsd, 1, structure_rmsd .gt. -1 ) / real(j,P)

    ! ! get the median as well
    ! !     need to shift the starting point by however many -Inf's we have
    ! call quicksort_real(structure_rmsd)
    ! k = minloc( structure_rmsd, 1, structure_rmsd .gt. - 1 )
    ! if( mod( j, 2 ) .eq. 0 ) then
    !    median_rmsd = ( structure_rmsd( k + (j/2) ) &
    !         + structure_rmsd( k + (j/2) + 1 ) ) * 0.5_P
    ! else
    !    median_rmsd = structure_rmsd( k + (j/2) )
    ! end if

    ! write(*,'("mean_rmsd for assigned structures = ",F10.5)') mean_rmsd
    ! write(*,'("median    for assigned structures = ",F10.5)') median_rmsd

    ! parameters%mean_structure_rmsd = mean_rmsd
    ! parameters%median_structure_rmsd = median_rmsd

  end subroutine assign_singleton_cna

  !============================================================!
  !     compare cluster signatures to reference structures     !
  !============================================================!
  subroutine cluster_neighbors(system,nl,config_file,suffix)
    implicit none
    ! external variables
    type (system_struct),  intent(inout) :: system
    type (cluster_struct), intent(inout) :: nl
    character(len=*), intent(in) :: config_file,suffix
    ! internal variables
    integer :: f_dat = 420
    integer, dimension(system%num_particles,system%num_particles) :: neighbors
    integer, dimension(num_structures,num_structures) :: counts
    integer :: i, j, k
    integer :: k_id

    ! initialize data structures
    neighbors = -1
    counts    = 0

    ! loop over particles
    do i = 1, system%num_particles

       ! get the cluster this particle belongs to
       j = nl%type(i)

       ! only do this for assigned particles
       if( j .gt. 0 ) then
          ! loop over this particle's neighbors
          do k = 1, nl%N0(i)
             k_id = nl%neighbor_map(k,i)
             ! add the particle id to the unique neighbors list
             call add_unique_element(k_id,neighbors(:,j))
          end do
       end if

    end do

    ! loop over cluster neighbors once the unique list is build
    !     this is necessary to avoid double-counting
    do j = 1, num_structures
       do k = 1, count(neighbors(:,j) .gt. -1)
          ! get the cluster this particle belongs to
          k_id = nl%type(neighbors(k,j))
          ! do not record unassigned particles
          if( k_id .gt. 0 ) then
             ! record the bond
             counts(k_id,j) = counts(k_id,j) + 1
          end if
       end do
    end do

    ! open data file
    open(unit=f_dat,file=trim(config_file)//'.'//trim(suffix)//'.'//'cluster_edges',&
         action='write',status='unknown')

    ! write to file
    do j = 1, num_structures
       do k = 1, num_structures
          write(f_dat,'(I0," ")',advance='no') counts(k,j)
       end do
       write(f_dat,*)
    end do

    close(f_dat)

  end subroutine cluster_neighbors

  !=========================================!
  !     find longest path in a subgraph     !
  !=========================================!
  subroutine find_longest_path(neighbor_path_length,common_neighbors,neighbor_map,neighbor_bond_count)
    implicit none
    ! external variables
    integer, dimension(:,:), intent(inout) :: common_neighbors
    integer, dimension(:),   intent(inout) :: neighbor_map, neighbor_path_length, neighbor_bond_count
    ! internal variables
    integer, dimension(max_pairs) :: allowed_ids
    integer, dimension(max_pairs) :: longest_path, temp_longest_path
    integer :: path_length, temp_path_length
    integer :: i_neighbors, particle_id
    integer :: i, j

    ! reset particle list
    do i = 1, count(neighbor_map .gt. 0)
       i_neighbors = count(common_neighbors(:,i) .gt. 0 )
       allowed_ids = common_neighbors(:,i)
       ! print*,'searching for the following ids:',allowed_ids(1:i_neighbors)

       path_length = 0
       longest_path = 0

       ! perform search of neighbor tree to identify chains
       do j = 1, i_neighbors
          particle_id = common_neighbors(j,i)

          ! reset the allowed ids every time we start from a new root
          allowed_ids(1:i_neighbors) = common_neighbors(1:i_neighbors,i)

          ! reset longest path
          temp_path_length  = 1
          temp_longest_path = 0
          temp_longest_path(1) = particle_id

          ! print*,'starting recursive search at root particle',particle_id

          ! search the tree
          call path_recursion(particle_id,temp_path_length,temp_longest_path,allowed_ids,&
               common_neighbors,neighbor_map,neighbor_bond_count(i))

          if( temp_path_length .gt. path_length ) then
             ! update the results
             path_length = temp_path_length
             longest_path = temp_longest_path
          end if

       end do

       ! report the results
       ! write(*,'(I0," : longest path has ",I0," bonds")') i,path_length - 1
       ! print*,i,': longest path is',longest_path(1:path_length)
       if( path_length .gt. 0 ) then
          neighbor_path_length(i) = path_length - 1
       else
          neighbor_path_length(i) = 0
       end if

    end do

  end subroutine find_longest_path

  !=========================================!
  !     wrapper for longest path search     !
  !=========================================!
  integer function find_longest_path_graph(i,nn)
    implicit none
    ! external variables
    integer, intent(inout) :: i
    integer, dimension(:,:), intent(inout) :: nn
    ! internal variables
    integer :: j
    integer, dimension(size(nn,1)) :: allowed, path, longest_path

    allowed = 0
    path = 0
    longest_path = 0

    do j = 1, count( nn(:,i) .gt. 0 )
       allowed(j) = nn(j,i)
    end do

    ! print*,'allowed:',allowed(1:count(allowed.gt.0))
    call local_graph(allowed,nn,path,longest_path)

    find_longest_path_graph = count(longest_path .gt. 0) - 1
    if( find_longest_path_graph .lt. 0 ) find_longest_path_graph = 0
    ! print*,'longest_path:',find_longest_path_graph
    ! print*,''

  end function find_longest_path_graph

  !=====================================================!
  !     recursively search network for longest path     !
  !=====================================================!
  recursive subroutine local_graph(allowed,nn,path,longest_path)
    implicit none
    ! external variables
    integer, dimension(:), intent(inout) :: allowed
    integer, dimension(:,:), intent(inout) :: nn
    integer, dimension(:), intent(inout) :: path, longest_path
    ! internal variables
    integer :: l, i, j
    integer, dimension(size(allowed)) :: local_allowed
    integer, dimension(size(path)) :: new_path

    l = count(path .gt. 0)
    if( l .gt. count(longest_path .gt. 0) ) &
         longest_path = path
    ! reset allowed moves
    if( l .eq. 0 ) then
       local_allowed = allowed
    else
       local_allowed = 0
       j = 0
       do i = 1, count(allowed .gt. 0)
          if( any( nn(:,path(l)) .eq. allowed(i) ) ) then
             j = j + 1
             local_allowed(j) = allowed(i)
          end if
       end do
       ! exclude first previous node from locally allowed moves
       do i = 1, count(local_allowed .gt. 0)
          if( local_allowed(i) .eq. path(l) ) &
               local_allowed(i) = 0
       end do
       ! exclude second previous node from locally allowed moves
       if( l .gt. 1 ) then
          do i = 1, count(allowed .gt. 0)
             if( local_allowed(i) .eq. path(l-1) ) &
                  local_allowed(i) = 0
          end do
       end if
    end if
    ! loop over allowed moves
    do i = 1, count(allowed .gt. 0)
       if( local_allowed(i) .gt. 0 ) then
          new_path = path
          new_path(l+1) = local_allowed(i)
          if( any( new_path .gt. maxval(allowed) ) ) then
             print*,'error in local_allowed:',local_allowed
          end if
          if( .not. any( new_path(1:l) .eq. local_allowed(i) ) ) &
               call local_graph(allowed,nn,new_path,longest_path)
          if( count(new_path .gt. 0) .gt. count(longest_path .gt. 0) ) &
               longest_path = new_path
          if( count(longest_path .gt. 0) .gt. count(allowed .gt. 0) ) &
               return
       end if
    end do

  end subroutine local_graph

  !=====================================================!
  !     recursively search network for longest path     !
  !=====================================================!
  recursive subroutine path_recursion(particle_id,path_length,longest_path,allowed_ids,common_neighbors,neighbor_map,max_length)
    implicit none
    ! external variables
    integer,                 intent(inout) :: particle_id, path_length, max_length
    integer, dimension(:),   intent(inout) :: allowed_ids, neighbor_map, longest_path
    integer, dimension(:,:), intent(inout) :: common_neighbors
    ! internal variables
    integer, dimension(size(longest_path)) :: temp_longest_path
    integer, dimension(size(allowed_ids))  :: temp_allowed_ids
    integer :: temp_path_length
    integer :: neighbor_counter, neighbor_id
    integer :: map_to_common

    ! remove this particle from the search tree
    where( allowed_ids .eq. particle_id )
       allowed_ids = 0
    end where


    map_to_common = minloc( neighbor_map, 1, neighbor_map .eq. particle_id )

    ! print*,'new allowed_ids:',allowed_ids(1:count(common_neighbors(:,map_to_common) .gt. 0))

    ! loop over nearest neighbors of this particle
    do neighbor_counter = 1, count(common_neighbors(:,map_to_common) .gt. 0 )

       ! identify the neighbor_id from the common_neighbors list
       neighbor_id = common_neighbors(neighbor_counter,map_to_common)

       ! only continue down this branch if it is among the allowed ids
       if( any(allowed_ids .eq. neighbor_id) ) then

          ! remove from temporary copy of allowed_ids
          temp_allowed_ids = allowed_ids
          where( temp_allowed_ids .eq. neighbor_id )
             temp_allowed_ids = 0
          end where

          ! increment temporary path length
          temp_path_length = path_length + 1

          ! record path
          temp_longest_path = longest_path
          temp_longest_path(temp_path_length) = neighbor_id

          if( temp_path_length .le. max_length ) then
             ! branch down to continue the path
             ! print*,'calling recursion on particle',neighbor_id,'from particle',particle_id
             ! print*,'    with path list',temp_longest_path(1:temp_path_length)
             call path_recursion(neighbor_id,temp_path_length,temp_longest_path,temp_allowed_ids,&
                  common_neighbors,neighbor_map,max_length)
          end if

          ! overwrite with the new (temporary) values once recursion stops
          if( temp_path_length .gt. path_length ) then
             path_length = temp_path_length
             longest_path = temp_longest_path
             ! print*,'new longest path is',longest_path(1:path_length)
             ! print*,'new longest path is',path_length
          end if

          if( path_length .eq. max_length + 1 ) then
             return
          end if

       end if

    end do

    ! print*,'going up a level'
    ! print*,''

  end subroutine path_recursion

  !============================================!
  !     look for clusters in the cna graph     !
  !============================================!
  recursive subroutine evaluate_cna_subgraph(particle_id,tol,system,nl,cna_list)
    implicit none
    ! external variables
    integer, intent(inout) :: particle_id, tol
    type (system_struct),  intent(inout) :: system
    type (cluster_struct), intent(inout) :: nl
    type (cna_struct), dimension(:), intent(inout) :: cna_list
    ! internal variables
    integer :: i, j, j_id

    nl%particle_list(particle_id) = 0

    ! report its id and position
    write(*,'(A)')
    write(*,'("particle ",I0," is located at (",3F10.5,")")') &
         particle_id, system%position(:,particle_id)

    ! report its cna signature
    write(*,'("its CNA signature is: ")',advance='no')
    i = 1
    do while(cna_list(particle_id)%counts(i) .gt. 0)
       write(*,'(I0,"x",I0," ")',advance='no') &
            cna_list(particle_id)%counts(i), cna_list(particle_id)%sig(i)
       i = i + 1
    end do

    ! recursively loop over neighbors that have high match counts
    do i = 1, nl%N0(particle_id)
       j = 1
       j_id = nl%neighbor_map(i,particle_id)
       if( count_cna_matches(cna_list(particle_id), cna_list(j_id)) &
            .ge. min(sum(cna_list(particle_id)%counts),sum(cna_list(j_id)%counts)) - tol &
            .and. nl%particle_list(j_id) .gt. 0 ) then
          call evaluate_cna_subgraph(j_id,tol,system,nl,cna_list)
       end if
    end do

  end subroutine evaluate_cna_subgraph

  subroutine compute_similarity_cna(parameters,system,nl,cna_list)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),  intent(inout) :: system
    type (cluster_struct), intent(inout) :: nl
    type (cna_struct), dimension(:), intent(inout) :: cna_list
    ! internal variables
    integer, parameter :: f_dat = 410
    integer :: i, j, s

    ! write directly to file -- the operation is cheap
    open(unit=f_dat,file=trim(parameters%config_file)//'.sim',action='write',status='unknown')

    do i = 1, system%num_particles
       do j = 1, system%num_particles
          if( i .eq. j ) then
             s = 0
          else
             s = count_cna_matches( cna_list(i), cna_list(j) )
          end if
          write(f_dat,'(I0," ")',advance='no') s
       end do
       write(f_dat,'(A)')
    end do

    close(f_dat)

  end subroutine compute_similarity_cna

end module cna
