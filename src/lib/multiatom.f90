module multiatom
  use datatypes
  use util
  implicit none
contains

  !============================================================!
  !     compare cluster signatures to reference structures     !
  !============================================================!
  subroutine identify_multiatom(system,nl,parameters,cna_list,ma_list)
    implicit none
    ! external variables
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: nl
    type (parameter_struct), intent(inout) :: parameters
    type (cna_struct),       dimension(:), intent(inout) :: cna_list
    type (multiatom_struct), dimension(:), intent(inout) :: ma_list
    ! internal variables
    logical :: new_ma
    type (multiatom_struct) :: ma_temp
    integer :: i, j, k, m, n
    integer :: i_neighbors, j_neighbors
    integer :: j_id, k_id
    integer :: n_full, n_min, match_count
    integer :: ma_count, edge_count, tess_count
    integer, dimension(system%num_particles) :: i_neighbor_count
    logical, dimension(system%num_particles) :: i_neighbor_identical
    logical, dimension(max_pairs) :: cell_include
    integer, dimension(max_pairs) :: all_ids
    integer, dimension(max_pairs,system%num_particles) :: associated_ids
 
    n = system%num_particles / 10
    ma_count = 0
    associated_ids = 0

    ! set up memory for multi-atom patterns
    allocate(ma_temp%all_center_id(n))
    ma_temp%all_center_id = 0
    
    allocate(ma_temp%all_edge_id(n))
    ma_temp%all_edge_id   = 0
    
    allocate(ma_temp%all_tess_id(n))
    ma_temp%all_tess_id   = 0
   
    allocate(ma_temp%all_assoc_id(n))
    ma_temp%all_assoc_id   = 0
    
    do i = 1, size(ma_list)
       allocate(ma_list(i)%all_center_id(n))
       ma_list(i)%all_center_id = 0       
       allocate(ma_list(i)%all_edge_id(n))
       ma_list(i)%all_edge_id = 0
       allocate(ma_list(i)%all_tess_id(n))
       ma_list(i)%all_tess_id = 0

       allocate(ma_list(i)%all_assoc_id(n))
       ma_list(i)%all_assoc_id = 0
    end do

    ! check every particle for participation in a multi-atom pattern
    do i = 1, system%num_particles
       all_ids = 0
       call add_unique_element_pos(i,all_ids)
       i_neighbors = nl%N0(i)
       i_neighbor_count = 0
       i_neighbor_identical = .false.
       i_neighbor_identical(i) = .true.
       ! write(*,'(I0," (",I0,"):")',advance='no') i, i_neighbors
       ! loop over neighbors
       do j = 1, i_neighbors
          ! convert neighbor number to global id
          j_id = nl%neighbor_map(j,i)
          call add_unique_element_pos(j_id,all_ids)
          ! write(*,'(" ",I0)',advance='no') j_id
          i_neighbor_count(j_id) = i_neighbor_count(j_id) + 1
          j_neighbors = nl%N0(j_id)
          ! write(*,'(" ",I0," (",I0,"):")',advance='no') j_id, j_neighbors
          ! loop over neighbors' neighbors
          do k = 1, j_neighbors
             ! convert neighbor number to global id
             k_id = nl%neighbor_map(k,j_id)
             ! write(*,'(" ",I0)',advance='no') k_id
             ! count occurences
             i_neighbor_count(k_id) = i_neighbor_count(k_id) + 1
             call add_unique_element_pos(k_id,all_ids)
          end do
       end do
       ! write(*,*)
       ! find fully-bonded particles (these participate in the unit cell)
       n_full = maxval(i_neighbor_count, 1, .not. i_neighbor_identical)
       if( n_full .gt. 0 ) then
          ! find least bonded particles (these are centers of adjacent unit cells)
          n_min = minval(i_neighbor_count,1,i_neighbor_count .gt. 0)
       end if
       if( n_full .gt. 1 ) then
          ! reset ma_temp
          ma_temp%edge_id = 0
          ma_temp%tess_id = 0
          ma_temp%all_assoc_id = 0

          ! assign new data
          ma_temp%center_id   = i
          ma_temp%center_type = system%type(i)
          ma_temp%center_cna  = cna_list(i)

          ! report CNA signatures of fully bonded particles
          write(*,'("found a multi-atom pattern (i=",I0,",n_full=",I0,",n_min=",I0,"):")',advance='no') &
               i, n_full, n_min
          j_id = 0
          do j = 1, count( i_neighbor_count .eq. n_full )
             j_id = j_id + 1
             j_id = minloc( i_neighbor_count(j_id:n), 1, i_neighbor_count(j_id:n) .eq. n_full ) + j_id - 1
             write(*,'(" ",I0)',advance='no') j_id
             associated_ids(j,i) = j_id
          end do
          write(*,*)

          ! THINGS THAT HAVE N_FULL COMMON NEIGHBORS ARE EDGES
          print*,'n_full:'
          j_id = 0
          do j = 1, count( i_neighbor_count .eq. n_full )
             write(*,'("     ")',advance='no')
             j_id = j_id + 1
             j_id = minloc( i_neighbor_count(j_id:n), 1, i_neighbor_count(j_id:n) .eq. n_full ) + j_id - 1

             edge_count = count( ma_temp%edge_id .gt. 0 ) + 1
             ma_temp%edge_id(edge_count)   = j_id
             ma_temp%edge_type(edge_count) = system%type(j_id)
             ma_temp%edge_cna(edge_count)  = cna_list(j_id)

             write(*,'(I0,": ")',advance='no') j_id
             do k = 1, count(cna_list(j_id)%counts .gt. 0)
                write(*,'(I0,"x",A,I0,"/")',advance='no') &
                     cna_list(j_id)%counts(k), &
                     trim(cna_list(j_id)%type(k)), &
                     cna_list(j_id)%sig(k)
             end do
             write(*,*)
          end do

          ! THINGS THAT MATCH PERFECTLY AND HAVE N_MIN COMMON NEIGHBORS
          !   ARE POTENTIAL TESSELLATION POINTS
          print*,'n_min:'
          j_id = 0
          do j = 1, count( i_neighbor_count .eq. n_min )
             j_id = j_id + 1
             j_id = minloc( i_neighbor_count(j_id:n), 1, i_neighbor_count(j_id:n) .eq. n_min ) + j_id - 1

             ! check for perfect match
             match_count = count_cna_matches( cna_list(i), cna_list(j_id) )
             if( match_count .eq. sum(cna_list(i)%counts) &
                  .and. match_count .eq. sum(cna_list(j_id)%counts) &
                  .and. system%type(i) .eq. system%type(j_id) ) then

                tess_count = count( ma_temp%tess_id .gt. 0 ) + 1
                ma_temp%tess_id(tess_count)   = j_id
                ma_temp%tess_type(tess_count) = system%type(j_id)
                ma_temp%tess_cna(tess_count)  = cna_list(j_id)

                write(*,'("     ")',advance='no')
                write(*,'(I0,": ")',advance='no') j_id
                do k = 1, count(cna_list(j_id)%counts .gt. 0)
                   write(*,'(I0,"x",A,I0,"/")',advance='no') &
                        cna_list(j_id)%counts(k), &
                        trim(cna_list(j_id)%type(k)), &
                        cna_list(j_id)%sig(k)
                end do
                write(*,*)

             end if
          end do
          write(*,*)

          ! check whether these tessellation points define a unit cell
          !   (they must collectively include all associated particles)
          write(*,'("found: ")',advance='no')
          cell_include = .false.

          ! include center points themselves
          m = minloc( all_ids, 1, all_ids .eq. i )
          if( m .gt. 0 ) then
             if( .not. cell_include(m) ) then
                write(*,'(I0,"(C) ")',advance='no') i
                cell_include( m ) = .true.
             end if
          end if

          do k = 1, nl%N0(i)
             k_id = nl%neighbor_map(k,i)
             m = minloc( all_ids, 1, all_ids .eq. k_id )
             if( m .gt. 0 ) then
                if( .not. cell_include(m) ) then
                   write(*,'(I0," ")',advance='no') k_id
                   cell_include( m ) = .true.
                end if
             end if
          end do

          do j = 1, count( ma_temp%tess_id .gt. 0 )
             j_id = ma_temp%tess_id(j)

             ! include tessellation points themselves
             m = minloc( all_ids, 1, all_ids .eq. j_id )
             if( m .gt. 0 ) then
                if( .not. cell_include(m) ) then
                   write(*,'(I0,"(T) ")',advance='no') j_id
                   cell_include( m ) = .true.
                end if
             end if

             do k = 1, nl%N0(j_id)
                k_id = nl%neighbor_map(k,j_id)
                m = minloc( all_ids, 1, all_ids .eq. k_id )
                if( m .gt. 0 ) then
                   if( .not. cell_include(m) ) then
                      write(*,'(I0," ")',advance='no') k_id
                      cell_include( m ) = .true.
                   end if
                end if
             end do
          end do
          write(*,*)

          if( .not. all( cell_include(1:count(all_ids.gt.0)) ) ) then

             write(*,'("Proposed tessellation points do not include all associated particles. Rejecting proposed MAP.")')
             print*,all_ids(1:count(all_ids.gt.0))
             write(*,'("NOT found: ")',advance='no')
             do j = 1, count(all_ids.gt.0)
                if( .not. cell_include(j) ) write(*,'(I0," ")',advance='no') all_ids(j)
             end do
             write(*,*)

             ! try with next highest number of bonds ("yellow" particles)
             ! find least bonded particles (these are centers of adjacent unit cells)
             n_min = minval(i_neighbor_count,1,i_neighbor_count .gt. n_min)
             ma_temp%tess_id = 0
             ! THINGS THAT MATCH PERFECTLY AND HAVE N_MIN COMMON NEIGHBORS
             !   ARE POTENTIAL TESSELLATION POINTS
             print*,'n_min:'
             j_id = 0
             do j = 1, count( i_neighbor_count .eq. n_min )
                j_id = j_id + 1
                j_id = minloc( i_neighbor_count(j_id:n), 1, i_neighbor_count(j_id:n) .eq. n_min ) + j_id - 1

                ! check for perfect match
                match_count = count_cna_matches( cna_list(i), cna_list(j_id) )
                if( match_count .eq. sum(cna_list(i)%counts) &
                     .and. match_count .eq. sum(cna_list(j_id)%counts) &
                     .and. system%type(i) .eq. system%type(j_id) ) then

                   tess_count = count( ma_temp%tess_id .gt. 0 ) + 1
                   ma_temp%tess_id(tess_count)   = j_id
                   ma_temp%tess_type(tess_count) = system%type(j_id)
                   ma_temp%tess_cna(tess_count)  = cna_list(j_id)

                   write(*,'("     ")',advance='no')
                   write(*,'(I0,": ")',advance='no') j_id
                   do k = 1, count(cna_list(j_id)%counts .gt. 0)
                      write(*,'(I0,"x",A,I0,"/")',advance='no') &
                           cna_list(j_id)%counts(k), &
                           trim(cna_list(j_id)%type(k)), &
                           cna_list(j_id)%sig(k)
                   end do
                   write(*,*)

                end if
             end do
             write(*,*)

             ! check whether these tessellation points define a unit cell
             !   (they must collectively include all associated particles)
             write(*,'("found: ")',advance='no')
             cell_include = .false.

             ! include center points themselves
             m = minloc( all_ids, 1, all_ids .eq. i )
             if( m .gt. 0 ) then
                if( .not. cell_include(m) ) then
                   write(*,'(I0,"(C) ")',advance='no') i
                   cell_include( m ) = .true.
                end if
             end if

             do k = 1, nl%N0(i)
                k_id = nl%neighbor_map(k,i)
                m = minloc( all_ids, 1, all_ids .eq. k_id )
                if( m .gt. 0 ) then
                   if( .not. cell_include(m) ) then
                      write(*,'(I0," ")',advance='no') k_id
                      cell_include( m ) = .true.
                   end if
                end if
             end do

             do j = 1, count( ma_temp%tess_id .gt. 0 )
                j_id = ma_temp%tess_id(j)

                ! include tessellation points themselves
                m = minloc( all_ids, 1, all_ids .eq. j_id )
                if( m .gt. 0 ) then
                   if( .not. cell_include(m) ) then
                      write(*,'(I0,"(T) ")',advance='no') j_id
                      cell_include( m ) = .true.
                   end if
                end if

                do k = 1, nl%N0(j_id)
                   k_id = nl%neighbor_map(k,j_id)
                   m = minloc( all_ids, 1, all_ids .eq. k_id )
                   if( m .gt. 0 ) then
                      if( .not. cell_include(m) ) then
                         write(*,'(I0," ")',advance='no') k_id
                         cell_include( m ) = .true.
                      end if
                   end if
                end do
             end do
             write(*,*)

             if( .not. all( cell_include(1:count(all_ids.gt.0)) ) ) then

                write(*,'("Proposed tessellation points do not include all associated particles. Rejecting proposed MAP.")')
                print*,all_ids(1:count(all_ids.gt.0))
                write(*,'("NOT found: ")',advance='no')
                do j = 1, count(all_ids.gt.0)
                   if( .not. cell_include(j) ) write(*,'(I0," ")',advance='no') all_ids(j)
                end do
                write(*,*)

                ! USE GREEN + YELLOW
                ! find least bonded particles (these are centers of adjacent unit cells)
                n_min = minval(i_neighbor_count,1,i_neighbor_count .gt. 0)
                ! THINGS THAT MATCH PERFECTLY AND HAVE N_MIN COMMON NEIGHBORS
                !   ARE POTENTIAL TESSELLATION POINTS
                print*,'n_min:'
                j_id = 0
                do j = 1, count( i_neighbor_count .eq. n_min )
                   j_id = j_id + 1
                   j_id = minloc( i_neighbor_count(j_id:n), 1, i_neighbor_count(j_id:n) .eq. n_min ) + j_id - 1

                   ! check for perfect match
                   match_count = count_cna_matches( cna_list(i), cna_list(j_id) )
                   if( match_count .eq. sum(cna_list(i)%counts) &
                        .and. match_count .eq. sum(cna_list(j_id)%counts) &
                        .and. system%type(i) .eq. system%type(j_id) ) then

                      tess_count = count( ma_temp%tess_id .gt. 0 ) + 1
                      ma_temp%tess_id(tess_count)   = j_id
                      ma_temp%tess_type(tess_count) = system%type(j_id)
                      ma_temp%tess_cna(tess_count)  = cna_list(j_id)

                      write(*,'("     ")',advance='no')
                      write(*,'(I0,": ")',advance='no') j_id
                      do k = 1, count(cna_list(j_id)%counts .gt. 0)
                         write(*,'(I0,"x",A,I0,"/")',advance='no') &
                              cna_list(j_id)%counts(k), &
                              trim(cna_list(j_id)%type(k)), &
                              cna_list(j_id)%sig(k)
                      end do
                      write(*,*)

                   end if
                end do
                write(*,*)

                ! check whether these tessellation points define a unit cell
                !   (they must collectively include all associated particles)
                write(*,'("found: ")',advance='no')
                cell_include = .false.

                ! include center points themselves
                m = minloc( all_ids, 1, all_ids .eq. i )
                if( m .gt. 0 ) then
                   if( .not. cell_include(m) ) then
                      write(*,'(I0,"(C) ")',advance='no') i
                      cell_include( m ) = .true.
                   end if
                end if

                do k = 1, nl%N0(i)
                   k_id = nl%neighbor_map(k,i)
                   m = minloc( all_ids, 1, all_ids .eq. k_id )
                   if( m .gt. 0 ) then
                      if( .not. cell_include(m) ) then
                         write(*,'(I0," ")',advance='no') k_id
                         cell_include( m ) = .true.
                      end if
                   end if
                end do

                do j = 1, count( ma_temp%tess_id .gt. 0 )
                   j_id = ma_temp%tess_id(j)

                   ! include tessellation points themselves
                   m = minloc( all_ids, 1, all_ids .eq. j_id )
                   if( m .gt. 0 ) then
                      if( .not. cell_include(m) ) then
                         write(*,'(I0,"(T) ")',advance='no') j_id
                         cell_include( m ) = .true.
                      end if
                   end if

                   do k = 1, nl%N0(j_id)
                      k_id = nl%neighbor_map(k,j_id)
                      m = minloc( all_ids, 1, all_ids .eq. k_id )
                      if( m .gt. 0 ) then
                         if( .not. cell_include(m) ) then
                            write(*,'(I0," ")',advance='no') k_id
                            cell_include( m ) = .true.
                         end if
                      end if
                   end do
                end do
                write(*,*)

                if( .not. all( cell_include(1:count(all_ids.gt.0)) ) ) then

                   write(*,'("Proposed tessellation points do not include all associated particles. Rejecting proposed MAP.")')
                   print*,all_ids(1:count(all_ids.gt.0))
                   write(*,'("NOT found: ")',advance='no')
                   do j = 1, count(all_ids.gt.0)
                      if( .not. cell_include(j) ) write(*,'(I0," ")',advance='no') all_ids(j)
                   end do
                   write(*,*)

                else

                   write(*,'("Green + Yellow tessellation points define a valid MAP.")')

                end if

             else
                write(*,'("Yellow tessellation points define a valid MAP.")')
             end if

          end if

          if( all( cell_include(1:count(all_ids.gt.0)) ) ) then

             ! decide whether this is a new pattern or not
             new_ma = .true.
             j = 0
             do while( j .lt. ma_count .and. new_ma )
                j = j + 1
                if( match_multiatom(ma_temp,ma_list(j)) ) new_ma = .false.
             end do

             ! STORE ALL PATTERNS SEPARATELY, FOR NOW
             new_ma = .true.
             
             ! append ma_list
             if( new_ma ) then
                ma_count = ma_count + 1
                ma_list(ma_count) = ma_temp
                print*,'new ma pattern',ma_count
                ! store center
                call add_unique_element_pos(ma_temp%center_id,ma_list(ma_count)%all_center_id)
                ! store edges
                do k = 1, count( ma_temp%edge_id .gt. 0 )
                   call add_unique_element_pos(ma_temp%edge_id(k),ma_list(ma_count)%all_edge_id)
                end do
                ! store tessellation points
                do k = 1, count( ma_temp%tess_id .gt. 0 )
                   call add_unique_element_pos(ma_temp%tess_id(k),ma_list(ma_count)%all_tess_id)
                end do
             else
                write(*,'("old ma pattern (see particle ",I0,")")' ) ma_list(j)%center_id
                ! store center
                call add_unique_element_pos(ma_temp%center_id,ma_list(j)%all_center_id)
                ! store edges
                do k = 1, count( ma_temp%edge_id .gt. 0 )
                   call add_unique_element_pos(ma_temp%edge_id(k),ma_list(j)%all_edge_id)
                end do
                ! store tessellation points
                do k = 1, count( ma_temp%tess_id .gt. 0 )
                   call add_unique_element_pos(ma_temp%tess_id(k),ma_list(j)%all_tess_id)
                end do
             end if

          end if ! if( .not. all( cell_include(1:count(all_ids.gt.0)) ) )

       end if ! if( n_full .gt. 1 )
    end do ! do i = 1, system%num_particles

    ! generate list of associated particles ...
    do i = 1, ma_count
       ! ... from centers
       do j = 1, count( ma_list(i)%all_center_id .gt. 0 )
          j_id = ma_list(i)%all_center_id(j)
          do k = 1, count( associated_ids(:,j_id) .gt. 0 )
             call add_unique_element_pos(associated_ids(k,j_id),ma_list(i)%all_assoc_id)
          end do
       end do
       ! ... from tessellation points
       do j = 1, count( ma_list(i)%all_tess_id .gt. 0 )
          j_id = ma_list(i)%all_tess_id(j)
          do k = 1, count( associated_ids(:,j_id) .gt. 0 )
             call add_unique_element_pos(associated_ids(k,j_id),ma_list(i)%all_assoc_id)
          end do
       end do
    end do

  end subroutine identify_multiatom

  !==============================================!
  !     count matches in multi-atom patterns     !
  !==============================================!
  logical function match_multiatom(ma_a,ma_b)
    implicit none
    ! external variables
    type (multiatom_struct), intent(in) :: ma_a, ma_b
    ! internal variables
    integer :: match_count, i, j, b_id
    logical, dimension(max_pairs) :: b_edge_visited

    ! default to false
    match_multiatom = .false.

    ! check centers are same type
    if( ma_a%center_type .ne. ma_b%center_type ) return

    ! check centers have matching cna signature
    match_count = count_cna_matches( ma_a%center_cna, ma_b%center_cna )
    if( match_count .ne. sum(ma_a%center_cna%counts) &
         .or. match_count .ne. sum(ma_b%center_cna%counts) ) return

    ! check same number of edges
    if( count( ma_a%edge_id .gt. 0 ) &
         .ne. count( ma_b%edge_id .gt. 0 ) ) return

    ! check edges are same type
    b_edge_visited = .false.
    do i = 1, count( ma_a%edge_id .gt. 0 )
       b_id = minloc( ma_b%edge_id, 1, &
            ma_b%edge_type .eq. ma_a%edge_type(i) .and. .not. b_edge_visited )
       if( b_id .le. 0 ) return
       b_edge_visited(b_id) = .true.
    end do
    if( .not. all(b_edge_visited(1:count(ma_b%edge_id .gt. 0))) ) return

    ! check edges have same cna signature
    b_edge_visited = .false.
    do i = 1, count( ma_a%edge_id .gt. 0 )
       j = 0
       b_id = 0
       do while( j .lt. count( ma_b%edge_id .gt. 0 ) .and. b_id .eq. 0 )
          j = j + 1
          if( .not. b_edge_visited(j) ) then
             match_count = count_cna_matches( ma_a%edge_cna(i), ma_b%edge_cna(j) )
             if( match_count .eq. sum(ma_a%edge_cna(i)%counts) &
                  .and. match_count .eq. sum(ma_b%edge_cna(j)%counts) ) b_id = j
          end if
       end do
       if( b_id .eq. 0 ) return ! no match found
       b_edge_visited(b_id) = .true.
    end do

    ! passed all tests, return true
    match_multiatom = .true.

  end function match_multiatom

end module multiatom
