module cluster
  use datatypes
  use util
  use celllist
  use sort
  implicit none

contains

  !===================================================!
  !     initialize the cluster struct                 !
  !===================================================!
  subroutine initialize_cluster(parameters,system,cluster)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    ! internal variables
    integer :: cell_id, neighbor_cell_id
    integer :: j, s, max_occupancy

    ! find most possible number of neighbors from cell list
    ! (we have the luxury of a static cell list)
    max_occupancy = 0
    do cell_id = 1, product(system%cell_list%cell_dim)
       s = 0
       do j = 1, 27
          neighbor_cell_id = system%cell_list%neighbors(j,cell_id)
          s = s + system%cell_list%cell_count(neighbor_cell_id)
       end do
       if( s .gt. max_occupancy ) then
          max_occupancy = s
       end if
    end do

    ! catch case where we do not use cell list
    if( max_occupancy .eq. 0 ) then
       max_occupancy = system%num_particles
    end if

    cluster%max_occupancy = max_occupancy
    ! write(*,'("width of neighbor list is ",I0)') cluster%max_occupancy

    ! allocate and reset particle list
    if( allocated( cluster%particle_list ) ) deallocate( cluster%particle_list )
    allocate( cluster%particle_list( system%num_particles ) )
    cluster%particle_list = 1
    ! allocate and reset map
    ! if( allocated( cluster%map ) ) deallocate( cluster%map )
    ! allocate( cluster%map( system%num_particles, system%num_particles ) )
    ! cluster%map = 0
    ! reset stats
    cluster%stats = 1
    ! allocate neighbor list and neighbor count
    if( allocated( cluster%neighbor_map ) ) deallocate( cluster%neighbor_map )
    allocate( cluster%neighbor_map( cluster%max_occupancy, system%num_particles ) )
    if( allocated( cluster%neighbor_dist ) ) deallocate( cluster%neighbor_dist )
    allocate( cluster%neighbor_dist( cluster%max_occupancy, system%num_particles ) )
    if( allocated( cluster%neighbor_count ) ) deallocate( cluster%neighbor_count )
    allocate( cluster%neighbor_count( system%num_particles ) )
    if( allocated(cluster%reverse_map  ) ) deallocate( cluster%reverse_map )
    allocate( cluster%reverse_map( system%num_particles ) )
    ! allocate particle type and adaptive cutoff radius
    if( allocated( cluster%type ) ) deallocate( cluster%type )
    allocate( cluster%type( system%num_particles ) )
    if( allocated( cluster%likelihood ) ) deallocate( cluster%likelihood )
    allocate( cluster%likelihood( system%num_particles ) )
    if( allocated( cluster%cna ) ) deallocate( cluster%cna )
    allocate( cluster%cna( max_pairs, system%num_particles ) )
    if( allocated( cluster%cutoff ) ) deallocate( cluster%cutoff )
    allocate( cluster%cutoff( system%num_particles ) )
    if( allocated( cluster%N0 ) ) deallocate( cluster%N0 )
    allocate( cluster%N0( system%num_particles ) )

  end subroutine initialize_cluster

  !==============================================!
  !     identify clusters from neighbor list     !
  !==============================================!
  subroutine find_clusters(parameters,system,cluster)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    ! internal variables
    integer :: num_clusters
    integer :: particle_id, type_id

    do type_id = 0, num_structures

       ! reset particle list
       cluster%particle_list = 0

       ! only search for particles of the same type
       where( cluster%type .eq. type_id )
          cluster%particle_list = 1
       end where

       ! perform search of particle tree to identify clusters
       do while( sum( cluster%particle_list ) .gt. 0 )
          ! locate first unsearched particle
          particle_id = minloc( cluster%particle_list, 1, cluster%particle_list .eq. 1 )
          ! search the tree
          call cluster_recursion(parameters,system,cluster,particle_id)
          ! go to next cluster
          cluster%stats(1) = cluster%stats(1) + 1
          cluster%stats(2) = 1
       end do

    end do

    ! save the number of clusters and allocate memory for com
    ! note: there is an empty cluster for single particles at the end
    num_clusters = cluster%stats(1) - 1 + 1
    cluster%num_clusters = num_clusters

    ! filter out single particle clusters
    call filter_small_clusters(parameters,system,cluster)

  end subroutine find_clusters

  !==============================================!
  !     identify clusters from neighbor list     !
  !     this is the cluster-centric version      !
  !==============================================!
  subroutine find_clusters_cna(parameters,system,cluster,cna_list)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    type (cna_struct), dimension(:), intent(inout) :: cna_list
    ! internal variables
    integer :: num_clusters
    integer :: particle_id

    ! set global recursion counter
    recursion_depth = 0

    ! reset particle list
    cluster%particle_list = 1

    ! reset cluster data
    ! cluster%map = 0
    cluster%num_clusters = 0
    cluster%stats(1) = 1 ! number of clusters
    cluster%stats(2) = 0 ! each cluster starts with 0 particles

    ! perform search of particle tree to identify clusters
    do while( sum( cluster%particle_list ) .gt. 0 )
       ! locate first unsearched particle
       particle_id = minloc( cluster%particle_list, 1, cluster%particle_list .eq. 1 )
       ! search the tree
       call cluster_recursion_cna(parameters,system,cluster,cna_list,particle_id)
       ! go to next cluster
       cluster%stats(1) = cluster%stats(1) + 1
       cluster%stats(2) = 1
    end do

    ! save the number of clusters and allocate memory for com
    ! note: there is an empty cluster for single particles at the end
    num_clusters = cluster%stats(1) - 1 + 1
    ! num_clusters = cluster%stats(1) - 1
    cluster%num_clusters = num_clusters

    write(*,'("found ",I0," clusters")') num_clusters - 1

    ! filter out single particle clusters
    ! call filter_small_clusters(parameters,system,cluster)

    ! write(*,'("filtered down to ",I0," clusters")') cluster%num_clusters

  end subroutine find_clusters_cna

  !=======================================================!
  !     recursively search neighbor list for clusters     !
  !=======================================================!
  recursive subroutine cluster_recursion(parameters,system,cluster,particle_id)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    integer,                 intent(inout) :: particle_id
    ! internal variables
    integer :: neighbor_id, neighbor_counter

    ! indicate that this particle has been searched
    cluster%particle_list(particle_id) = 0

    ! loop over nearest neighbors of this particle
    do neighbor_counter = 1, cluster%N0(particle_id)

       ! identify the neighbor_id from the neighbor map
       neighbor_id = cluster%neighbor_map(neighbor_counter,particle_id)

       ! branch down if the neighbor has not been searched
       if( cluster%particle_list(neighbor_id) .gt. 0 ) then
          call cluster_recursion(parameters,system,cluster,neighbor_id)
       end if

    end do

    ! record which particles are in each cluster
    ! cluster%map(cluster%stats(2),cluster%stats(1)) = particle_id

    ! record which cluster this particle belongs to
    cluster%reverse_map(particle_id) = cluster%stats(1)

    ! increment the cluster counter
    !     this will only trigger if all of the branches
    !     spawned by this particle are exhausted
    cluster%stats(2) = cluster%stats(2) + 1

  end subroutine cluster_recursion

  !=======================================================!
  !     recursively search neighbor list for clusters     !
  !=======================================================!
  recursive subroutine cluster_recursion_cna(parameters,system,cluster,cna_list,particle_id)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    type (cna_struct), dimension(:), intent(inout) :: cna_list
    integer,                 intent(inout) :: particle_id
    ! internal variables
    integer :: neighbor_id, neighbor_counter
    real(P) :: dist, r0_i, r0_j

    recursion_depth = recursion_depth + 1

    ! indicate that this particle has been searched
    cluster%particle_list(particle_id) = 0

    ! increment the cluster counter
    cluster%stats(2) = cluster%stats(2) + 1

    ! record which particles are in each cluster
    ! cluster%map(cluster%stats(2),cluster%stats(1)) = particle_id

    ! record which cluster this particle belongs to
    cluster%reverse_map(particle_id) = cluster%stats(1)

    ! loop over all neighbors of this particle
    do neighbor_counter = 1, cluster%neighbor_count(particle_id)

       ! identify the neighbor_id from the neighbor map
       neighbor_id = cluster%neighbor_map(neighbor_counter,particle_id)

       ! check that the neighbors are within one of the cutoffs
       dist  = cluster%neighbor_dist(neighbor_counter,particle_id)
       r0_i  = cluster%cutoff(particle_id) * r0_cutoff
       r0_j  = cluster%cutoff(neighbor_id) * r0_cutoff

       ! only continue if they are inside the larger cutoff
       if( dist .lt. max( r0_i, r0_j )  ) then
          ! branch down if the neighbor has not been searched
          if( cluster%particle_list(neighbor_id) .gt. 0 ) then
             ! AND it has an identical signature
             !   (must guarantee short-circuit)
             if( count_cna_matches(cna_list(particle_id), cna_list(neighbor_id)) &
               .ge. min(sum(cna_list(particle_id)%counts),&
               sum(cna_list(neighbor_id)%counts)) - parameters%cna_tol ) then
                if( recursion_depth .lt. recursion_limit ) then
                   call cluster_recursion_cna(parameters,system,cluster,cna_list,neighbor_id)
                end if
             end if
          endif

       end if

    end do

    !  anything below this point will only trigger
    !     if all of the branches spawned by this
    !     particle are exhausted

    recursion_depth = recursion_depth - 1

  end subroutine cluster_recursion_cna

  !=======================================================!
  !     recursively search neighbor list for clusters     !
  !=======================================================!
  recursive subroutine cluster_recursion_cna_global(parameters,system,cluster,cna_list,particle_id)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    type (cna_struct), dimension(:), intent(inout) :: cna_list
    integer,                 intent(inout) :: particle_id
    ! internal variables
    integer :: neighbor_id

    ! indicate that this particle has been searched
    cluster%particle_list(particle_id) = 0

    ! increment the cluster counter
    cluster%stats(2) = cluster%stats(2) + 1

    ! record which particles are in each cluster
    ! cluster%map(cluster%stats(2),cluster%stats(1)) = particle_id

    ! record which cluster this particle belongs to
    cluster%reverse_map(particle_id) = cluster%stats(1)

    ! loop over all neighbors of this particle
    do neighbor_id = 1, system%num_particles

       if( cluster%particle_list(neighbor_id) .gt. 0  ) then

          ! branch down if the neighbor has not been searched
          ! AND it has an identical signature
          if( cluster%particle_list(neighbor_id) .gt. 0 .and. &
               count_cna_matches(cna_list(particle_id), cna_list(neighbor_id)) &
               .ge. min(sum(cna_list(particle_id)%counts),&
               sum(cna_list(neighbor_id)%counts)) - parameters%cna_tol ) then
             call cluster_recursion_cna_global(parameters,system,cluster,cna_list,neighbor_id)
          end if

       end if

    end do

    !  anything below this point will only trigger
    !     if all of the branches spawned by this
    !     particle are exhausted

  end subroutine cluster_recursion_cna_global

  !=======================================================!
  !     recursively search neighbor list for clusters     !
  !     use the cna signature from the entire cluster     !
  !=======================================================!
  recursive subroutine cluster_recursion_cna_shells(parameters,system,cluster,cluster_cna,cna_list,particle_id)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    type (cna_struct), dimension(:), intent(inout) :: cna_list
    type (cna_struct),       intent(inout) :: cluster_cna
    integer,                 intent(inout) :: particle_id
    ! internal variables
    integer :: neighbor_id, neighbor_counter
    real(P) :: dist, r0_i, r0_j

    ! indicate that this particle has been searched
    cluster%particle_list(particle_id) = 0

    ! increment the cluster counter
    cluster%stats(2) = cluster%stats(2) + 1

    ! record which particles are in each cluster
    ! cluster%map(cluster%stats(2),cluster%stats(1)) = particle_id

    ! record which cluster this particle belongs to
    cluster%reverse_map(particle_id) = cluster%stats(1)

    ! loop over all neighbors of this particle
    do neighbor_counter = 1, cluster%neighbor_count(particle_id)

       ! identify the neighbor_id from the neighbor map
       neighbor_id = cluster%neighbor_map(neighbor_counter,particle_id)

       ! check that the neighbors are within one of the cutoffs
       dist  = cluster%neighbor_dist(neighbor_counter,particle_id)
       r0_i  = cluster%cutoff(particle_id) * r0_cutoff
       r0_j  = cluster%cutoff(neighbor_id) * r0_cutoff

       ! only continue if they are inside the larger cutoff
       if( dist .lt. max( r0_i, r0_j )  ) then

          ! branch down if the neighbor has not been searched
          ! AND it has an identical signature
          if( cluster%particle_list(neighbor_id) .gt. 0 .and. &
               count_cna_matches( cluster_cna, cna_list(neighbor_id) ) .ge. &
               min( sum(cluster_cna%counts), sum(cna_list(neighbor_id)%counts) ) &
               - parameters%cna_tol ) then
             call cluster_recursion_cna_shells(parameters,system,cluster,cluster_cna,cna_list,neighbor_id)
          end if

       end if

    end do

    !  anything below this point will only trigger
    !     if all of the branches spawned by this
    !     particle are exhausted

  end subroutine cluster_recursion_cna_shells

  !=============================================!
  !     filter out single-particle clusters     !
  !=============================================!
  subroutine filter_small_clusters(parameters,system,cluster)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    ! internal variables
    integer :: i, j, k, cluster_size

    ! loop through clusters
    i = 1
    do while( i .lt. cluster%num_clusters )
       cluster_size = count( cluster%reverse_map .eq. i )
       if( cluster_size .lt. min_cluster_size ) then
          do k = 1, cluster_size
             ! find the next particle corresponding to this cluster
             j = minloc(cluster%reverse_map, 1, cluster%reverse_map .eq. i )
             ! change the particle type to disordered
             cluster%type(j) = 0
             ! move this particle to the unassigned cluster
             cluster%reverse_map(j) = cluster%num_clusters
          end do
          ! decrement all indices by one to remove cluster from reverse map
          where( cluster%reverse_map .ge. i )
             cluster%reverse_map = cluster%reverse_map - 1
          end where
          ! decrement number of clusters
          cluster%num_clusters = cluster%num_clusters - 1
       else
          ! only increment counter if we did not decrement cluster ids
          i = i + 1
       end if
    end do

    ! rebuild map from reverse map
    ! cluster%map = 0
    ! do i = 1, system%num_particles
    !    j = cluster%reverse_map(i)
    !    k = count(cluster%map(:,j) .gt. 0) + 1
    !    cluster%map(k,j) = i
    ! end do

  end subroutine filter_small_clusters

  !====================================================!
  !     generate neighbor list using the cell list     !
  !====================================================!
  subroutine build_neighbor_list_cell_list(parameters,system,cluster)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    ! internal variables
    real(P), dimension(3) :: r_ij, r_i, r_j
    real(P) :: r_ij_length
    integer :: particle_id, neighbor_id, count, local_id
    integer :: cell_id, neighbor_cell_id, cell_counter

    ! build the cell list
    !   1.5 cells = neighbor list radius
    parameters%cell_cutoff = parameters%neighbor_cutoff / 1.5_P
    call build_cell_list(parameters,system)

    ! ensure that memory is allocated for the neighbor list
    call initialize_cluster(parameters,system,cluster)

    cluster%neighbor_count = 0
    cluster%neighbor_map = 0

    ! loop through particles
    do particle_id = 1, system%num_particles
       ! retrieve particle data
       r_i = system%position(:,particle_id)
       ! find the cell id from the particle data
       cell_id = which_cell(system,system%position(:,particle_id))
       ! loop over neighbor cells (includes itself)
       do cell_counter = 1, 27
          ! retrieve neighbor cell id from
          neighbor_cell_id = system%cell_list%neighbors(cell_counter,cell_id)
          ! loop through particles in the chosen cell
          do local_id = 1, system%cell_list%cell_count(neighbor_cell_id)
             ! find its identity from the map
             neighbor_id = system%cell_list%cell_map(local_id,neighbor_cell_id)
             ! skip lower triangular interactions
             if( neighbor_id .gt. particle_id ) then
                ! get displacement vector and distance
                r_j = system%position(:,neighbor_id)
                r_ij = displacement(system,r_i,r_j)
                r_ij_length = sqrt( sum( r_ij**2.0_P ) )
                ! do assignments
                if( r_ij_length .le. parameters%neighbor_cutoff ) then

                   ! assign neighbor_id to particle_id neighbor list
                   count = cluster%neighbor_count(particle_id) + 1
                   ! update number of neighbors
                   cluster%neighbor_count(particle_id) = count
                   ! update neighbor identities
                   cluster%neighbor_map(count,particle_id) = neighbor_id
                   ! update neighbor distance
                   cluster%neighbor_dist(count,particle_id) = r_ij_length

                   ! assign particle_id to neighbor_id neighbor list
                   count = cluster%neighbor_count(neighbor_id) + 1
                   ! update number of neighbors
                   cluster%neighbor_count(neighbor_id) = count
                   ! update neighbor identities
                   cluster%neighbor_map(count,neighbor_id) = particle_id
                   ! update neighbor distance
                   cluster%neighbor_dist(count,neighbor_id) = r_ij_length

                end if
             end if
          end do
       end do
    end do

  end subroutine build_neighbor_list_cell_list

  !============================================================!
  !     generate neighbor list without using the cell list     !
  !============================================================!
  subroutine build_neighbor_list_no_cell_list(parameters,system,cluster)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    ! internal variables
    real(P), dimension(3) :: r_ij, r_i, r_j
    real(P) :: r_ij_length
    integer :: particle_id, neighbor_id, count, local_id

    ! ensure that memory is allocated for the neighbor list
    call initialize_cluster(parameters,system,cluster)

    cluster%neighbor_count = 0
    cluster%neighbor_map = 0

    ! loop through particles
    do particle_id = 1, (system%num_particles-1)
       ! retrieve particle data
       r_i = system%position(:,particle_id)
       do neighbor_id = (particle_id+1), system%num_particles
          ! skip lower triangular interactions
          if( neighbor_id .gt. particle_id ) then
             ! get displacement vector and distance
             r_j = system%position(:,neighbor_id)
             r_ij = displacement(system,r_i,r_j)
             r_ij_length = sqrt( sum( r_ij**2.0_P ) )
             ! do assignments
             if( r_ij_length .le. parameters%neighbor_cutoff ) then

                ! assign neighbor_id to particle_id neighbor list
                count = cluster%neighbor_count(particle_id) + 1
                ! update number of neighbors
                cluster%neighbor_count(particle_id) = count
                ! update neighbor identities
                cluster%neighbor_map(count,particle_id) = neighbor_id
                ! update neighbor distance
                cluster%neighbor_dist(count,particle_id) = r_ij_length

                ! assign particle_id to neighbor_id neighbor list
                count = cluster%neighbor_count(neighbor_id) + 1
                ! update number of neighbors
                cluster%neighbor_count(neighbor_id) = count
                ! update neighbor identities
                cluster%neighbor_map(count,neighbor_id) = particle_id
                ! update neighbor distance
                cluster%neighbor_dist(count,neighbor_id) = r_ij_length

             end if
          end if
       end do
    end do

  end subroutine build_neighbor_list_no_cell_list

  !==================================================!
  !     generate neighbor list without cell list     !
  !==================================================!
  subroutine build_neighbor_list(parameters,system,cluster)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    ! internal variables
    real(P), dimension(3) :: r_ij, r_i, r_j
    real(P) :: r_ij_length
    integer :: particle_id, neighbor_id, count

    ! ensure that memory is allocated for the neighbor list
    call initialize_cluster(parameters,system,cluster)

    cluster%neighbor_count = 0
    cluster%neighbor_map = 0

    ! loop through particles
    do particle_id = 1, (system%num_particles - 1)
       ! retrieve particle data
       r_i = system%position(:,particle_id)
       ! loop over other particles
       do neighbor_id = (particle_id + 1), system%num_particles
          ! get displacement vector and distance
          r_j = system%position(:,neighbor_id)
          r_ij = displacement(system,r_i,r_j)
          r_ij_length = sqrt( sum( r_ij**2.0_P ) )
          ! do assignments
          if( r_ij_length .le. parameters%neighbor_cutoff ) then

             ! assign neighbor_id to particle_id neighbor list
             count = cluster%neighbor_count(particle_id) + 1
             ! update number of neighbors
             cluster%neighbor_count(particle_id) = count
             ! update neighbor identities
             cluster%neighbor_map(count,particle_id) = neighbor_id
             ! update neighbor distance
             cluster%neighbor_dist(count,particle_id) = r_ij_length

             ! assign particle_id to neighbor_id neighbor list
             count = cluster%neighbor_count(neighbor_id) + 1
             ! update number of neighbors
             cluster%neighbor_count(neighbor_id) = count
             ! update neighbor identities
             cluster%neighbor_map(count,neighbor_id) = particle_id
             ! update neighbor distance
             cluster%neighbor_dist(count,neighbor_id) = r_ij_length

          end if
       end do
    end do

  end subroutine build_neighbor_list

  subroutine sort_neighbor_list(system,cluster)
    implicit none
    ! external variables
    type (system_struct),    intent(inout) :: system
    type (cluster_struct),   intent(inout) :: cluster
    ! internal variables
    integer :: i, j, k, l
    integer :: nearest_id, nearest_id_count
    real(P) :: last_nearest_dist, nearest_dist, dist_sum
    integer, dimension(cluster%max_occupancy) :: map
    integer, dimension(cluster%max_occupancy) :: temp_nl_map
    real(P), dimension(cluster%max_occupancy) :: temp_nl_dist

    do i = 1, system%num_particles

       last_nearest_dist = 0.0_P
       dist_sum = 0.0_P
       map = 0

       k = cluster%neighbor_count(i)

       ! construct a mapping from the unsorted to sorted list
       j = 0
       do while( j .lt. k )
          ! identify the next smallest distance
          nearest_dist = minval(cluster%neighbor_dist(1:k,i), cluster%neighbor_dist(1:k,i) .gt. last_nearest_dist)
          ! count the entries that have this distance
          nearest_id_count = count(cluster%neighbor_dist(1:k,i) .eq. nearest_dist)
          ! loop over those entries
          l = 0
          nearest_id = 0
          do while( l .lt. nearest_id_count .and. j .lt. k )
             nearest_id = minloc(cluster%neighbor_dist(nearest_id+1:k,i), &
                  1, cluster%neighbor_dist(nearest_id+1:k,i) .eq. nearest_dist) + nearest_id

             l = l + 1
             j = j + 1

             ! the nl index of the jth farthest neighbor is map(j)
             map(j) = nearest_id
          end do
          last_nearest_dist = nearest_dist
       end do

       ! copy data to temporary memory
       temp_nl_map(1:k)  = cluster%neighbor_map(1:k,i)
       temp_nl_dist(1:k) = cluster%neighbor_dist(1:k,i)

       ! sort the lists according to the mapping
       do j = 1, k
          cluster%neighbor_map(j,i)  = temp_nl_map(map(j))
          cluster%neighbor_dist(j,i) = temp_nl_dist(map(j))
       end do

    end do

  end subroutine sort_neighbor_list

  subroutine get_nearby_neighbors(system,parameters,cluster,cutoff_flag)
    implicit none
    ! external variables
    type (system_struct),  intent(inout) :: system
    type (parameter_struct), intent(inout) :: parameters
    type (cluster_struct), intent(inout) :: cluster
    integer, intent(inout) :: cutoff_flag
    ! internal variables
    integer :: k, particle_id
    real(P) :: r0, avg_r0, count_r0

    do particle_id = 1, system%num_particles

       k = cluster%neighbor_count(particle_id)

       if( k .le. 0 ) then
          cluster%N0(particle_id) = 0
          cluster%cutoff(particle_id) = 0.0_P
       else
          ! need at least num_nn_avg neighbors to define a cutoff radius
          if( k .lt. num_nn_avg ) then
             write(*,'("particle ",I0," has fewer than ",I0," neighbors! (",I0,")")') particle_id, num_nn_avg, k
             ! parameters%neighbor_cutoff = parameters%neighbor_cutoff * 1.5_P
             ! cluster%cutoff(particle_id) = r0
             ! cluster%N0(particle_id) = 0
             ! return
             r0 = sqrt( sum(cluster%neighbor_dist(1:k,particle_id)**2.0_P) / real(k,P) )
          else
             ! get adaptive cutoff radius
             r0 = sqrt( sum(cluster%neighbor_dist(1:num_nn_avg,particle_id)**2.0_P) / real(num_nn_avg,P) )

             avg_r0 = avg_r0 + r0
             count_r0 = count_r0 + 1.0
          end if

          cluster%cutoff(particle_id) = r0

          ! find the neighbors that fall in the first shell
          cluster%N0(particle_id) = count(cluster%neighbor_dist(1:k,particle_id) .le. r0_cutoff * r0)

          ! check that neighborlist cutoff is large enough
          if( count(cluster%neighbor_dist(1:k,particle_id) .gt. r0_cutoff * r0) .eq. 0 ) then
             write(*,'("particle ",I0," has all neighbors inside its r0 cutoff!")') particle_id
             ! parameters%neighbor_cutoff = parameters%neighbor_cutoff * 1.5_P
             ! return
          else
             ! write(*,'("particle ",I0," has ",I0," neighbors and r0 = ",A)') &
             !      particle_id, cluster%N0(particle_id), var_float(cluster%cutoff(particle_id),2)
          end if
       end if

    end do

    ! set the flag to ok if the routine completed
    cutoff_flag = 1

  end subroutine get_nearby_neighbors

end module cluster
