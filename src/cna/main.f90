program main
  use datatypes
  use io
  use celllist
  use cluster
  use compute
  use cna
  use reference
  use multiatom
  implicit none
  type (parameter_struct) :: parameters
  type (system_struct) :: system
  type (cluster_struct) :: nl
  type (identity_struct) :: identities
  type (cna_struct), dimension(:), allocatable :: cna_list
  type (multiatom_struct), dimension(:), allocatable :: ma_list
  integer :: i, j
  integer :: cutoff_flag
  real(P) :: start_time, current_time, elapsed_time
  character(len=1000) :: fline
  integer :: f_list = 0
  integer :: ierr = 0
  integer :: f_dat = 10
  integer, dimension(:), allocatable :: reference_histogram, temp_histogram
  
  ! start the clock
  call cpu_time(start_time)

  ! get the run name from the command line
  call parse_cmdline(parameters)

  ! read reference structures
  call read_ref_entries_cna(parameters,identities)

  ! open data files
  open(unit=f_dat,file=trim(parameters%snapshot_file)//'.dat',action='write',status='unknown')
  
  open(unit=f_list,file=trim(parameters%snapshot_file),action='read',status='old')
  
  read(f_list,'(A)',iostat=ierr) fline
  do while( ierr .eq. 0 )
     
     parameters%config_file = trim(fline)
     read(f_list,'(A)',iostat=ierr) fline

     ! read XML config and initialize system
     call acquire_config(parameters,system)

     ! write the elapsed time for the neighborlist
     call cpu_time(current_time)
     elapsed_time = current_time - start_time
     write(*,'(F10.5," : Reading XML complete")') current_time

     cutoff_flag = 0

     do while( cutoff_flag .eq. 0 )

        ! limit the radius to half the smallest box dimension
        if( parameters%neighbor_cutoff .gt. minval(system%box_length) * 0.5_P  ) then
           parameters%neighbor_cutoff = minval(system%box_length) * 0.5_P
           write(*,'("Warning: CNA radius cannot exceed half the smallest box dimension")')
           cutoff_flag = 1
        end if
        
        ! build the neighbor list
        call build_neighbor_list_cell_list(parameters,system,nl)

        ! write the elapsed time for building the neighborlist
        call cpu_time(current_time)
        elapsed_time = current_time - elapsed_time
        write(*,'(F10.5," : Building neighborlist with cutoff ",A," complete")') &
             current_time, var_float(parameters%neighbor_cutoff,2)

        ! sort the neighborlist by distance
        call sort_neighbor_list(system,nl)

        ! get number of nearby neighbors
        call get_nearby_neighbors(system,parameters,nl,cutoff_flag)

        cutoff_flag = 1
        
     end do
     
     ! write the elapsed time for sorting the neighborlist
     call cpu_time(current_time)
     elapsed_time = current_time - elapsed_time
     write(*,'(F10.5," : Sorting neighborlist complete")') current_time
     
     ! run common neighbor analysis
     if( allocated( cna_list ) ) deallocate( cna_list )
     allocate(cna_list(system%num_particles))
     do i = 1, system%num_particles
        call common_neighbor_analysis(i,system,nl,cna_list(i))
     end do

     ! write the elapsed time for the run
     call cpu_time(current_time)
     elapsed_time = current_time - elapsed_time
     write(*,'(F10.5," : Common neighbor analysis complete")') current_time

     ! try to do a clustering analysis using the common neighbor signatures
     ! the algorithm allows differences to accumulate into a cascading chain
     parameters%cna_tol = 0
     call find_clusters_cna(parameters,system,nl,cna_list)

     ! write the elapsed time for the run
     call cpu_time(current_time)
     elapsed_time = current_time - elapsed_time
     write(*,'(F10.5," : Clustering complete")') current_time

     ! match perfect clusters against reference structures
     call match_clusters_cna(system,nl,parameters,cna_list,identities)

     ! search for multi-atom patterns
     if( parameters%map_construction ) then
        print*,'trying to make a MAP'

        if( allocated(ma_list) ) deallocate(ma_list)
        allocate( ma_list( system%num_particles ) )
        call identify_multiatom(system,nl,parameters,cna_list,ma_list)

        ! write the elapsed time for the run
        call cpu_time(current_time)
        elapsed_time = current_time - elapsed_time
        write(*,'(F10.5," : Multi-atom analysis complete")') current_time

        call write_maps(parameters,ma_list)
        
        ! write the elapsed time for the run
        call cpu_time(current_time)
        elapsed_time = current_time - elapsed_time
        write(*,'(F10.5," : MAP write complete")') current_time
     end if
     
     ! find perfect cluster neighbors
     ! call cluster_neighbors(system,nl,parameters%config_file,'exclusive')
     
     ! write the elapsed time for the run
     call cpu_time(current_time)
     elapsed_time = current_time - elapsed_time
     write(*,'(F10.5," : Cluster matching complete")') current_time

     ! assign singleton clusters to the nearest available reference structure
     call assign_singleton_cna(parameters,system,nl,cna_list,identities)

     ! find singleton-inclusive cluster neighbors
     ! call cluster_neighbors(system,nl,parameters%config_file,'inclusive')

     ! write the elapsed time for the run
     call cpu_time(current_time)
     elapsed_time = current_time - elapsed_time
     write(*,'(F10.5," : Singleton matching complete")') current_time

     ! write a colored xml
     call write_XML(parameters,system,nl,'full_color')

     ! write clustering results to file
     call write_reference_structure(parameters,identities,nl)

     ! ensure histogram is the correct size
     if( .not. allocated(reference_histogram) ) allocate( reference_histogram(ref_size) )
     if( size(reference_histogram) .ne. ref_size ) then
        if( allocated( temp_histogram ) ) deallocate( temp_histogram )
        ! copy histogram to temporary buffer
        allocate( temp_histogram(ref_size) )
        temp_histogram = 0
        temp_histogram(1:size(reference_histogram)) = reference_histogram
        ! resize reference_histogram
        if( allocated( reference_histogram) ) deallocate( reference_histogram )
        allocate( reference_histogram(ref_size) )
        reference_histogram = temp_histogram
     end if
     
     reference_histogram = 0
     do i = 1, system%num_particles
        j = nl%type(i) + 1
        reference_histogram(j) = reference_histogram(j) + 1
     end do
     do i = 1, size(reference_histogram)
        write(f_dat,'(I0," ")',advance='no') reference_histogram(i)
     end do
     write(f_dat,*)
     
     ! write the elapsed time for the run
     call cpu_time(current_time)
     elapsed_time = current_time - elapsed_time
     write(*,'(F10.5," : Analysis of snapshot ",A," complete")') current_time, trim(parameters%config_file)
     
     call flush()
     
  end do

  ! close data file
  close(f_dat)

end program main
