########################################
# Set up how to compile the source files
########################################

# Add the source files
SET(HRC_src ${SRCHRC}/agglomerate.f90
	    ${SRCHRC}/main.f90
)

# Define the executable in terms of the source files
ADD_EXECUTABLE(${HRCEXE} ${HRC_src})

#####################################################
# Add the needed libraries and special compiler flags
#####################################################

# This links foo to the analysis library
TARGET_LINK_LIBRARIES(${HRCEXE} ${ANALYSISLIB})

# Uncomment if you need to link to BLAS and LAPACK
#TARGET_LINK_LIBRARIES(${HRCEXE} ${BLAS_LIBRARIES}
#                                ${LAPACK_LIBRARIES}
#                                ${CMAKE_THREAD_LIBS_INIT})

# Uncomment if you have parallization
#IF(USE_OPENMP)
#    SET_TARGET_PROPERTIES(${HRCEXE} PROPERTIES
#                          COMPILE_FLAGS "${OpenMP_Fortran_FLAGS}"
#                          LINK_FLAGS "${OpenMP_Fortran_FLAGS}")
#ELSEIF(USE_MPI)
#    SET_TARGET_PROPERTIES(${HRCEXE} PROPERTIES
#                          COMPILE_FLAGS "${MPI_Fortran_COMPILE_FLAGS}"
#                          LINK_FLAGS "${MPI_Fortran_LINK_FLAGS}")
#    INCLUDE_DIRECTORIES(${MPI_Fortran_INCLUDE_PATH})
#    TARGET_LINK_LIBRARIES(${HRCEXE} ${MPI_Fortran_LIBRARIES})
#ENDIF(USE_OPENMP)

#####################################
# Tell how to install this executable
#####################################

SET(CMAKE_INSTALL_PREFIX ~/)
INSTALL(TARGETS ${HRCEXE} RUNTIME DESTINATION ${BIN})
