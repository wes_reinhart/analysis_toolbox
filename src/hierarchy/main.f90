program main
  use datatypes
  use io
  use celllist
  use cluster
  use compute
  use cna
  use reference
  use agglomerate
  implicit none
  type (parameter_struct) :: parameters
  type (cluster_struct) :: nl
  type (identity_struct) :: identities, global_identities
  type (hierarchy_struct) :: hierarchy
  integer :: i, j, frame_id
  real(P) :: start_time, current_time, elapsed_time
  character(len=1000) :: fline
  integer :: f_list = 0
  integer :: f_dat = 10  
  integer :: ierr = 0
  integer, dimension(:), allocatable :: ref_hist
  
  ! start the clock
  call cpu_time(start_time)

  ! get the run name from the command line
  call parse_cmdline_hierarchy(parameters)
  
  ! open list of snapshots
  open(unit=f_list,file=trim(parameters%snapshot_file),action='read',status='old')

  ! read the first snapshot to get trajectory info
  read(f_list,'(A)',iostat=ierr) fline
  parameters%config_file = trim(fline)

  ! find the number of frames that will be read in
  parameters%num_frames = 1
  read(f_list,'(A)',iostat=ierr) fline
  do while( ierr .eq. 0 )
     parameters%num_frames = parameters%num_frames + 1
     read(f_list,'(A)',iostat=ierr) fline
  end do

  write(*,'("preparing to read in ",I0," frames from snapshot list")') parameters%num_frames

  ! setup necessary data structures
  call acquire_hierarchy_info(parameters,nl)
  call setup_identities(parameters,identities)
  call setup_identities(parameters,global_identities)
  allocate(ref_hist(n_ref_buffer))
  
  ! read reference structures
  call read_ref_entries_cna(parameters,global_identities)

  ! open data file
  open(unit=f_dat,file=trim(parameters%snapshot_file)//'.dat',action='write',status='unknown')
  
  ! go back through the trajectories and accumulate bond angles
  rewind(f_list)  
  read(f_list,'(A)',iostat=ierr) fline
  identities%frame_id = 0
  do while( ierr .eq. 0 )

     parameters%config_file = trim(fline)
     read(f_list,'(A)',iostat=ierr) fline
     identities%frame_id = identities%frame_id + 1
     
     ! read the reference structure back from file
     call reset_identities(identities)
     call read_reference_structure(parameters,identities)

     ! reset ref_size
     ref_size = size(global_identities%baa)
     
     ! merge into the global definition
     call merge_reference_structures(parameters,identities,global_identities)

     write(*,'("merged ",I0," local into ",I0," global")') num_local_structures, num_structures

     ! reset histogram of observed structures
     if( ref_size .gt. size(ref_hist) ) then
        if( allocated( ref_hist ) ) deallocate( ref_hist )
        allocate( ref_hist( ref_size ) )
     end if
     ref_hist = 0
     
     ! generate histogram
     do i = 1, num_local_structures
        
        ! map to global
        j = global_identities%global_map(i,identities%frame_id)
        
        ! write to global histogram bin
        ref_hist(j) = identities%baa(i)%num_total
        
     end do
     
     ! write histogram
     do i = 1, size(ref_hist)
        write(f_dat,'(I0," ")',advance='no') ref_hist(i)
     end do
     write(f_dat,*)
     
  end do

  ! close data file
  close(f_dat)

  ! save global references
  call write_global_reference_structure(parameters,global_identities)

  ! go back through the trajectories and recolor snapshots
  rewind(f_list)
  frame_id = 0
  read(f_list,'(A)',iostat=ierr) fline
  do while( ierr .eq. 0 )

     parameters%config_file = trim(fline)
     read(f_list,'(A)',iostat=ierr) fline
     frame_id = frame_id + 1

     write(*,'("Recoloring ",A)') trim(parameters%config_file)
     
     ! recolor snapshot using trajectory-wide library
     call recolor_XML_full(frame_id,parameters,global_identities,hierarchy)
     
  end do

  ! write the elapsed time for the run
  call cpu_time(current_time)
  elapsed_time = current_time - elapsed_time
  write(*,'(F10.5," : Trajectory color matching complete")') current_time

  if( parameters%tree_cutoff .gt. 0 ) then
     ! compare reference structures using bond angle analysis
     call hierarchical_clustering(parameters,global_identities,nl,hierarchy)

     ! write the elapsed time for the run
     call cpu_time(current_time)
     elapsed_time = current_time - start_time
     write(*,'(F10.5," : Reference structure agglomeration complete")') current_time

     ! go back through the trajectories and recolor snapshots
     rewind(f_list)
     frame_id = 0
     read(f_list,'(A)',iostat=ierr) fline
     do while( ierr .eq. 0 )

        parameters%config_file = trim(fline)
        read(f_list,'(A)',iostat=ierr) fline
        frame_id = frame_id + 1

        ! recolor snapshot using coarse clusters
        call recolor_XML(frame_id,parameters,global_identities,hierarchy)

     end do

     ! write the elapsed time for the run
     call cpu_time(current_time)
     elapsed_time = current_time - elapsed_time
     write(*,'(F10.5," : Trajectory color coarsening complete")') current_time
  end if

end program main
