module agglomerate
  use datatypes
  use util
  use compute
  implicit none

contains

  !===========================================================!
  !     the main wrapper for the agglomerative clustering     !
  !===========================================================!
  subroutine hierarchical_clustering(parameters,identities,nl,hierarchy)
    implicit none
    ! external variables
    type (parameter_struct), intent(inout) :: parameters
    type (identity_struct),  intent(inout) :: identities
    type (cluster_struct),   intent(inout) :: nl
    type (hierarchy_struct), intent(inout) :: hierarchy
    ! internal variables
    integer :: i
    real(P), dimension(num_structures,num_structures) :: X
    real(P) :: cutoff 

    ! calculate the distance matrix
    call compute_distance_matrix(parameters,identities,X)
    
    ! setup the hierarchy data structure
    call setup_hierarchy(hierarchy,X,identities)
    do i = 1, num_structures
       hierarchy%selfsim(i) = identities%p(i)
    end do
    
    write(*,'("performing agglomerative hierarchical clustering with n = ",I0)') &
         hierarchy%n
    
    ! perform the agglomeration until all clusters are merged
    i = 0
    do while( sum( hierarchy%Yprime ) .gt. 0.0 )
       call merge_nearest(hierarchy,identities)
    end do

    ! save the tree to file
    call write_hierarchy(parameters,hierarchy)

    ! decide cutoff from identities
    if( parameters%tree_cutoff .lt. 0.0 ) then
       cutoff = sum( identities%p, 1, identities%p .gt. 0 ) / real( count( identities%p .gt. 0 ), P)
    else
       cutoff = parameters%tree_cutoff
    end if

    cutoff = 100.0_P

    ! convert to coarse representation based on cutoff
    call tree_to_cluster_id(hierarchy,identities,nl,cutoff)
    
  end subroutine hierarchical_clustering

  !========================================!
  !     save hierarchical tree to file     !
  !========================================!
  subroutine write_hierarchy(parameters,hierarchy)
    implicit none
    ! external variables
    type (parameter_struct), intent(in) :: parameters
    type (hierarchy_struct), intent(in) :: hierarchy
    ! internal variables
    integer, parameter :: f_dat = 420
    integer :: i

    ! save to file
    open(unit=f_dat,file=trim(parameters%snapshot_file)//'.tree',action='write',status='unknown')

    ! loop over links
    do i = 1, hierarchy%l
       ! write to file
       write(f_dat,'(2I10,E15.5)') hierarchy%Zid(1:2,i), hierarchy%Zheight(i)
    end do
    
    close(f_dat)
    
  end subroutine write_hierarchy

  !====================================================!
  !     convert hierarchy tree to cluster identity     !
  !====================================================!
  subroutine tree_to_cluster_id(hierarchy,identities,nl,cutoff)
    implicit none
    ! external variables
    type (hierarchy_struct), intent(inout) :: hierarchy
    type (identity_struct),  intent(inout) :: identities
    type (cluster_struct),   intent(inout) :: nl
    real(P), intent(in) :: cutoff
    ! internal variables
    integer :: i, j, k, nlinks, max_m, max_obs
    integer, dimension(:), allocatable :: visited
    integer, dimension(size(nl%type)) :: coarse_type

    write(*,'("cutting tree at height ",A)') var_float(cutoff,5)
    
    nlinks = minloc( hierarchy%Zheight, 1, hierarchy%Zheight .gt. cutoff ) - 1
    ! number of leaves is always number of links + 1
    max_m = ( hierarchy%l + 1 ) + nlinks
    
    allocate( visited( max_m ) )
    visited = 0

    do i = 1, nlinks
       visited( hierarchy%Zid(1,i) ) = 1
       visited( hierarchy%Zid(2,i) ) = 1
    end do

    do i = 1, count( visited .eq. 0 )
       j = minloc( visited, 1, visited .eq. 0 )
       hierarchy%reduced_leaves(:,i) = hierarchy%leaves(:,j)
       visited(j) = 1
    end do

    coarse_type = 0
    do i = 1, count(hierarchy%reduced_leaves(1,:) .gt. 0)
       k = 0
       max_obs = 0
       do j = 1, count(hierarchy%reduced_leaves(:,i) .gt. 0)
          if( identities%baa(hierarchy%reduced_leaves(j,i))%num_obs .gt. max_obs) then
             k = hierarchy%reduced_leaves(j,i)
             max_obs = identities%baa(hierarchy%reduced_leaves(j,i))%num_obs
          end if
       end do

       if( k .gt. 0 ) then
          write(*,'("node ",I0,", typified by ",A,", contains: ")',advance='no') &
               i, trim(identities%name(k))
       else
          write(*,'("node ",I0," has no particles (probably), contains: ")',advance='no') i
       end if
       
       do j = 1, count(hierarchy%reduced_leaves(:,i) .gt. 0)
          write(*,'(I0," ")',advance='no') hierarchy%reduced_leaves(j,i)
          ! agglomerate this cluster into its parent
          hierarchy%reverse_map( hierarchy%reduced_leaves(j,i) ) = i
          where( nl%type .eq. hierarchy%reduced_leaves(j,i) )
             coarse_type = i
          end where
       end do
       write(*,*)
       
    end do

    nl%type = coarse_type

  end subroutine tree_to_cluster_id
  
  !============================================================!
  !     compare cluster signatures to reference structures     !
  !============================================================!
  subroutine compute_distance_matrix(parameters,identities,X)
    implicit none
    ! external variables
    type (parameter_struct), intent(in) :: parameters
    type (identity_struct), intent(inout) :: identities
    real(P), dimension(num_structures,num_structures), intent(out) :: X
    ! internal variables
    integer, parameter :: f_dat = 410
    integer :: i, j
    real(P) :: s

    write(*,'("computing similarity matrix based on bond angles...")')

    X = infinity
    s = 0.0_P
    
    ! assign self-similarity
    do i = 1, num_structures
       if( count( identities%baa(i)%angles .gt. -100.0 ) .gt. 0 ) then
          s = identities%p(i)
          X(i,i) = s
       end if
    end do
    
    ! loop over structures
    do i = 1, (num_structures-1)
       write(*,'(I0," ")',advance='no') i
       if( count( identities%baa(i)%angles .gt. -100.0 ) .gt. 0 ) then
          do j = (i+1), num_structures
             ! check for zero length
             if( count( identities%baa(j)%angles .gt. -100.0 ) .gt. 0 ) then
                ! compare to each other cluster
                s = match_residual(identities%baa(i)%angles, identities%baa(j)%angles)
                ! save to memory
                X(i,j) = s
                X(j,i) = s
             end if
          end do
       end if
    end do
    write(*,*)

    ! save to file
    open(unit=f_dat,file=trim(parameters%snapshot_file)//'.sim',action='write',status='unknown')

    ! loop over clusters
    do i = 1, num_structures
       do j = 1, num_structures
          ! write to file
          write(f_dat,'(E15.5," ")',advance='no') X(i,j)
       end do
       write(f_dat,'(A)')
    end do
    
    close(f_dat)
    
  end subroutine compute_distance_matrix
  
  !============================================!
  !     setup the hierarchy data structure     !
  !============================================!
  subroutine setup_hierarchy(hierarchy,X,identities)
    implicit none
    ! external variables
    type (hierarchy_struct), intent(inout) :: hierarchy
    real(P), dimension(:,:), intent(inout) :: X
    type (identity_struct),  intent(inout) :: identities
    ! internal variables
    integer :: Yrequest, i

    hierarchy%n = size(X,1)
    hierarchy%l = 0
    
    ! allocate memory (needs [2.5 * n * (n-1)] entries)
    Yrequest = 3 * num_structures * (num_structures-1)

    allocate( hierarchy%Y(Yrequest) )
    hierarchy%Y = infinity

    allocate( hierarchy%Yprime(Yrequest) )
    hierarchy%Yprime = 0

    allocate( hierarchy%Zid(2,Yrequest) ) ! cluster A, cluster B
    hierarchy%Zid = 0

    allocate( hierarchy%Zheight(Yrequest) ) ! link strength
    hierarchy%Zheight = 0.0_P

    allocate( hierarchy%leaves(num_structures,Yrequest) ) ! the original number of leaves is the max
    hierarchy%leaves = 0

    allocate( hierarchy%reduced_leaves(num_structures,Yrequest) )
    hierarchy%reduced_leaves = 0
    
    allocate( hierarchy%disallowed(Yrequest) )
    hierarchy%disallowed = 0

    allocate( hierarchy%reverse_map(num_structures) )
    hierarchy%reverse_map = 0

    allocate( hierarchy%selfsim(Yrequest) )
    hierarchy%selfsim = 0

    allocate( hierarchy%counts(Yrequest) )
    hierarchy%counts = 0
    
    ! make 1D representation from 2D distance matrix
    call make_lower(hierarchy,X)

    ! set up Yprime
    hierarchy%Yprime = 0
    hierarchy%Yprime(1:hierarchy%m) = 1

    ! set up leaves
    hierarchy%leaves = 0
    do i = 1, num_structures
       hierarchy%leaves(1,i) = i
       hierarchy%counts(i)   = identities%baa(i)%num_obs
       hierarchy%selfsim(i)  = identities%p(i)
    end do

  end subroutine setup_hierarchy

  !=========================================================!
  !     find the closest two clusters and perform merge     !
  !=========================================================!  
  subroutine merge_nearest(hierarchy,identities)
    implicit none
    ! external variables
    type (hierarchy_struct) :: hierarchy
    type (identity_struct)  :: identities
    ! internal variables
    integer :: a, b, min_id
    integer, dimension(num_structures) :: newleaves
    real(P), dimension(hierarchy%n)  :: newdist
    integer, dimension(hierarchy%n)  :: newprime

    newleaves = 0
    newdist = infinity
    newprime = 1
    
    ! choose clusters to merge from living clusters
    min_id = minloc( hierarchy%Y, 1, hierarchy%Yprime .eq. 1 )
    call lower_to_twodim(min_id,hierarchy%n,a,b)

    hierarchy%l = hierarchy%l + 1
    hierarchy%Zid(1,hierarchy%l) = a
    hierarchy%Zid(2,hierarchy%l) = b
    hierarchy%Zheight(hierarchy%l) = hierarchy%Y(min_id)
        
    ! merge leaves belonging to sub-clusters
    newleaves = 0
    call add_unique_leaves(a,newleaves,hierarchy)
    call add_unique_leaves(b,newleaves,hierarchy)
    hierarchy%leaves(1:count(newleaves .gt. 0),(hierarchy%n+1)) = &
         newleaves(1:count(newleaves .gt. 0))

    ! kill the old clusters
    call remove_row(a,hierarchy)
    call add_unique_disallowed(a,hierarchy)
    call remove_row(b,hierarchy)
    call add_unique_disallowed(b,hierarchy)

    ! use some linkage scheme to determine the new clusters
    call weighted_average_linkage(newdist,hierarchy,identities)

    ! set up new Yprime entries
    call get_newprime(newprime,hierarchy)

    ! append new cluster data
    hierarchy%Y( (hierarchy%m+1):(hierarchy%m+hierarchy%n) ) = newdist
    hierarchy%Yprime( (hierarchy%m+1):(hierarchy%m+hierarchy%n) ) = newprime
    hierarchy%m = hierarchy%m + hierarchy%n

    call get_newsize(hierarchy)
    
    ! increment cluster counter
    hierarchy%n = hierarchy%n + 1
    
  end subroutine merge_nearest

  !============================================================!
  !     calculate the new size of the agglomerated cluster     !
  !============================================================!    
  subroutine get_newsize(hierarchy)
    implicit none
    ! external variables
    type (hierarchy_struct) :: hierarchy
    ! internal variables
    integer :: i, j
    ! reset values
    j = hierarchy%n+1
    hierarchy%counts(j) = 0
    ! loop over leaves
    do i = 1, count(hierarchy%leaves(:,j) .gt. 0)
       hierarchy%counts(j) = hierarchy%counts(j) + hierarchy%counts(hierarchy%leaves(i,j))
    end do
  end subroutine get_newsize

  !=====================================!
  !     generate new Yprime entries     !
  !=====================================!    
  subroutine get_newprime(newprime,hierarchy)
    implicit none
    ! external variables
    integer, dimension(:), intent(inout) :: newprime
    type (hierarchy_struct) :: hierarchy
    ! internal variables
    integer :: i
    ! loop over all disallowed
    do i = 1, count( hierarchy%disallowed .gt. 0 )
       newprime(hierarchy%disallowed(i)) = 0
    end do
  end subroutine get_newprime
  
  !============================================================!
  !     complete linkage -- greatest distance among leaves     !
  !============================================================!  
  subroutine complete_linkage(newdist,hierarchy)
    implicit none
    ! external variables
    real(P), dimension(:), intent(inout) :: newdist
    type (hierarchy_struct) :: hierarchy
    ! internal variables
    integer :: j, k, l
    integer :: leafcount, nleaves, id
    real(P) :: dist, max_dist
    ! loop over all clusters
    do j = 1, hierarchy%n
       max_dist = 0.0_P
       l = 0
       ! loop over leaves of this cluster
       nleaves = count(hierarchy%leaves(:,hierarchy%n+1) .gt. 0)
       do leafcount = 1, nleaves
          k = hierarchy%leaves(leafcount,hierarchy%n+1)          
          if( k .ne. j ) then
             ! max distance of sub-clusters
             call twodim_to_lower(k,j,hierarchy%n,id)
             dist = hierarchy%Y(id)
             if( dist .gt. max_dist ) then
                max_dist = dist
             end if
          end if
       end do
       newdist(j) = max_dist
    end do
  end subroutine complete_linkage

  !=======================================================!
  !     average linkage -- mean distance among leaves     !
  !=======================================================!
  subroutine weighted_average_linkage(newdist,hierarchy,identities)
    implicit none
    ! external variables
    real(P), dimension(:), intent(inout) :: newdist
    type (hierarchy_struct), intent(inout) :: hierarchy
    type (identity_struct), intent(inout)  :: identities
    ! internal variables
    integer :: j, k, l, m
    integer :: leafcount, nleaves, id
    real(P) :: dist, this_dist

    hierarchy%selfsim(hierarchy%n+1) = weighted_average_selfsim(hierarchy,identities)
    
    ! get average distance vector for each cluster
    do j = 1, hierarchy%n
       dist = 0.0_P
       l = 0
       ! loop over leaves of this cluster
       nleaves = count(hierarchy%leaves(:,hierarchy%n+1) .gt. 0)
       do leafcount = 1, nleaves
          k = hierarchy%leaves(leafcount,hierarchy%n+1)
          if( k .ne. j ) then
             ! average distance of sub-clusters
             call twodim_to_lower(k,j,hierarchy%n,id)
             this_dist = hierarchy%Y(id)
             if( this_dist .gt. 0 .and. this_dist .lt. infinity * 0.5_P ) then
                m = hierarchy%counts(k)
                dist = dist + this_dist * real(m,P)
                l = l + m
             end if
          end if
       end do
       if( l .gt. 0 ) then
          newdist(j) = dist / real(l,P)
       else
          newdist(j) = infinity
       end if
       
       ! do not allow merges above maximum clustering cutoff
       if( newdist(j) .gt. hierarchy%selfsim(j) .or. &
            newdist(j) .gt. hierarchy%selfsim(hierarchy%n+1) ) then
          newdist(j) = infinity
       end if
       
    end do
    
  end subroutine weighted_average_linkage
  
  !=======================================================!
  !     average linkage -- mean distance among leaves     !
  !=======================================================!
  subroutine average_linkage(newdist,hierarchy,identities)
    implicit none
    ! external variables
    real(P), dimension(:), intent(inout) :: newdist
    type (hierarchy_struct), intent(inout) :: hierarchy
    type (identity_struct), intent(inout)  :: identities
    ! internal variables
    integer :: j, k, l
    integer :: leafcount, nleaves, id
    real(P) :: dist, this_dist

    ! hierarchy%selfsim(hierarchy%n+1) = average_selfsim(hierarchy,identities)
    ! hierarchy%selfsim(hierarchy%n+1) = minimum_selfsim(hierarchy,identities)
    ! hierarchy%selfsim(hierarchy%n+1) = maximum_selfsim(hierarchy,identities)
    hierarchy%selfsim(hierarchy%n+1) = infinity
    
    ! get average distance vector for each cluster
    do j = 1, hierarchy%n
       dist = 0.0_P
       l = 0
       ! loop over leaves of this cluster
       nleaves = count(hierarchy%leaves(:,hierarchy%n+1) .gt. 0)
       do leafcount = 1, nleaves
          k = hierarchy%leaves(leafcount,hierarchy%n+1)
          if( k .ne. j ) then
             ! average distance of sub-clusters
             call twodim_to_lower(k,j,hierarchy%n,id)
             this_dist = hierarchy%Y(id)
             if( this_dist .gt. 0 .and. this_dist .lt. infinity * 0.5_P ) then
                dist = dist + this_dist
                l = l + 1
             end if
          end if
       end do
       if( l .gt. 0 ) then
          newdist(j) = dist / real(l,P)
       end if
       
       ! do not allow merges above maximum clustering cutoff
       if( newdist(j) .gt. hierarchy%selfsim(j) .or. &
            newdist(j) .gt. hierarchy%selfsim(hierarchy%n+1) ) then
          newdist(j) = infinity
       end if
       
    end do
    
  end subroutine average_linkage

  !==============================================!
  !     minimum self-similarity among leaves     !
  !==============================================!
  real(P) function minimum_selfsim(hierarchy,identities)
    implicit none
    ! external variables
    type (hierarchy_struct), intent(inout) :: hierarchy
    type (identity_struct), intent(inout)  :: identities
    ! internal variables
    integer :: k, l
    integer :: leafcount, nleaves
    real(P) :: dist, this_dist, min_dist
    min_dist = infinity
    dist = 0.0_P
    l = 0
    ! loop over leaves of this cluster
    nleaves = count(hierarchy%leaves(:,hierarchy%n+1) .gt. 0)
    do leafcount = 1, nleaves
       k = hierarchy%leaves(leafcount,hierarchy%n+1)
       this_dist = identities%p(k)
       if( this_dist .lt. min_dist ) then
          min_dist = this_dist
       end if
    end do
    minimum_selfsim = min_dist
  end function minimum_selfsim

  !==============================================!
  !     minimum self-similarity among leaves     !
  !==============================================!
  real(P) function maximum_selfsim(hierarchy,identities)
    implicit none
    ! external variables
    type (hierarchy_struct), intent(inout) :: hierarchy
    type (identity_struct), intent(inout)  :: identities
    ! internal variables
    integer :: k, l
    integer :: leafcount, nleaves
    real(P) :: dist, this_dist, max_dist
    max_dist = 0.0_P
    dist = 0.0_P
    l = 0
    ! loop over leaves of this cluster
    nleaves = count(hierarchy%leaves(:,hierarchy%n+1) .gt. 0)
    do leafcount = 1, nleaves
       k = hierarchy%leaves(leafcount,hierarchy%n+1)
       this_dist = identities%p(k)
       if( this_dist .gt. max_dist ) then
          max_dist = this_dist
       end if
    end do
    maximum_selfsim = max_dist
  end function maximum_selfsim

  !===========================================!
  !     mean self-similarity among leaves     !
  !===========================================!
  real(P) function weighted_average_selfsim(hierarchy,identities)
    implicit none
    ! external variables
    type (hierarchy_struct), intent(inout) :: hierarchy
    type (identity_struct), intent(inout)  :: identities
    ! internal variables
    integer :: k, l, m
    integer :: leafcount, nleaves
    real(P) :: dist
    dist = 0.0_P
    l = 0
    ! loop over leaves of this cluster
    nleaves = count(hierarchy%leaves(:,hierarchy%n+1) .gt. 0)
    do leafcount = 1, nleaves
       k = hierarchy%leaves(leafcount,hierarchy%n+1)
       ! weighted average self-similarity of sub-clusters
       m = hierarchy%counts(k)
       dist = dist + identities%p(k) * real(m,P)
       l = l + m
    end do
    if( l .gt. 0 ) then
       weighted_average_selfsim = dist / real(l,P)
    else
       weighted_average_selfsim = 0.0_P
    end if
  end function weighted_average_selfsim
  
  !===========================================!
  !     mean self-similarity among leaves     !
  !===========================================!
  real(P) function average_selfsim(hierarchy,identities)
    implicit none
    ! external variables
    type (hierarchy_struct), intent(inout) :: hierarchy
    type (identity_struct), intent(inout)  :: identities
    ! internal variables
    integer :: k, l
    integer :: leafcount, nleaves
    real(P) :: dist
    dist = 0.0_P
    l = 0
    ! loop over leaves of this cluster
    nleaves = count(hierarchy%leaves(:,hierarchy%n+1) .gt. 0)
    do leafcount = 1, nleaves
       k = hierarchy%leaves(leafcount,hierarchy%n+1)
       ! average self-similarity of sub-clusters
       dist = dist + identities%p(k)
       l = l + 1
    end do
    if( l .gt. 0 ) then
       average_selfsim = dist / real(l,P)
    else
       average_selfsim = 0.0_P
    end if
  end function average_selfsim
  
  !=================================================!
  !     add unique leaves to the newleaves list     !
  !=================================================!
  subroutine add_unique_disallowed(a,hierarchy)
    implicit none
    ! external variables
    integer, intent(in) :: a
    type (hierarchy_struct) :: hierarchy
    ! internal variables
    integer :: j
    j = count( hierarchy%disallowed .gt. 0 )
    if( minloc( hierarchy%disallowed, 1, &
         hierarchy%disallowed .eq. a ) .eq. 0 ) then
       j = j + 1
       hierarchy%disallowed(j) = a
    end if
  end subroutine add_unique_disallowed
  
  !=================================================!
  !     add unique leaves to the newleaves list     !
  !=================================================!
  subroutine add_unique_leaves(a,newleaves,hierarchy)
    implicit none
    ! external variables
    integer, intent(in) :: a
    integer, dimension(:), intent(inout) :: newleaves
    type (hierarchy_struct) :: hierarchy
    ! internal variables
    integer :: i, j
    j = count( newleaves .gt. 0 )
    do i = 1, count( hierarchy%leaves(:,a) .gt. 0 )
       ! only add unique leaves
       if( minloc( newleaves, 1, newleaves .eq. hierarchy%leaves(i,a) ) .eq. 0 ) then
          j = j + 1
          newleaves(j) = hierarchy%leaves(i,a)
       end if
    end do
  end subroutine add_unique_leaves
  
  !=======================================!
  !     remove entire row from Yprime     !
  !=======================================!
  subroutine remove_row(a,hierarchy)
    implicit none
    ! external variables
    integer, intent(inout) :: a
    type (hierarchy_struct) :: hierarchy
    ! internal variables
    integer :: j, l
    j = 1
    do while( j .le. hierarchy%n )
       if( j .ne. a ) then
          call twodim_to_lower(a,j,hierarchy%n,l)
          hierarchy%Yprime(l) = 0
       end if
       j = j + 1
    end do
  end subroutine remove_row

  !=========================================================!
  !     convert 2D distance matrix to 1D representation     !
  !=========================================================!
  subroutine make_lower(hierarchy,X)
    implicit none
    ! external variables
    type (hierarchy_struct) :: hierarchy
    real(P), dimension(:,:), intent(in) :: X
    ! internal variables
    integer :: i, j, k
    k = 0
    do i = 2, size(X,1)
       do j = 1, (i-1)
          k = k + 1
          hierarchy%Y(k) = X(i,j)
       end do
    end do
    hierarchy%m = k
  end subroutine make_lower

  !================================!
  !     convert 1D index to 2D     !
  !================================!
  subroutine lower_to_twodim(in_l,n,i,j)
    implicit none
    ! external variables
    integer, intent(in)  :: in_l, n
    integer, intent(out) :: i, j
    ! internal variables
    integer :: k
    ! start at (2,1)
    k = 1
    i = 2
    j = 1
    do while( k .lt. in_l )
       ! increment 1D
       k = k + 1
       ! increment column
       j = j + 1
       ! increment row if column exceeds row
       !     start next row below the diagonal
       if( j .gt. (i-1) ) then
          i = i + 1
          j = 1
       end if
    end do
  end subroutine lower_to_twodim

  !================================!
  !     convert 2D index to 1D     !
  !================================!
  subroutine twodim_to_lower(in_i,in_j,n,l)
    implicit none
    ! external variables
    integer, intent(in)  :: in_i, in_j, n
    integer, intent(out) :: l
    ! internal variables
    integer :: i, j, x, temp
    i = in_i
    j = in_j
    ! need to be below the diagonal
    if( i .eq. j ) then
       write(*,'("twodim_to_lower: cannot ask for diagonal elements!")')
       l = 0
       return
    elseif( i .lt. j ) then
       temp = i
       i = j
       j = temp
    end if
    l = 0
    ! reach the row before the final one
    x = 0
    do while( x .lt. (i-1) )
       ! increment the 1d index
       l = l + x
       x = x + 1
    end do
    ! add the column
    l = l + j
  end subroutine twodim_to_lower
  
end module agglomerate
